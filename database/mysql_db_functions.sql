delimiter $$

DROP FUNCTION IF EXISTS a4w_get_adventure_start_date;
DROP FUNCTION IF EXISTS a4w_get_adventure_end_date;
DROP FUNCTION IF EXISTS a4w_get_adventure_sort_date;
DROP FUNCTION IF EXISTS a4w_is_active_adventure;


CREATE FUNCTION a4w_get_adventure_start_date(
    adv_event_id BIGINT(20) UNSIGNED
)
RETURNS DATETIME
BEGIN
    DECLARE adv_date VARCHAR(255) DEFAULT NULL;

         SELECT m.meta_value
           INTO adv_date
           FROM wp_jr9p0btxon_em_events AS e
      LEFT JOIN wp_jr9p0btxon_postmeta AS m
             ON e.post_id = m.post_id
          WHERE e.event_id = adv_event_id
            AND meta_key = 'adventure_start_date';

    RETURN STR_TO_DATE(adv_date, '%Y-%m-%d %T');
END $$

CREATE FUNCTION a4w_get_adventure_end_date(
    adv_event_id BIGINT(20) UNSIGNED
)
RETURNS DATETIME
BEGIN
    DECLARE adv_date VARCHAR(255) DEFAULT NULL;

         SELECT m.meta_value
           INTO adv_date
           FROM wp_jr9p0btxon_em_events AS e
      LEFT JOIN wp_jr9p0btxon_postmeta AS m
             ON e.post_id = m.post_id
          WHERE e.event_id = adv_event_id
            AND meta_key = 'adventure_date';

    RETURN STR_TO_DATE(adv_date, '%Y-%m-%d %T');
END $$

CREATE FUNCTION a4w_get_adventure_sort_date(
    adv_event_id BIGINT(20) UNSIGNED
)
RETURNS DATETIME
BEGIN
    DECLARE adv_post_id BIGINT(20) UNSIGNED DEFAULT 0;
    DECLARE adv_sort_field VARCHAR(255) DEFAULT 'end';
    DECLARE adv_date VARCHAR(255) DEFAULT NULL;

    SELECT post_id
      INTO adv_post_id
      FROM wp_jr9p0btxon_em_events
     WHERE event_id = adv_event_id;

    SELECT meta_value
      INTO adv_sort_field
      FROM wp_jr9p0btxon_postmeta
     WHERE post_id = adv_post_id
       AND meta_key = 'adventure_sort_date';

    IF adv_sort_field = 'end' THEN
        SELECT meta_value
          INTO adv_date
          FROM wp_jr9p0btxon_postmeta
         WHERE post_id = adv_post_id
           AND meta_key = 'adventure_date';
    ELSEIF adv_sort_field = 'start' THEN
        SELECT meta_value
          INTO adv_date
          FROM wp_jr9p0btxon_postmeta
         WHERE post_id = adv_post_id
           AND meta_key = 'adventure_start_date';
    END IF;

    RETURN STR_TO_DATE(adv_date, '%Y-%m-%d %T');
END $$

CREATE FUNCTION a4w_is_active_adventure(
    adv_event_id BIGINT(20) UNSIGNED
)
RETURNS BOOLEAN
BEGIN
    DECLARE adv_expiry_date DATETIME;
    DECLARE adv_end_datetime DATETIME;
    DECLARE adv_end_date DATE;
    DECLARE adv_end_time TIME;
    DECLARE adv_is_active BOOLEAN DEFAULT FALSE;
    
    SET time_zone = 'America/Edmonton';

    SELECT event_end
      INTO adv_expiry_date
      FROM wp_jr9p0btxon_em_events
     WHERE event_id = adv_event_id;

    IF adv_expiry_date < now() THEN
        SET adv_is_active = FALSE;
    ELSE
        SET adv_end_datetime = a4w_get_adventure_end_date( adv_event_id );
        SET adv_end_date = DATE( adv_end_datetime );
        
        IF adv_end_date IS NULL THEN
            SET adv_is_active = TRUE;
        ELSEIF adv_end_date > CURRENT_DATE() THEN
            SET adv_is_active = TRUE;
        ELSEIF adv_end_date < CURRENT_DATE() THEN
            SET adv_is_active = FALSE;
        ELSE
            SET adv_end_time = TIME( adv_end_datetime );
            
            IF adv_end_time = 0 THEN
                -- A "zero time" is shorthand for "all day adventure"; basically
                --  we interpret as being active until the end of the day / midnight.
                SET adv_is_active = TRUE;
            ELSE
                SET adv_is_active = (adv_end_time > CURRENT_TIME());
            END IF;
        END IF;
    END IF;
    
    RETURN adv_is_active;
END $$

delimiter ;
