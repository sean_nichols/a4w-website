<?php
/*
Template Name: Front Page
*/
?>
<?php get_header(); ?>

    <div class="row front-row" >
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="left-column">
    		<div id="banner" class="banner-video">
<?php
    $this_post_id = get_the_ID();
    $num_banner_img = get_post_meta( $this_post_id, 'images', TRUE );
    $banner_images = array();
    for ($i = 0; $i < $num_banner_img; $i++) {
        $banner_images[] = get_post_meta( $this_post_id, 'images_' . $i . '_image', TRUE );
    }
    $banner_img_str = implode( ',', $banner_images );
?>
                <?php echo do_shortcode( '[a4w-image-rotator width="738" height="500" images="' . $banner_img_str . '" length="30" random="true"]' ); ?>
    		</div><!-- banner -->

            <div class="page-content" style="margin-bottom:30px;"><?php the_content(); ?></div>

    		<div id="news-stories">

<?php /* echo '<p>[' . $num_banner_img . ':' . $banner_img_str . ']</p>' */ ?>

<?php /* variable for alternating post styles */
$oddpost = 'story-one';
?>
<?php $the_query = new WP_Query( 'showposts=2' ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

<div class="<?php echo $oddpost; ?>">
	<div class="story-text">
		<h3><?php the_title(); ?></h3>
		<?php the_excerpt(); ?> 
	</div><!-- story-text -->
	<div class="story-link">
		<a href="<?php the_permalink() ?>">Continue Reading</a>
	</div><!-- story-link -->
</div><!-- story-one -->


<?php /* Changes every other post to a different class */
$oddpost = 'story-one';
if ('story-one' == $oddpost) $oddpost = 'story-two';
else $oddpost = 'story-one';
?>
<?php endwhile;?>
<?php wp_reset_query(); ?>

    		</div><!-- news-stories -->    	
    		<div class="read-more-news">
    			<p><a href="<?php bloginfo('url'); ?>/news">Read More News Updates »</a></p>
    		</div><!-- read-more-news -->
    		<div class="sponsors">
    			<h2>Our Supporters</h2>
    			<div class="sponsor-logos">
	    			
    				<?php if(get_field('sponsor_logo')): ?>
                    <?php while(has_sub_field('sponsor_logo')): ?>
                    <a target="_blank" href="<?php the_sub_field('supporter_link') ?>" style="visibility: <?php the_sub_field('show_hide') ?>">
                    <img src="<?php the_sub_field('logo_image') ?>"></a>
                    <?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
					<center><a href="http://climbforwilderness.ca/about/corporate-supporters/">View our full list of supporters</a></center>
    			</div>
    		</div><!-- sponsors -->
    		<div class="footer-nav">
    			<?php wp_nav_menu( array('menu' => 'Footer Menu' )); ?>
    		</div><!-- footer-nav -->
    	</div><!-- left-column -->
    	
    	<div class="fourcol" id="right-column">
    		<?php get_sidebar(); ?>	
    	</div><!-- right-column -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->

<?php get_footer(); ?>
