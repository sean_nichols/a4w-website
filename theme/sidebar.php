		<?php wp_reset_query(); ?>
<?php /*
   		<div id="important-dates">
            <h4 class="green-text">IMPORTANT DATES</h4>
            <?php if(get_field('important_dates', 'option')): ?>
				<?php while(has_sub_field('important_dates', 'option')): ?>
					<p><span class="important-date-item"><?php the_sub_field('item'); ?></span><span class="right-text"><?php the_sub_field('date'); ?></span></p>
				<?php endwhile; ?>
			<?php endif; ?>
        </div><!-- important-dates -->
*/ ?>
<style type="text/css">
div.advlist li,
div.sponslist li {
	list-style: none;
	width: 100%;
	margin-bottom: 2px
}
div.advlist div.sidebar_spons_name,
div.advlist div.sidebar_spons_extra {
    height: 40px;
    vertical-align: top;
    display: inline-block;
    line-height: 18px;
    white-space: nowrap;
    padding-top: 3px;
}

div.advlist div.sidebar_spons_name {
    width: 66%;
    text-align: left;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 0px 3px 2px 0px;
}
div.advlist div.sidebar_spons_extra {
    width: 19%;
    text-align: right;
    margin-bottom: 1px;
}

div.advlist div.singleheight {
    height: 25px !important;
}

</style>
            <div id="upcoming-adventures" class="advlist">
                <h4><li class="green-text">Upcoming Adventures</h4>
                <?php echo do_shortcode( '[get-a4w-upcoming]' ); ?>
            </div><!-- upcoming-adventures -->
        
     		<div id="top-ten" class="advlist">
                <h4><li class="green-text">Top Adventures</h4>
                <?php echo do_shortcode( '[get-a4w-leaderboard type="adventures" scope="year:2022"]' ); ?>
	   		</div><!-- top-ten -->

<?php /*     		<div id="top-sponsors" class="advlist">
                <h4><li class="green-text">Sponsor Leaderboard</h4>
                <?php echo do_shortcode( '[get-a4w-leaderboard type="sponsors" scope="year:2021"]' ); ?>
	   		</div><!-- top-ten --> */ ?>

        <div class="sub-nav">
 
 <?php if ( ! dynamic_sidebar( 'Sub Navigation' )) : ?>

<?php endif; ?>
 
    		<div id="newsletter">
<h4>Sign Up for A4W News!</h4>
<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://AlbertaWilderness.us17.list-manage.com/subscribe/post?u=d32dee74e9b17a2745430f0ac&amp;id=9784013cd5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<h2>Subscribe to our mailing list</h2>
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME">First Name  <span class="asterisk">*</span>
</label>
	<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE3">Company </label>
	<input type="text" value="" name="MMERGE3" class="" id="mce-MMERGE3">
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE4">City </label>
	<input type="text" value="" name="MMERGE4" class="" id="mce-MMERGE4">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d32dee74e9b17a2745430f0ac_9784013cd5" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
    		</div><!-- newsletter -->
            <div id="a4w-instagram-widget" class="a4w-socialmedia-widget">
                <?php echo do_shortcode( '[instagram-feed]' ); ?>
            </div><!-- a4w-instagram-widget -->
    		<div id="a4w-facebook-widget" class="a4w-socialmedia-widget">
				<?php /* <div class="fb-like-box" data-href="https://www.facebook.com/Climb-and-Run-for-Wilderness-202085660139792/" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div> */ ?>
                <div class="fb-page" data-href="https://www.facebook.com/Adventures4Wilderness" data-tabs="timeline" data-width="278" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Adventures4Wilderness" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Adventures4Wilderness">Adventures for Wilderness</a></blockquote></div>
    		</div><!-- a4w-facebook-widget -->
    		<div id="a4w-twitter-widget" class="a4w-socialmedia-widget">
    			<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/Adventures4Wild" data-widget-id="422838832002637825">Tweets by @Adventures4Wild</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    		</div><!-- a4w-twitter-widget -->
