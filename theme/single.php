<?php get_header(); ?>

    <div class="row">
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="left-column">
    		<div class="page-content">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

			<article>
				<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<p><strong><?php the_time('F j, Y'); ?></strong></p>
				<?php if ( has_post_thumbnail() ) { /* Loads the post's featured thumbnail, requires Wordpress 3.0+ */ echo '<div class="featured-thumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
				<div class="post-content">
					<?php the_content(); ?>
					<?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?>
				</div><!-- post-content -->
			</article>

			<?php /* If a user fills out their bio info, it's included here */ ?>

		</div><!-- post -->

		<div class="newer-older">
			<p class="older"><?php previous_post_link('%link', '&laquo; Previous post') ?>
			<p class="newer"><?php next_post_link('%link', 'Next Post &raquo;') ?></p>
		</div><!-- newer-older -->
		<div class="clear"></div><!-- clear -->


	<?php endwhile; /* end loop */ ?>

			</div><!-- page-content -->
    	</div><!-- left-column -->
    	
    	<div class="fourcol" id="right-column">
    		<?php get_sidebar(); ?>	
    	</div><!-- right-column -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->

<?php get_footer(); ?>