 <div class="tabs">
    <ul class="tab-links">
        <li class="active"><a href="#tab1">Top Individuals</a></li>
        <li><a href="#tab2">Top<br>Teams</a></li>
    </ul>
 
    <div class="tab-content">
        <div id="tab1" class="tab active">

<?php

$content = file_get_contents("http://my.e2rm.com/webgetservice/get.asmx/getParticipantScoreBoard?eventID=162348&languageCode=en-CA&sortBy=onlineAmount&listItemCount=10&externalQuestionID=&externalAnswerID=&Source=");
$xml = simplexml_load_string($content) or die("Error: Cannot create object");
// echo '<pre>';
// var_dump($xml->ParticipantScoreBoard_collection->ParticipantScoreBoard);
// echo '</pre>';
$arr = $xml->ParticipantScoreBoard_collection->ParticipantScoreBoard;
$output = "<ol>";

foreach($arr as $value)
{
	if($value->IsAnonymous=='y'){
		$output.= "<li>Anonymous - ".$value->onlineTotalCollected."</li>";
		continue;
	}
	$output.= "<li>".$value->ParticipantFirstName." ".$value->ParticipantLastName." - $".$value->onlineTotalCollected."</li>";
}

$output .= "</ol>";

echo $output;
?>
		
        </div>
 
        <div id="tab2" class="tab">
			
<?php

$content = file_get_contents("http://my.e2rm.com/webgetservice/get.asmx/getTeamScoreBoard?eventID=162348&languageCode=en-CA&sortBy=onlineCount&listItemCount=10&externalQuestionID=&externalAnswerID=&Source=");
$xml = simplexml_load_string($content) or die("Error: Cannot create object");
// echo '<pre>';
// var_dump($xml->ParticipantScoreBoard_collection->ParticipantScoreBoard);
// echo '</pre>';
$arr = $xml->TeamScoreBoard_collection->TeamScoreBoard;
$output = "<ol>";

foreach($arr as $value)
{
	if($value->IsAnonymous=='y'){
		$output.= "<li>Anonymous - ".$value->OnlineTotalCollected."</li>";
		continue;
	}
	$output.= "<li>".$value->TeamName." - $".$value->OnlineTotalCollected."</li>";
}

$output .= "</ol>";

echo $output;
?>
        </div>
 
    </div>
</div>
