<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title('', true,''); ?></title>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<!-- CSS
================================================== -->
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/responsive.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/meanmenu.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/images/a4w_social_media_icon_57x57.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/images/a4w_social_media_icon_72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/images/a4w_social_media_icon_114x114.png">


<!-- RSS
================================================== -->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'atom_url' ); ?>" />


<!-- JS
================================================== -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


<!-- Google Fonts
================================================== -->

<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


<!-- Facebook
================================================== -->

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=1527544094161328&autoLogAppEvents=1"></script>

<!-- Responsive Tables
================================================== -->

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-responsiveTables.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.responsiveText.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('.responsive').not('table').responsiveText();
    $('table.responsive').responsiveTables();
    });
</script>



<?php wp_head(); ?> <?php /* This allows many Wordpress features & plugins to function properly. */ ?>
</head>

<body <?php body_class(); ?>>

<div class="container">

	<div class="row">
		<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
		<div class="fourcol" id="logo">
            <?php /* Alt logo: /wp-content/uploads/2020/01/a4w_logo_400x225.jpg */ ?>
			<a title="Home" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2020/08/A4W_Logo_gwark_circled_transparent_400x225-1.png"></a><br>
			In support of the <a target="_blank" href="https://albertawilderness.ca">Alberta Wilderness Association</a>
		</div>
		<!-- logo -->
		<div class="sixcol" id="event-date">
			<!--<span class="date">Climb for Wilderness</span>
			    <span style="font-size:smaller;">is now</span><br>-->
			<span class="location">Adventures for Wilderness</span>
		</div>
		<div class="fourcol top-buttons" style="padding-top:20px;">
			<a id="link-register" title="Browse" href="/adventures">Adventures</a>
			<a id="link-donate" title="Donate" href="/donate">Donate</a>
		</div>
		<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->
    
	<div class="row">
		<div class="sixteencol" id="navigation">
			<header>
				<nav>
					<?php wp_nav_menu( array('menu' => 'Main Menu' )); ?>
				</nav>
			</header>
		</div><!-- navigation -->
    </div><!-- row -->
