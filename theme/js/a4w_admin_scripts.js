document.addEventListener( 'DOMContentLoaded', function() {
    var yr = new Date().getFullYear();
    
    var e = document.getElementById( 'a4w-slug-year' );
    if (e !== null) {
        e.innerHTML = yr;
    }
    
    var editor_div = document.getElementById( 'adv_slug_editor' );
    if (editor_div !== null) {
        var wrapper_1 = editor_div.getElementsByClassName( 'acf-input' )[ 0 ];
        var wrapper_2 = wrapper_1.getElementsByClassName( 'acf-input-wrap' )[ 0 ];
        var e_input = wrapper_2.getElementsByTagName( 'input' )[ 0 ];
        e_input.placeholder = 'a4w' + yr + '-<adventure_name>';
    }
} );
