<?php
/*
Template Name: Participant Sponsor Page
*/
?>

<?php
$nl = "\n";

$booking_id = FALSE;
$fullname = 'a Participant';
$page_body = 'Unknown Adventure participant: We are unable to find any information for that participant.';


$part_spec = explode( '/', $_GET[ 'participant' ] );
if (count( $part_spec ) == 2) {
    list( $part_idspec, $part_name ) = $part_spec;
    if (is_numeric( $part_idspec )) {
        $idmask = 10 ** (strlen( $part_idspec ) - 8);

        $part_token = floor( $part_idspec / $idmask );
        $booking_id = $part_idspec % $idmask;
    }
}

if ($booking_id) {
    $r = a4w_verify_participant_for_sponsorship( $booking_id, $part_token, $part_name );
    if (! $r[ 0 ]) {
        $page_body = $r[ 1 ];
        $booking_id = FALSE;
    } else {
        $details = $r[ 1 ];
    }
}

if ($booking_id) {
    $fullname = a4w_propercase( $details[ 'fullname' ] );
    $event_id = $details[ 'adventure' ][ 'id' ];
    $adv_date_sc = '[show-a4w-event-date eventid="' . $event_id . '" formattime="false" nodate=""]';
    $adv_date = do_shortcode( $adv_date_sc );
    
    $donate_url = '/donate?adventure=' . $details[ 'adventure' ][ 'slug' ] . '&message=' . urlencode( $fullname );
    $adventure_url = '/adventures/' . $details[ 'adventure' ][ 'slug' ];
    
    $adventure_excerpt = get_the_excerpt( $details[ 'adventure' ][ 'postid' ] );
    $thumbnail_url = get_the_post_thumbnail_url( $details[ 'adventure' ][ 'postid' ], 'medium' );
    
    $sponsors_list = '';
    if ($details[ 'sponsors' ]) {
        $sp_total = 0;
        $sponsors_list = '<div id="sponsorship-history">' . $nl .
                         '    <p>Thanks to everyone who has already sponsored me!</p>' . $nl .
                         '    <div id="existing-sponsors">' . $nl;
        foreach ($details[ 'sponsors' ] as $sponsorship) {
            list( $sp_date, $sp_name, $sp_amount ) = $sponsorship;
            $sp_total += $sp_amount;
            $sponsors_list .= '        ' .
                              '<span class="sponsorship-date">' . date( 'M j', strtotime( $sp_date ) ) . '</span>' .
                              '<span class="sponsorship-name">' . $sp_name . '</span>' . 
                              '<span class="sponsorship-amount">$' . number_format( $sp_amount, 2 ) . '</span><br>' . $nl;
        }
        $sponsors_list .= '        <div style="height:4px;width:100%;border-bottom:1px solid black;"></div>' . $nl .
                          '        ' .
                          '<span class="sponsorship-date">&nbsp;</span>' .
                          '<span class="sponsorship-name">TOTAL TO DATE:</span>' .
                          '<span class="sponsorship-amount">$' . number_format( $sp_total, 2 ) . '</span><br>' . $nl .
                          '    </div>' . $nl .
                          '</div>' . $nl;
    }
    
    $page_body = '<table class="sponsorship-context"><thead>' . $nl .
                 '    <td style="width:50%;">' . $nl .
                 '        <h3>About Me</h3>' . $nl .
                 '    </td><td style="width:50%;">' . $nl .
                 '        <h3>About My Adventure</h3>' . $nl .
                 '    </td>' . $nl .
                 '</thead><tbody><tr>' . $nl .
                 '    <td style="width:50%;">' . $nl .
                 '        My name is ' . $part_name . ', and I’m going on an Adventure for wilderness! ' . $nl .
                 '        On ' . $adv_date . ', I’m taking part in <i>' . $details[ 'adventure' ][ 'name' ] . '</i>. ' . $nl .
                 '        As part of this adventure, I’m raising money for <a href="https://www.albertawilderness.ca/">Alberta ' . $nl .
                 '        Wilderness Association</a> (AWA) to help protect Alberta’s wild spaces, wildlife and wild waters.<br><br>' . $nl .
                 '        You can help me support this great cause by clicking the link below. All donations will receive a ' . $nl .
                 '        charitable tax receipt.<br><br>' . $nl .
                 '        <a href="' . $donate_url . '" target="_blank" class="donate-link">Click here to sponsor ' . $fullname . '.</a>' . $nl .
                 '    </td><td style="width:50%;">' . $nl .
                 '        <a href="' . $adventure_url . '"><b>' . $details[ 'adventure' ][ 'name' ] . '</b></a><br><br>' . $nl .
                 '        <p style="font-size:smaller;"><a href="' . $adventure_url . '"><img src="' . $thumbnail_url . '"></a>' . $nl .
                 '            ' . $adventure_excerpt . $nl .
                 '        </p>' . $nl .
                 '    </td>' . $nl .
                 '</tr></tbody></table><br>' . $nl . $nl .
                 $sponsors_list;
} else {
    $page_body .= '3?<br><pre>[' . $booking_id . ']' . $nl . print_r( $details, TRUE ) . '</pre>';
}
?>

<?php get_header(); ?>

    <div class="row">
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="left-column">
    		<div class="page-content">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<h2><?php echo 'Sponsor ' . $fullname ?></h2>
					<?php the_content(); ?>
                    <hr>
                    <?php echo $page_body; ?>
				<?php endwhile; ?>
			</div><!-- page-content -->
    	</div><!-- left-column -->
    	
    	<div class="fourcol" id="right-column">
    		<?php get_sidebar(); ?>	
    	</div><!-- right-column -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->

<?php get_footer(); ?>
