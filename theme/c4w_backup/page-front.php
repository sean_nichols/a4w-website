<?php
/*
Template Name: Front Page
*/
?>
<?php get_header(); ?>

    <div class="row front-row" >
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="left-column">
    		<div id="banner" class="banner-video">
	    		<!-- <video loop muted autoplay poster="http://www.climbforwilderness.ca/wp/wp-content/themes/climb2015/images/Front-Banner_Training_Socks.jpg" class="fullscreen-bg__video">
					<source src="http://www.climbforwilderness.ca/wp/wp-content/themes/climb2015/images/training_socks.mp4" type="video/mp4">
				</video> -->
    			<img class="video-title" align="Climb for Wilderness" src="<?php bloginfo('template_url'); ?>/images/banner-seeyou2020.jpg">
    		</div><!-- banner -->
    		<div id="news-stories">
	    		
<?php /* variable for alternating post styles */
$oddpost = 'story-one';
?>
<?php $the_query = new WP_Query( 'showposts=2' ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

<div class="<?php echo $oddpost; ?>">
	<div class="story-text">
		<h3><?php the_title(); ?></h3>
		<?php the_excerpt(); ?> 
	</div><!-- story-text -->
	<div class="story-link">
		<a href="<?php the_permalink() ?>">Continue Reading</a>
	</div><!-- story-link -->
</div><!-- story-one -->


<?php /* Changes every other post to a different class */
$oddpost = 'story-one';
if ('story-one' == $oddpost) $oddpost = 'story-two';
else $oddpost = 'story-one';
?>
<?php endwhile;?>
<?php wp_reset_query(); ?>

    		</div><!-- news-stories -->    	
    		<div class="read-more-news">
    			<p><a href="<?php bloginfo('url'); ?>/news">Read More News Updates »</a></p>
    		</div><!-- read-more-news -->
    		<div class="sponsors">
    			<h2>Our Supporters</h2>
    			<div class="sponsor-logos">
	    			
    				<?php if(get_field('sponsor_logo')): ?>
                    <?php while(has_sub_field('sponsor_logo')): ?>
                    <a target="_blank" href="<?php the_sub_field('supporter_link') ?>" style="visibility: <?php the_sub_field('show_hide') ?>">
                    <img src="<?php the_sub_field('logo_image') ?>"></a>
                    <?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
					<center><a href="http://climbforwilderness.ca/about/corporate-supporters/">View our full list of supporters</a></center>
    			</div>
    		</div><!-- sponsors -->
    		<div class="footer-nav">
    			<?php wp_nav_menu( array('menu' => 'Footer Menu' )); ?>
    		</div><!-- footer-nav -->
    	</div><!-- left-column -->
    	
    	<div class="fourcol" id="right-column">
    		<?php get_sidebar(); ?>	
    	</div><!-- right-column -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->

<?php get_footer(); ?>
