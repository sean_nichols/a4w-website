   		<div id="important-dates">
    			<h4 class="green-text">IMPORTANT DATES</h4>
    			<p><strong>Registration Open</strong> <span class="right-text">Dec 18, 2018</span></p>
				<p><strong>Registration Closed</strong>	<span class="right-text">Apr 27, 2019</span></p>
				<p><strong>Event Day</strong> <span class="right-text">Apr 27, 2019</span></p>
				<br>
				<h4 class="blue-text">EVENT DAY TIMES</h4>
				<p><strong>Registration Desk</strong> <span class="right-text">8:00am</span></p>
				<p><strong>Climb</strong> <span class="right-text">8:30am - 11:30am</span></p>
				<p><strong>Elite Stair Runners</strong> <span class="right-text">10:30am</span></p>
				<br>
				<p><strong>Please note:</strong> A $5 surcharge will be added for registering after 9:00PM Friday, April 26th.</p>
    		</div><!-- important-dates -->
    		
    		<div class="sub-nav">
 
 <?php if ( ! dynamic_sidebar( 'Sub Navigation' )) : ?>

<?php endif; ?>
 
     		<div id="top-ten">
    <h4><li class="active">Top Individuals </h4>

<?php

$content = file_get_contents("http://my.e2rm.com/webgetservice/get.asmx/getParticipantScoreBoard?eventID=275633&languageCode=en-CA&sortBy=onlineAmount&listItemCount=10&externalQuestionID=&externalAnswerID=&Source=");
$xml = simplexml_load_string($content) or die("Error: Cannot create object");
// echo '<pre>';
// var_dump($xml->ParticipantScoreBoard_collection->ParticipantScoreBoard);
// echo '</pre>';
$arr = $xml->ParticipantScoreBoard_collection->ParticipantScoreBoard;
$output = "<ol>";

foreach($arr as $value)
{
	$money = $value->onlineTotalCollected;
	$mon_arr = explode('.', $money);
	//echo count($mon_arr[1]);
	if (strlen($mon_arr[1])==4) {
		$money = substr($money, 0, -2);
	}
	if($value->IsAnonymous=='y'){
		$output.= "<li>Anonymous: $".$money."</li>";
		continue;
	}
	$output.= "<li>".$value->ParticipantFirstName." ".$value->ParticipantLastName."&nbsp; $".$money."</li>";
}

$output .= "</ol>";

echo $output;

?>
 
	   		</div><!-- top-ten -->

 
	   		</div><!-- top-ten -->
    		<div id="newsletter">
<h4>Sign Up for C4W News!</h4>
<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://AlbertaWilderness.us17.list-manage.com/subscribe/post?u=d32dee74e9b17a2745430f0ac&amp;id=9784013cd5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<h2>Subscribe to our mailing list</h2>
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME">First Name  <span class="asterisk">*</span>
</label>
	<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE3">Company </label>
	<input type="text" value="" name="MMERGE3" class="" id="mce-MMERGE3">
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE4">City </label>
	<input type="text" value="" name="MMERGE4" class="" id="mce-MMERGE4">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d32dee74e9b17a2745430f0ac_9784013cd5" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
    		</div><!-- newsletter -->
    		<div id="facebook">
				<div class="fb-like-box" data-href="https://www.facebook.com/Climb-and-Run-for-Wilderness-202085660139792/" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
    		</div><!-- facebook -->
    		<div id="twitter">
    			<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/Climb4Wild" data-widget-id="422838832002637825">Tweets by @Climb4Wild</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    		</div><!-- twitter -->
