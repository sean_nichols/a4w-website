<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title('', true,''); ?></title>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<!-- CSS
================================================== -->
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/responsive.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/meanmenu.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/images/apple-touch-icon-114x114.png">


<!-- RSS
================================================== -->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'atom_url' ); ?>" />


<!-- JS
================================================== -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


<!-- Google Fonts
================================================== -->

<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


<!-- Facebook
================================================== -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=154539254637602&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Responsive Tables
================================================== -->

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-responsiveTables.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.responsiveText.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('.responsive').not('table').responsiveText();
    $('table.responsive').responsiveTables();
    });
</script>



<?php wp_head(); ?> <?php /* This allows many Wordpress features & plugins to function properly. */ ?>
</head>

<body <?php body_class(); ?>>

<div class="container">

	<div class="row">
		<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
		<div class="fourcol" id="logo">
			<a title="Home" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2016/12/logo_c4w_black.png"></a><br>
			In support of the <a target="_blank" href="https://albertawilderness.ca">Alberta Wilderness Association</a>
		</div>
		<!-- logo -->
		<div class="sixcol" id="event-date">
			<span class="date">Saturday, April 27, 2019</span><br>
			<span class="location">The Bow Building</span><br>
			500 Centre St SE, Calgary, AB
		</div>
		<div class="fourcol top-buttons">
			<a id="link-register" title="Register" href="https://secure.e2rm.com/registrant/LoginRegister.aspx?eventid=275633&action=register">Register</a>
			<a id="link-donate" title="Donate" href="https://secure.e2rm.com/registrant/search.aspx?EID=275633&Lang=en-CA&Referrer=http%3a%2f%2fclimbforwilderness.ca%2fwp%2f&searchState=individualSearch">Donate</a>
		</div>
		<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->
    
	<div class="row">
		<div class="sixteencol" id="navigation">
			<header>
				<nav>
					<?php wp_nav_menu( array('menu' => 'Main Menu' )); ?>
				</nav>
			</header>
		</div><!-- navigation -->
    </div><!-- row -->
