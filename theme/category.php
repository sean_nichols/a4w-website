<?php get_header(); ?>

	<div class="sixteen columns" id="banner">
		<?php if ( ! dynamic_sidebar( 'Header' ) ) : ?><!-- Header Widget --><?php endif ?>
	</div><!-- banner -->
	
	<div class="ten columns" id="page-content">

	<h1><?php printf( __( 'Category Archives: %s' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
	<?php echo category_description(); /* Displays the category's description from the Wordpress admin */ ?>
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post-single">
			<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			<?php if ( has_post_thumbnail() ) { /* Loads the post's featured thumbnail, requires Wordpress 3.0+ */ echo '<div class="featured-thumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
	
			<p><?php _e('Written on '); the_time('F j, Y'); _e(' at '); the_time(); _e(', by '); the_author_posts_link() ?></p>
			<div class="post-excerpt">
				<?php the_excerpt(); /* The excerpt is loaded to help avoid duplicate content issues */ ?>
			</div>
	
			<div class="post-meta">
				<p><?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></p>
				<p><?php _e('Categories:'); ?> <?php the_category(', ') ?></p>
				<p><?php if (the_tags('Tags: ', ', ', ' ')); ?></p>
			</div><!-- post-meta-->
		</div><!-- post-single -->
	<?php endwhile; else: ?>
		<div class="no-results">
			<p><strong><?php _e('There has been an error.'); ?></strong></p>
			<p><?php _e('We apologize for any inconvenience, please hit back on your browser or use the search form below.'); ?></p>
			<?php get_search_form(); /* Outputs the default Wordpress search form */ ?>
		</div><!-- no-results -->
	<?php endif; ?>
		
	<div class="oldernewer">
		<p class="older"><?php next_posts_link('&laquo; Older Entries') ?></p>
		<p class="newer"><?php previous_posts_link('Newer Entries &raquo;') ?></p>
	</div><!-- oldernewer-->
	
	</div><!-- page-content -->

	<div class="six columns" id="sidebar">
<?php get_sidebar(); ?>	
	</div><!-- sidebar -->

<?php get_footer(); ?>