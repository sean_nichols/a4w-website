<?php get_header(); ?>
    <style>
.accordion-title {
    cursor: pointer;
}
.accordion-title::after {
    content: "⮞";
}
.accordion-title.open::after {
    content: "⮟";
}
.featured-thumbnail img {
    border: 2px solid #2e633d;
    max-width: 400px;
    max-height: 300px;
    width: auto;
    height: auto;
}
.therm_caption {
    width: 290px;
    margin: 20px 0px 0px 0px;
    padding-left: 40px;
    border-width: 1px 0px 0px 0px;
    border-style: solid;
    border-color: #b0b0b0;
}
.therm_container {
    width: 250px;
    height: 300px;
    margin: 0px 40px 10px;
    position: relative;
}
.therm_bg {
    position: absolute;
}
.therm_fill_container {
    width: 250px;
    overflow: hidden;
    position: absolute;
}
.therm_fill_topline {
    top: 0px;
    left: 2px;
    width: 187px;
    height: 1px;
    border-width: 2px 0px 0px 0px;
    border-color: #488d5c;
    border-style: dashed;
    position: absolute;
}
.em-osm-container {
    border: 2px solid #2e633d;
}
    </style>
    
    <div class="row">
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="left-column">
    		<div class="page-content">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

			<article>
				<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<?php if ( has_post_thumbnail() ) { /* Loads the post's featured thumbnail, requires Wordpress 3.0+ */ echo '<div class="featured-thumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
				<div class="post-content">
					<?php the_content(); ?>
					<?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?>
				</div><!-- post-content -->
			</article>

			<?php /* If a user fills out their bio info, it's included here */ ?>

		</div><!-- post -->

		<div class="newer-older">
			<p class="older"><?php previous_post_link('%link', '&laquo; Previous Adventure') ?>
			<p class="newer"><?php next_post_link('%link', 'Next Adventure &raquo;') ?></p>
		</div><!-- newer-older -->
		<div class="clear"></div><!-- clear -->


	<?php endwhile; /* end loop */ ?>

			</div><!-- page-content -->
    	</div><!-- left-column -->
    	
    	<div class="fourcol" id="right-column">
    		<?php get_sidebar(); ?>	
    	</div><!-- right-column -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->

<?php get_footer(); ?>