<?php get_header(); ?>

    <div class="row">
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="left-column">
    		<div class="page-content">
				
				<h1><?php _e('Error 404 Not Found'); ?></h1>
				<p><?php _e('Nobody is perfect. The page cannot be found.'); ?></p>
				<p><?php _e('Please check your URL or use the search form below.'); ?></p>
				<?php get_search_form(); /* Outputs the default Wordpress search form */ ?>
				
			</div><!-- page-content -->
    	</div><!-- left-column -->
    	
    	<div class="fourcol" id="right-column">
    		<?php get_sidebar(); ?>	
    	</div><!-- right-column -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->

<?php get_footer(); ?>