<?php
/*
Template Name: Supporters Page
*/
?>
<?php get_header(); ?>
<style>
table#a4w_top_individual_sponsors img {
    margin: 20px 0px 0px 40px;
}
</style>

    <div class="row">
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="left-column">
    		<div class="page-content">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<h2><?php the_title(); ?></h2>
 				
                    <h3>Corporate Supporters</h3>
					<div class="sponsor-logos">
    				<?php if(get_field('sponsor_logo', 2888)): ?>
                    <?php while(has_sub_field('sponsor_logo', 2888)): ?>
                    <?php
                        $display = get_sub_field( 'show_hide' );
                        if ($display == 'initial') {
                            $display = 'inline-block';
                        }
                    ?>
                    <a target="_blank" href="<?php the_sub_field('supporter_link') ?>" style="display: <?php echo $display ?>">
                    <img src="<?php the_sub_field('logo_image') ?>"></a>
                    <?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
    			</div>

					<?php the_content(); ?><!-- sponsor-logos -->
				<?php endwhile; ?>
			</div><!-- page-content -->
    	</div><!-- left-column -->
    	
    	<div class="fourcol" id="right-column">
    		<?php get_sidebar(); ?>	
    	</div><!-- right-column -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->

<?php get_footer(); ?>
