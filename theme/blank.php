<?php /* Template Name: Blank Page Template */ ?>
<?php $pagetitle = 'Climb for Wilderness | Calgary, Alberta -- ' . the_title( '', '', FALSE ); ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8" />
    <title><?php echo $pagetitle; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="https://climbforwilderness.ca/wp-content/themes/climb2015/images/favicon.ico">
    <link rel="apple-touch-icon" href="https://climbforwilderness.ca/wp-content/themes/climb2015/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://climbforwilderness.ca/wp-content/themes/climb2015/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://climbforwilderness.ca/wp-content/themes/climb2015/images/apple-touch-icon-114x114.png">
    <meta name="description" content="Climb for Wilderness is Alberta Wilderness Association&#039;s annual Earth Day event at the Bow Tower."/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="https://climbforwilderness.ca/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Climb for Wilderness is Alberta Wilderness Association&#039;s annual Earth Day event at the Bow Tower." />
    <meta property="og:url" content="https://climbforwilderness.ca/" />
    <meta property="og:site_name" content="Adventures For Wilderness" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Climb for Wilderness is Alberta Wilderness Association&#039;s annual Earth Day event at the Bow Tower." />
<?php
    echo '    <meta property="og:title" content="' . $pagetitle . '" />' . "\n";
    echo '    <meta name="twitter:title" content="' . $pagetitle . '" />' . "\n";
?>
    <style>
body, p, h1 {
    font-family: helvetica, arial, sans-serif;
    color: #666666;
}
body, p {
    font-size: 14px;
    line-height: 1.43;
}
h1 {
    font-size: 36px;
    font-weight: 500;
    line-height: 1.1;
}
    </style>
</head>
<body>

<?php
    the_post();
    the_content();
?>

</body>
</html>
