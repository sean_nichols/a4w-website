<?php

include 'shortcodes.php';
include 'shortcodes_timing.php';

	// enables wigitized sidebars
	if ( function_exists('register_sidebar') )

	// Sidebar Widget
	// Location: the sidebar
	register_sidebar(array('name'=>'Sidebar',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	// Sub Navigation Widget
	// Location: right after the navigation
	register_sidebar(array('name'=>'Sub Navigation',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
    
    // Styling and scripts for Admin pages
    function add_admin_theme_custom() {
        wp_enqueue_style( 'a4w_admin_style', get_stylesheet_directory_uri() . '/a4w_admin_style.css' );
        wp_enqueue_script( 'a4w_admin_scripts', get_template_directory_uri() . '/js/a4w_admin_scripts.js' );
    }
    add_action( 'admin_enqueue_scripts', 'add_admin_theme_custom' );
    
// allows php in your text widgets	
add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}

// post thumbnail support
add_theme_support( 'post-thumbnails' );
// adds the post thumbnail to the RSS feed
function cwc_rss_post_thumbnail($content) {
    global $post;
    if(has_post_thumbnail($post->ID)) {
        $content = '<p>' . get_the_post_thumbnail($post->ID) .
        '</p>' . get_the_content();
    }
    return $content;
}
add_filter('the_excerpt_rss', 'cwc_rss_post_thumbnail');
add_filter('the_content_feed', 'cwc_rss_post_thumbnail');

// custom menu support
add_theme_support( 'menus' );
if ( function_exists( 'register_nav_menus' ) ) {
    register_nav_menus(
        array(
          'header-menu' => 'Header Menu',
          'sidebar-menu' => 'Sidebar Menu',
          'footer-menu' => 'Footer Menu',
          'logged-in-menu' => 'Logged In Menu'
        )
    );
}

// adds Post Format support
// learn more: http://codex.wordpress.org/Post_Formats
// add_theme_support( 'post-formats', array( 'aside', 'gallery','link','image','quote','status','video','audio','chat' ) );

// removes detailed login error information for security
add_filter('login_errors',create_function('$a', "return null;"));

// removes the WordPress version from your header for security
function wb_remove_version() {
    return '<!--built on the Whiteboard Framework-->';
}
add_filter('the_generator', 'wb_remove_version');


// Removes Trackbacks from the comment count
add_filter('get_comments_number', 'comment_count', 0);
function comment_count( $count ) {
    if ( ! is_admin() ) {
        global $id;
        $comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
        return count($comments_by_type['comment']);
    } else {
        return $count;
    }
}

// invite rss subscribers to comment
function rss_comment_footer($content) {
    if (is_feed()) {
        if (comments_open()) {
            $content .= 'Comments are open! <a href="'.get_permalink().'">Add yours!</a>';
        }
    }
    return $content;
}

// custom excerpt ellipses for 2.9+
function custom_excerpt_more($more) {
    return 'Read More &raquo;';
}
add_filter('excerpt_more', 'custom_excerpt_more');
// no more jumping for read more link
function no_more_jumping($post) {
    return '<a href="'.get_permalink($post->ID).'" class="read-more">'.'&nbsp; Continue Reading &raquo;'.'</a>';
}
add_filter('excerpt_more', 'no_more_jumping');

// category id in body and post class
function category_id_class($classes) {
    global $post;
    foreach((get_the_category($post->ID)) as $category)
        $classes [] = 'cat-' . $category->cat_ID . '-id';
        return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');

// adds a class to the post if there is a thumbnail
function has_thumb_class($classes) {
    global $post;
    if( has_post_thumbnail($post->ID) ) { $classes[] = 'has_thumb'; }
        return $classes;
}
add_filter('post_class', 'has_thumb_class');

// add_action( 'admin_init', 'theme_options_init' );
// add_action( 'admin_menu', 'theme_options_add_page' );

// Allow KML / KMZ uploads to Media
function allow_kml_mime_types( $mime_types ){
    return array_merge( $mime_types, [
		'kml' => 'application/vnd.google-earth.kml+xml',
	    'kmz' => 'application/vnd.google-earth.kmz'	
	] );
}
add_filter( 'upload_mimes', 'allow_kml_mime_types', 1, 1 );

// Init plugin options to white list our options
// function theme_options_init(){
// 	register_setting( 'tat_options', 'tat_theme_options', 'theme_options_validate' );
// }

// Load up the menu page
// function theme_options_add_page() {
// 	add_theme_page( __( 'Theme Options', 'tat_theme' ), __( 'Theme Options', 'tat_theme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
// }

// begin LifeGuard Assistant
// learn more about the LifeGuard Assistant: http://wplifeguard.com/lifeguard-plugin/
// learn more about the affiliate program: http://wplifeguard.com/affiliates/
add_action('admin_menu', 'lgap_add_pages');
function lgap_add_pages() {
    add_menu_page(__('Help','menu-test'), __('Help','menu-test'), 'read', 'lifeguard-assistant-plugin', 'lgap_main_page' );
}
function lgap_main_page() {
    echo "<h2>" . __( 'Help', 'menu-test' ) . "</h2>";
    // place your affiliate ID between the " on the following line
    $lgap_aff = "";
    // get your affiliate ID here: http://wplifeguard.com/wp-admin/profile.php?page=affiliateearnings
    echo '
    <style type="text/css">
        #wplg { font-family: "Varela",Helvetica,Trebuchet MS,Verdana,"DejaVu Sans",sans-serif; }
        #wplg a:link,#wplg a:visited { color: #21759b; text-decoration: none; }
        #wplg a:hover { color: #d54e21; }
        .wplg-video { background: #f6f6f6; border: 1px solid #dadada; padding: 12px; margin: 0 12px 12px 0; float: left; }
        .wplg-clear { clear: both; }
        .wplg-green-button { box-shadow:inset 0 0 3px rgba(0,0,0,.1); font-size: 20px; line-height: 32px; height: 32px; width: 434px; margin: 0 12px 12px 0; text-align: center; display: block; border: 2px solid #9abf89; background: #7da742; color: #f1ffeb !important; text-shadow: 0 0 3px rgba(125,167,66,.75); }
        .wplg-green-button:hover { border: 2px solid #c0e1aa; background: #8ac636; }
        .wplg-green-button:active { border: 2px solid #88a65e; background: #5d822a; }
    </style>
    <link href="http://fonts.googleapis.com/css?family=Varela" rel="stylesheet" type="text/css">
    <div id="wplg">
        <p>Need help with WordPress? Here is a collection of free WordPress video tutorials from <a href="http://wplifeguard.com/'.$lgap_aff.'">wpLifeGuard</a> to help you get going. <a href="http://wplifeguard.com/get-access/'.$lgap_aff.'">Get access to more videos.</a></p>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32852753?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32856785?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32857648?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32860297?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32872861?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32878118?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32881530?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32864178?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32863614?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32862744?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-video"><iframe src="http://player.vimeo.com/video/32857481?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="412" height="309" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
        <div class="wplg-clear"></div>
        <a class="wplg-green-button" href="http://wplifeguard.com/get-access/'.$lgap_aff.'">Get Full Access Now</a>
    </div>
    ';
}
// end LifeGuard Assistant

?>