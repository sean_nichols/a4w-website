<?php

$content = file_get_contents("http://my.e2rm.com/webgetservice/get.asmx/getParticipantScoreBoard?eventID=162348&languageCode=en-CA&sortBy=onlineAmount&listItemCount=10&externalQuestionID=&externalAnswerID=&Source=");
$xml = simplexml_load_string($content) or die("Error: Cannot create object");
// echo '<pre>';
// var_dump($xml->ParticipantScoreBoard_collection->ParticipantScoreBoard);
// echo '</pre>';
$arr = $xml->ParticipantScoreBoard_collection->ParticipantScoreBoard;
$output = "<ul>";

foreach($arr as $value)
{
	if($value->IsAnonymous=='y'){
		$output.= "<li>Anonymous Total collected:".$value->onlineTotalCollected."</li>";
		continue;
	}
	$output.= "<li>".$value->ParticipantFirstName." ".$value->ParticipantLastName." Total collected:".$value->onlineTotalCollected."</li>";
}

$output .= "</ul>";

echo $output;
?>