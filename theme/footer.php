    <div class="row bottom-links">
    	<div class="onecol first spacer">&nbsp;</div><!-- spacer -->
    	<div class="tencol" id="copyright">
    		© <?php echo date("Y") ?> Alberta Wilderness Association  <span class="divider">|</span>  <a href="<?php bloginfo('url'); ?>/privacy-policy">Privacy Policy</a>
    	</div><!-- copyright -->
    	<div class="fourcol" id="build-studio">
    		Designed by <a rel="nofollow" target="_blank" title="Build Studio Inc." href="https://buildstudio.ca">Build Studio</a>
    	</div><!-- build-studio -->
    	<div class="onecol last spacer">&nbsp;</div><!-- spacer -->
    </div><!-- row -->
    
</div><!-- container -->

<script src="<?php bloginfo('template_url'); ?>/js/jquery.meanmenu.js"></script> 

<script>
	jQuery(document).ready(function () {
    	jQuery('header nav').meanmenu();
	});
</script>

<script>
jQuery(document).ready(function() {
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
 
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
});
</script>

<!-- OVERRIDE CSS -->

<style>

/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */
@media only screen and (min-width: 480px) and (max-width: 767px) {


}

/* Mobile Portrait Size to Mobile Landscape Size (devices and browsers) */
@media only screen and (max-width: 479px) {


}

</style>

<!-- Google Analytics
================================================== -->

<SCRIPT TYPE="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push( [ '_setAccount', 'UA-10796814-5' ] );
        _gaq.push( [ '_trackPageview' ] );

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = 'https://www.google-analytics.com/ga.js';
    
            var s = document.getElementsByTagName( 'script' )[ 0 ];
            s.parentNode.insertBefore( ga, s );
        })();
    </SCRIPT>

<?php wp_footer(); /* This allows many Wordpress features & plugins to function properly. */ ?>
</body>
</html>