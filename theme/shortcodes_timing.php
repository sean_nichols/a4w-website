<?php

/**
  * @author Sean Nichols seanni@trichotomy.ca
  **/
 
//
// Added 2017-04-19 - code to include a table of timing results in a page, by means of a shortcode
// Updated 2019-04-26 - allowing the "method" parameter to specify a data retrieval method
//
// [timing-results timeformat="format" sort="timetaken" limit="0" method="static"]
//
// All parameters are optional (defaults as above):
//   * timeformat: "format" -> string formatted as hh:ii:ss 
//                 "second" -> number of seconds taken
//                 "minute" -> number of minutes taken
//                 "hour"   -> number of hours taken
//   * sort: "timetaken"   -> sort by time taken
//           "cardnum"     -> sort by (RFID) card number
//           "climbername" -> sort by climber name ('firstname lastname')
//           "entered"     -> sort by time stairway entered
//           "exited"      -> sort by time stairway exited
//           "-$foo"       -> sort by $foo but reversed (i.e.: in descending order)
//   * limit: "-1" -> include all results
//            "n"  -> limit table to first n results (if n > 0)
//   * method: "static" -> read from (local) static .dat file
//             "sql"    -> read from db via SQL
//             "http"   -> read from db via HTTP rest API
//


function sc_makeTableRow( $card, $name, $time ) {
    return '<tr>' . PHP_EOL .
           '<td style="width: 100px;">' . $card . '</td>' . PHP_EOL .
           '<td style="width: 400px;">' . $name . '</td>' . PHP_EOL .
           '<td style="width: 150px;">' . $time . '</td>' . PHP_EOL .
           '</tr>' . PHP_EOL;
}

function sc_readTimingResults_STATIC( $year ) {
    $filename = 'timing_results' . ($year == '' ? '' : '_' . $year) . '.dat';
    $url = 'https://climbforwilderness.ca/wp-content/uploads/timing_results/' . $filename;

    return explode( PHP_EOL, file_get_contents( $url ) );
}

function sc_readTimingResults_HTTP( $year ) {
    $url = 'https://albertawilderness.ca/c4w/timing.php';
    $api_params = array(
        'action'     => 'results',
        'sort'       => 'timetaken',
        'timeformat' => 'format'
    );
    if ($year != '') {
        $api_params[ 'year' ] = $year;
    }
    $headers = array(
        'Accept'          => 'text/html,text/plain,application/xhtml+xml,application/xml,q=0.9,*/*,q=0.8',
        'Accept-language' => 'en-US,en,q=0.5',
        'User-Agent'      => 'C4W Website Shortcode',
        'Connection'      => 'keep-alive',
        'Pragma'          => 'no-cache',
        'Cache-Control'   => 'no-cache'
    );
    
    $http_opts = array(
        'method' => 'GET'
    );
    $header_str = '';
    foreach ($headers as $k => $v) {
        $header_str .= $k . ': ' . $v . "\r\n";
    }
    $http_opts[ 'header' ] = $header_str;
    
    return explode( PHP_EOL, file_get_contents(
        $url . '?' . http_build_query( $api_params ),
        false,
        stream_context_create( array( $http_opts ) )
    ) );
}

function sc_readTimingResults_SQL( $year ) {
    $dbhost = 'localhost';
    $dbname = 'fw5141110682';
    $dbuser = 'CfWrLckOVy2ZOuhrO6ihsqFS5GSgVc';
    $dbpass = 'db4685546307';
    
    $dsn = 'mysql:host=' . $dbhost . ';dbname=' . $dbname;
    $opts = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        PDO::ATTR_PERSISTENT => false
    );

    try {
        $dbh = new PDO( $dsn, $dbuser, $dbpass, $opts );
        $sql = 'SELECT   cardnum, climbername, TIMEDIFF( exited, entered ) AS timetaken ' .
               'FROM     runs ' .
               'WHERE    exited IS NOT NULL ' .
               'ORDER BY timetaken;';
        $stmt = $dbh->prepare( $sql );
        $stmt->execute();

        $results = array( 'SUCCESS' );
        while ($rs = $stmt->fetch( PDO::FETCH_ASSOC )) {
            array_push( $results, $rs[ 'cardnum' ], $rs[ 'climbername' ], $rs[ 'timetaken' ] );
        }
    } catch (PDOException $e) {
        $results = array( 'FAIL', 'PDO Error ' . $e->getMessage() );
    }
    
    return $results;
}

function sc_endsWith( $haystack, $needle, $case_insensitive = true ) {
    $len = strlen( $needle );
    return (substr_compare( $haystack, $needle, -$len, $len, $case_insensitive ) === 0);
}

function sc_formatOrdinalWith( $name, $suffix ) {
    return substr( $name, 0, -9 ) . '<sup><i>' . $suffix . '</i></sup> climb)';
}

function sc_formatOrdinal( $name ) {
    if (! sc_endsWith( $name, 'climb)' )) {
        return $name;
    }
    
    if (sc_endsWith( $name, '1st climb)' )) {
        return sc_formatOrdinalWith( $name, 'st' );
    } else if (sc_endsWith( $name, '2nd climb)' )) {
        return sc_formatOrdinalWith( $name, 'nd' );
    } else if (sc_endsWith( $name, '3rd climb)' )) {
        return sc_formatOrdinalWith( $name, 'rd' );
    } else if (sc_endsWith( $name, 'th climb)' )) {
        return sc_formatOrdinalWith( $name, 'th' );
    }
    
    return $name;
}

function sc_getTimingResultsTable( $atts, $content, $method ) {
    $defaults = array(
        'timeformat' => 'format',
        'sort'       => 'timetaken',
        'limit'      => -1,
        'year'       => '',
        'method'     => 'static'
    );

    extract( shortcode_atts( $defaults, $atts ) );

    $results = call_user_func( 'sc_readTimingResults_' . strtoupper( $method ), $year );
    if ($results[ 0 ] == 'SUCCESS') {
        $output = '<table style="width: 650px;">' . PHP_EOL .
                  '<thead>' . PHP_EOL .
                  sc_makeTableRow( '<b>Card #</b>', '<b>Climber Name</b>', '<b>Time</b>' ) .
                  '</thead>' . PHP_EOL .
                  '<tbody>' . PHP_EOL;

        $n = count( $results );
        $i = 1;

        do {
            $name = sc_formatOrdinal( trim( $results[ $i + 1 ] ) );
            $output .= sc_makeTableRow( $results[ $i ], $name, $results[ $i + 2 ] );
            $i += 3;
        } while ($i < $n);
        $output .= '</tbody>' . PHP_EOL .
                   '</table>' . PHP_EOL;
    } else {
        $output = $results[ 1 ];
    }
    
    return $output;
}

add_shortcode( 'timing-results', 'sc_getTimingResultsTable' );    

?>