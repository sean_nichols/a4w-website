<?php

/****************************************************************************
 *
 * Various functions, hooks, etc. to be used by customizations for the
 *  Gravity Forms plugin and associated A4W registration, etc. forms.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-02-19
 *
 ****************************************************************************/
 
require_once( __DIR__ . '/a4w-db-functions.php' );
require_once( __DIR__ . '/a4w-admin-functions.php' );

// To display the appropriate fundraising links and text in the confirmation
//  message to adventure participants. We have one shortcode:
//   • show-a4w-participant-fundraising
// It takes two parameters:
//   • The first is either the slug (param "event") or ID (param "eventid") of
//     the A4W event being registered in.
//   • "selfsponsor" is a boolean indicating whether the registrant expressed
//     they wished to self-sponsor (True) or fundraise the amount (False).

add_shortcode( 'show-a4w-participant-fundraising', 'sc_show_a4w_fundraising_links' );

function sc_get_a4w_fundraising_links_text( $eventid, $event, $selfsponsor ) {
    $nl = "\n";
    
    if ($eventid == -1 && $event == '') {
        return [
            'ok'  => FALSE,
            'msg' => 'Adventure not found.'
        ];
    }
    
    $selfsponsor = (strtolower( $selfsponsor ) === 'false' ? FALSE : (bool) $selfsponsor);

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return ['ok' => FALSE, 'msg' => $r[ 1 ]];
    }

    $dbh = $r[ 1 ];
    
    if ($eventid == -1) {
        if ($event != '') {
            $r = a4w_db_get_event_id_from_slug( $dbh, $event );
            if (! $r[ 0 ]) {
                return ['ok' => FALSE, 'msg' => $r[ 1 ]];
            }
            $eventid = $r[ 1 ];
        }
    }
    
    $amounts = a4w_db_get_fundraising_amount( $dbh, $eventid );

    if ($amounts[ 'minimum' ] != -1) {
        $sponstype = 'minimum';
        $amount = $amounts[ 'minimum' ];
    } elseif ($amounts[ 'suggestion' ] != -1) {
        $sponstype = 'suggested';
        $amount = $amounts[ 'suggestion' ];
    } else {
        return ['ok' => FALSE, 'msg' => 'No minimum or suggested fundraising amount defined for this adventure'];
    }

    $sponsorship_presets = [20, 40, 50, 60, 80, 100, 200];
    if (in_array( intval( $amount ), $sponsorship_presets )) {
        $amount_param = '$' . intval( $amount );
    } else {
        $amount_param = 'Other';
    }
    
    $slug = a4w_db_get_event_slug( $dbh, $eventid );
    $output = '<p>';
    if ($selfsponsor) {
        $sponsorlink = '/donate?adventure=' . $slug . '&attribute_sponsorship-amount=' . $amount_param;
        $output .= 'You indicated in your registration that you intend to self-sponsor for the ' .
                   $sponstype . ' sponsorship amount of $' . $amount . '.<br><br>' .
                   'You can make this sponsorship now or at any time using ' .
                   '<a href="' . $sponsorlink . '" target="_blank">Alberta Wilderness Association’s ' .
                   'secure online donation form</a>.<br><br>' .
                   'AWA is a federally-registered not-for-profit charity, and all donations ' .
                   'are 100% tax-receiptable.';
    } else {
        $adventurelink = '/adventures/' . $slug . '/';
        $output .= 'You indicated in your registration that you intend to fundraise from friends ' .
                   'and family for the ' . $sponstype . ' sponsorship amount of $' . $amount . '.<br><br>' .
                   'Feel free to send your sponsors to the adventure page at<br>' .
                   '<a style="display:inline-block;margin-left:40px;" href="' . $adventurelink . '" target="_blank">' .
                   'www.AdventuresForWilderness.ca' . $adventurelink . '</a><br><br>' .
                   'By clicking the "<b>Sponsor this Adventure!</b>" button on that page, they will ' .
                   'be taken to Alberta Wilderness Association’s secure online donation form where ' .
                   'they can make a sponsorship in your name.<br><br>' .
                   'AWA is a federally-registered not-for-profit charity, and all donations ' .
                   'are 100% tax-receiptable.';
    }
    $output .= '</p>';
    
    return ['ok' => TRUE, 'msg' => $output];
}

function sc_show_a4w_fundraising_links( $atts ) {
    $defaults = [
        'eventid'     => -1,
        'event'       => '',
        'selfsponsor' => FALSE
    ];

    extract( shortcode_atts( $defaults, $atts ) );
    
    $r = sc_get_a4w_fundraising_links_text( $eventid, $event, $selfsponsor );
    if (! $r[ 'ok' ]) {
        return '';
    }
    
    return $r[ 'msg' ];
}

?>
