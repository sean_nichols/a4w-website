<?php

/****************************************************************************
 *
 * Various functions, hooks, etc. to be used by customizations for A4W
 *  coordinators' admin pages.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2022-06-01
 *
 ****************************************************************************/

require_once( __DIR__ . '/a4w-db-functions.php' );
require_once( __DIR__ . '/a4w-version.php' );

//
// pdf-lib stuff to allow creating/modifying PDF documents at client end.
//****************************************************************************
add_action( 'wp_enqueue_scripts', 'a4w_setup_pdflib_scripts' );

function a4w_setup_pdflib_scripts() {
    wp_enqueue_script( 'a4w-pdflib-scripts', '/wp-content/mu-plugins/a4w-events/a4w-events-pdf-functions.js', [], A4W_ADMIN_VERSION );
    
    if (A4W_ADMIN_LOGGING_LEVEL >= LOG_DEBUG) {
        wp_enqueue_script( 'pdflib-pdflib', '/wp-content/mu-plugins/pdf-lib/pdf-lib.js', [], A4W_ADMIN_VERSION );
        wp_enqueue_script( 'pdflib-download', '/wp-content/mu-plugins/pdf-lib/download.js', [], A4W_ADMIN_VERSION );
    } else {
        wp_enqueue_script( 'pdflib-pdflib', '/wp-content/mu-plugins/pdf-lib/pdf-lib.min.js', [], A4W_ADMIN_VERSION );
        wp_enqueue_script( 'pdflib-download', '/wp-content/mu-plugins/pdf-lib/download.min.js', [], A4W_ADMIN_VERSION );
    }
}
