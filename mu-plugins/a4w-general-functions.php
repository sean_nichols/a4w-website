<?php

/****************************************************************************
 *
 * General shortcodes and other functions to be used across A4W.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-09-13
 *
 ****************************************************************************/

require_once( __DIR__ . '/a4w-credentials.php' );

define( 'A4W_SITE_URL', 'https://adventuresforwilderness.ca/' );
define( 'A4W_ADVENTURES_URL', A4W_SITE_URL . 'adventures/' );
// define( 'A4W_ADMIN_LOGGING_LEVEL', LOG_INFO );
define( 'A4W_ADMIN_LOGGING_LEVEL', LOG_DEBUG );

define( 'A4W_COORDINATOR_NAME', 'Sean Nichols' );
define( 'A4W_COORDINATOR_EMAIL', 'snichols@abwild.ca' );
define( 'A4W_COORDINATOR_USERID', 1 );


add_shortcode( 'a4w-add-script', 'a4w_sc_add_script' );
add_shortcode( 'a4w-add-stylesheet', 'a4w_sc_add_style' );

function a4w_sc_add_script( $atts, $content = NULL ) {
    $defaults = ['file' => ''];
    
    extract( shortcode_atts( $defaults, $atts ) );
    if (substr( $file, -3 ) === '.js') {
        wp_enqueue_script( 'a4w-admin-script', '/wp-content/mu-plugins/' . $file, [], A4W_ADMIN_VERSION );
    }
}

function a4w_sc_add_style( $atts, $content = NULL ) {
    $defaults = ['file' => ''];
    
    extract( shortcode_atts( $defaults, $atts ) );
    if (substr( $file, -4 ) === '.css') {
        wp_enqueue_style( 'a4w-admin-script', '/wp-content/mu-plugins/' . $file, [], A4W_ADMIN_VERSION );
    }
}

function a4w_propercase( $str ) {
    $words = explode( ' ', strtolower( $str ) );
    foreach (array_keys( $words ) as $i) {
        $word = $words[ $i ];
        if (! in_array( $word, ['and', 'or'] )) {
            if (substr( $word, 0, 2 ) == 'o\'') {
                $word = 'O’' . ucfirst( substr( $word, 2 ) );
            } elseif (substr( $word, 0, 2 ) == 'o’') {
                $word = 'O’' . ucfirst( substr( $word, 2 ) );
            } elseif (substr( $word, 0, 2 ) == 'mc') {
                $word = 'Mc' . ucfirst( substr( $word, 2 ) );
            } elseif (substr( $word, 0, 3 ) == 'mac') {
                $word = 'Mac' . ucfirst( substr( $word, 3 ) );
            } else {
                $word = ucfirst( $word );
            }
            
            $d = strpos( $word, '-' );
            if ($d !== FALSE) {
                $word = substr( $word, 0, $d + 1 ) . ucfirst( substr( $word, $d + 1 ) );
            }
        }
        $words[ $i ] = $word;
    }
    return implode( ' ', $words );
}

function a4w_admin_log( $loglevel, $data = NULL, $filename = NULL, $context = NULL ) {
    if ($loglevel <= A4W_ADMIN_LOGGING_LEVEL) {
        $ts = current_time( 'timestamp' );
        
        $severity = ($loglevel == LOG_EMERG   ? 'EMERG'   : 
                    ($loglevel == LOG_ALERT   ? 'ALERT'   :
                    ($loglevel == LOG_CRIT    ? 'CRIT'    :
                    ($loglevel == LOG_ERR     ? 'ERR'     :
                    ($loglevel == LOG_WARNING ? 'WARNING' :
                    ($loglevel == LOG_NOTICE  ? 'NOTICE'  :
                    ($loglevel == LOG_INFO    ? 'INFO'    :
                    ($loglevel == LOG_DEBUG   ? 'DEBUG'   :
                     $loglevel))))))));

        $backtrace = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 2 );
        if ($data == NULL) {
            $data = 'Reached line ' . $backtrace[ 0 ][ 'line' ];
        }
        if ($context == NULL) {
            $context = $backtrace[ 1 ][ 'function' ];
        }
        
        if ($filename == NULL) {
            $filename = 'a4w-logs/logfile.txt';

            if (strlen( $context ) > 30) {
                $context = substr( $context, 0, 27 ) . '...';
            }
            $context .= '()';
            $data = str_replace( "\n", "\n" . str_repeat( ' ', 52 ), $data );

            $format = "%-12.12s%-7.7s%-33.33s%s\n";
            $flags = FILE_APPEND | LOCK_EX;
        } else {
            $format = "At %s with %s from %s():\n%s";
            $flags = 0;
        }
        
        $filepath = '/www/wp-content/uploads/' . $filename;
        $msg = sprintf( $format, $ts, $severity, $context, $data );
        file_put_contents( $filepath, $msg, $flags );
    }
}

?>
