// <script type='text/javascript' src='https://www.adventuresforwilderness.ca/wp-content/mu-plugins/pdf-lib/pdf-lib.js?ver=5.9.3'></script>
// <script type='text/javascript' src='https://www.adventuresforwilderness.ca/wp-content/mu-plugins/pdf-lib/download.js?ver=5.9.3'></script>

async function a4w_download_event_waiver( adv_name, adv_coord, adv_date ) {
    var url = 'https://www.adventuresforwilderness.ca/wp-content/uploads/2021/06/20200604_a4w_Waiver_Group_Form.pdf';
    var oldsize = await fetch( url ).then( res => res.arrayBuffer() );
    var waiver = await window.PDFLib.PDFDocument.load( oldsize );

    var helvetica = await waiver.embedFont( window.PDFLib.StandardFonts.Helvetica );

    var page = waiver.getPages()[ 0 ];
    page.setFont( helvetica );
    page.setFontSize( 9 );
    page.setFontColor( window.PDFLib.rgb( 0, 0, 0 ) );

    var sz = page.getSize();

    page.moveTo( (0.2554 * sz.width), (0.8604 * sz.height) );
    page.drawText( adv_name );

    page.moveTo( (0.7789 * sz.width), (0.8604 * sz.height) );
    page.drawText( adv_date );

    page.moveTo( (0.3062 * sz.width), (0.8260 * sz.height) );
    page.drawText( adv_coord );

    var newsize = await waiver.save();

	var adv_date_fn = adv_date.replace( '-', '' );
	var adv_name_fn = adv_name.toLowerCase().replace( ' ', '_' );
	var pdf_filename = adv_date_fn + '_a4w_waiver_form_' + adv_name_fn + '.pdf';

    download( newsize, pdf_filename, 'application/pdf' );
}
