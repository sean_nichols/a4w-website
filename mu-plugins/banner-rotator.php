<?php

/****************************************************************************
 *
 * A simple banner rotator
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-03-25
 *
 ****************************************************************************/

$BANNER_ROTATOR_VERSION = 20;

if (! function_exists( 'a4w_parse_bool' )) {
    function a4w_parse_bool( $boolstr ) {
        $falses = ['f', 'false', 'n', 'no', '', '0'];
        if (in_array( strtolower( $boolstr ), $falses, TRUE )) {
            return FALSE;
        }
        
        return TRUE;
    }
}

function a4w_make_browser_specific_css( $properties, $indent = 0 ) {
    $prefixes = ['-webkit-', '-moz-', '-o-', ''];
    
    $indent = str_repeat( ' ', 4 * $indent );
    $css = '';
    foreach ($properties as $k => $v) {
        foreach ($prefixes as $prefix) {
            $css .= $indent . $prefix . $k . ': ' . $v . ";\n";
        }
    }
    
    return $css;
}

// The banner rotator takes 6 parameters:
//  • "width" (STRING, required)
//    determines the width of the div that the images will fit into. It may
//    be specified in any CSS-compliant units. If no unit is supplied, "px"
//    is assumed.
//  • "height" (STRING, required)
//    determines the height of the div that the images will fit into. It may
//    be specified in any CSS-compliant units. If no unit is supplied, "px"
//    is assumed.
//  • "images" (STRING, required)
//    is a comma-separated list of postids of the images that are to be used
//    in the rotator.
//  • "length" (DECIMAL, optional)
//    is the length of time (in seconds) that each image is to be displayed
//    in the banner, including transition time. If absent, defaults to 5.
//  • "transition" (DECIMAL, optional)
//    is the length of time (in seconds) that each transition is to take. If
//    absent, defaults to 0.5.
//  • "random" (BOOL, optional)
//    indicates whether the order of the images should be randomized (true)
//    or as indicated by the order in the "images" parameter (false). If
//    absent, defaults to false.
//
// *********************************************
// WARNING: YOU CAN ONLY USE THIS ONCE PER PAGE!
// *********************************************
//
// Allowing multiple uses per page is a future enhancement. :-)
//

add_shortcode( 'a4w-image-rotator', 'a4w_sc_banner_rotator' );

function a4w_sc_banner_rotator( $atts, $content = NULL ) {
    $nl = "\n";
    
    $defaults = [
        'width'      => NULL,
        'height'     => NULL,
        'images'     => NULL,
        'length'     => 5,
        'transition' => 0.5,
        'random'     => 'false'
    ];
    
    extract( shortcode_atts( $defaults, $atts ) );

    if (is_null( $width ) || is_null( $height ) || is_null( $images )) {
        return 'Invalid parameters supplied for <tt>a4w-image-rotator</tt>.';
    }
    
    $random = a4w_parse_bool( $random );
    $image_ids = explode( ',', $images );
    
    if (count( $image_ids ) == 0) {
        return 'Invalid image parameter supplied for <tt>a4w-image-rotator</tt>.';
    }
    
    if ($random) {
        shuffle( $image_ids );
    }

    $urls = [];
    foreach ($image_ids as $postid) {
        $url = wp_get_attachment_url( intval( $postid ) );
        if ($url !== FALSE) {
            $urls[] = $url;
        }
    }

    $n = count( $urls );
    if ($n == 0) {
        return 'Invalid image parameter supplied for <tt>a4w-image-rotator</tt>.';
    }
    
    if (is_numeric( $width )) {
        $width .= 'px';
    }
    if (is_numeric( $height )) {
        $height .= 'px';
    }
    
    $aspect_ratio = intval( $height ) / intval( $width );
    
    $length = floatval( $length );
    $transition = floatval( $transition );
    $duration = $length * $n;
    
    $slide_pct = 100 / $n;
    $turn_pct = $slide_pct * $transition / $length;
    
    $iv1 = '0';
    $iv2 = round( $slide_pct - $turn_pct, 1 );
    $iv3 = round( $slide_pct, 1 );
    $iv4 = round( 100 - $turn_pct, 1 );
    $iv5 = '100';
    
    $output = '<style>' . $nl .
              '#a4w-rotator {' . $nl .
              '    position: relative;' . $nl .
              '    width: 100%;' . $nl .
              '    max-height: ' . $height . ';' . $nl .
              '    max-width: ' . $width . ';' . $nl .
              '    margin: 0px;' . $nl .
              '}' . $nl .
              '#a4w-rotator img {' . $nl .
              '    position: absolute;' . $nl .
              '    left: 0px;' . $nl .
              '    max-height: ' . $height . ';' . $nl .
              '    max-width: ' . $width . ';' . $nl .
              a4w_make_browser_specific_css( [
                  'animation-name'            => 'a4w_rotator',
                  'animation-timing-function' => 'ease-in-out',
                  'animation-iteration-count' => 'infinite',
                  'animation-duration'        => $duration . 's'
              ], 1 ) .
              '}' . $nl .
              '@keyframes a4w_rotator {' . $nl .
              '    ' . $iv1 . '% { opacity: 1 }' . $nl .
              '    ' . $iv2 . '% { opacity: 1 }' . $nl .
              '    ' . $iv3 . '% { opacity: 0 }' . $nl .
              '    ' . $iv4 . '% { opacity: 0 }' . $nl .
              '    ' . $iv5 . '% { opacity: 1 }' . $nl .
              '}';
    for ($i = 1; $i <= $n; $i++) {
        $delay = round( ($n - $i) * $length , 1 );
        $output .= '#a4w-rotator img:nth-of-type(' . $i . ') {' . $nl .
                   a4w_make_browser_specific_css( [
                       'animation-delay' => $delay . 's'
                   ], 1 ) .
                   '}' . $nl;
    }
    $output .= '</style>' . $nl .
               '<div id="a4w-rotator">' . $nl;
    for ($i = 0; $i < $n; $i++) {
        $output .= '    <img id="a4w-rotator-' . $i . '" src="' . $urls[ $i ] . '">' . $nl;
    }
    $output .= '</div>' . $nl .
               '<script>' . $nl .
               'function rotator_size_img() {' . $nl .
               '    var aspect = ' . $aspect_ratio . ';' . $nl .
               '    var e = document.getElementById( "a4w-rotator" );' . $nl .
               '    w = e.clientWidth;' . $nl .
               '    h = w * aspect; ' . $nl .
               '    e.style.height = h + "px";' . $nl .
               '    for (var i = 0; i < ' . $n . '; i++) {' . $nl .
               '        var img_e = document.getElementById( "a4w-rotator-" + i );' . $nl .
               '        img_e.style.width = w + "px";' . $nl .
               '        img_e.style.height = h + "px";' . $nl .
               '    }' . $nl .
               '}' . $nl .
               'window.addEventListener( "DOMContentLoaded", rotator_size_img );' . $nl .
               'window.addEventListener( "resize", rotator_size_img );' . $nl .
               '</script>' . $nl;
    
    return $output;
}

?>