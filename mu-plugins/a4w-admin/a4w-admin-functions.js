/****************************************************************************
 *
 * Various functions, hooks, etc. to be used by customizations for the
 *  A4W administration page.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-01-16
 *
 ****************************************************************************/

function a4w_call_server( api, args, callback, method = 'GET' ) {
    var xhr = new XMLHttpRequest();
    var url = '/wp-json/a4w-admin/v1/' + api;

    var params = [];
    for (var key in args) {
        if (args.hasOwnProperty( key )) {
            params.push( key + '=' + encodeURIComponent( args[ key ] ) );
        }
    }
    var paramstr = params.join( '&' );
    
    if (method == 'GET' && paramstr.length > 0) {
        url += '?' + paramstr;
    }
    
    xhr.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                var response = JSON.parse( this.responseText );
                // var response = this.responseText;
                callback( response );
            } else {
                alert( this.status + ' ' + this.statusText );
            }
        }
    };
    xhr.open( method, url, true );
    
    xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
    
    if (method == 'POST') {
        xhr.setRequestHeader( 'Content-type', 'application/x-www-form-urlencoded' );
        xhr.send( paramstr );
    } else if (method == 'GET') {
        xhr.send( null );
    }
}

function a4w_show_api_message( domid, msg ) {
    var e = document.getElementById( domid );
    e.style.display = 'block';
    e.innerHTML = '<p>' + msg + '</p>';
}

function a4w_api_cb_show_response( msg, domid, wrap = 'response' ) {
    var output = msg;
    
    if (wrap != null) {
        output = '<table><tr><th>' + wrap + '</th></tr>\n' +
                 '<tr><td style="bgcolor:#e6ffef;">' + msg + '</td></tr>\n' +
                 '</table>\n';
    }
    
    var e = document.getElementById( domid );
    e.innerHTML = output;
}

function a4w_get_invite_adventure( invite_code ) {
    var params = {};
    
    params[ 'invitecode' ] = invite_code;
    
    a4w_call_server( 'get_invite_adv', params, a4w_get_invite_adventure_cb, 'GET' );
}

function a4w_get_invite_adventure_cb( response ) {
    var msg;
    var textcolor;
    var bgcolor;
    var optclass = null;
    var shownext = false;
    
    if (response.status == 'fail') {
        msg = 'Oops! Something went wrong when trying to verify your invite code. ' +
              'The reason given is: ' +
              '"<i style="color:black;">' + response.msg + '</i>"<br><br>' +
              'You can double check the invite code, and try again in a few minutes.<br><br>' +
              'Alternatively, please feel free to contact the Alberta Wilderness ' +
              'Association Office at (403) 283-2025, or by email at ' +
              '<a href="a4w@abwild.ca">a4w@abwild.ca</a>. We will be able to ensure ' +
              'you are properly registered for your event.';
        textcolor = '#a00000';
        bgcolor = '#fee6ef';
    } else {
        msg = response.adventure_name;
        textcolor = '#808080';
        bgcolor = '#f0f0f0';
        shownext = true;
        
        if (response.adventure_name.startsWith( 'Getting Dave to the Summit' )) {
            optclass = 'a4w2020-getting-dave-to-the-summit-options';
        } else if (response.adventure_name.startsWith( 'Pollinator Power' )) {
            optclass = 'a4w2020-pollinator-power-options';
        }
    }
    
    if (optclass !== null) {
        var opts = document.getElementsByClassName( optclass );
        if (opts.length > 0) {
            for (var i = 0; i < opts.length; i++) {
                opts[ i ].style.display = 'block';
            }
        }
    }

    var p = document.getElementById( 'a4w-adventure-name-cb' );
    p.innerHTML = msg;
    p.style.color = textcolor;
    p.style.backgroundColor = bgcolor;
    
    if (! shownext) {
        document.getElementById( 'gform_next_button_6_1' ).style.display = 'none';
    }
}

function a4w_add_participant( params = null ) {
    var submitbutton = document.getElementById( 'a4w-admin-participant-submit' );
    submitbutton.disabled = true;
    
    if (! params) {
        params = {};
        
        var advlist = document.getElementById( 'a4w-admin-participant-adventure' );
        var event_id = advlist.options[ advlist.selectedIndex ].value;
        
        if (event_id == 0) {
            alert( 'Please select an adventure.' );
            submitbutton.disabled = false;
            return;
        }
        
        params[ 'eventid' ]       = event_id;
        params[ 'partname' ]      = document.getElementById( 'a4w-admin-participant-name' ).value.trim();
        params[ 'entryid' ]       = document.getElementById( 'a4w-admin-participant-entryid' ).value.trim();
        params[ 'extrainfo' ]     = document.getElementById( 'a4w-admin-participant-extra-info' ).value.trim();
        params[ 'partemail' ]     = document.getElementById( 'a4w-admin-participant-email' ).value.trim();
        params[ 'partphone' ]     = document.getElementById( 'a4w-admin-participant-phone' ).value;
        params[ 'waiver' ]        = document.getElementById( 'a4w-admin-participant-waiver' ).checked;
        params[ 'notify' ]        = document.getElementById( 'a4w-admin-participant-notify' ).checked;
        params[ 'confirm' ]       = document.getElementById( 'a4w-admin-participant-confirm' ).checked;
        params[ 'selfsponsored' ] = document.getElementById( 'a4w-admin-participant-selfsponsor' ).checked;
        params[ 'sponsamt' ]      = document.getElementById( 'a4w-admin-participant-sponsor-amt' ).value;
        params[ 'sponsordernum' ] = document.getElementById( 'a4w-admin-participant-sponsor-ordernum' ).value.trim();
        params[ 'duplicateok' ]   = document.getElementById( 'a4w-admin-participant-duplicate-ok' ).value;

        var planlist = document.getElementById( 'a4w-admin-participant-plan' );
        params[ 'sponsorplan' ]   = planlist.options[ planlist.selectedIndex ].value;

        var tixlist = document.getElementById( 'a4w-admin-participant-tickettype' );
        if (tixlist !== null) {
            params[ 'ticketid' ]  = tixlist.options[ tixlist.selectedIndex ].value;
        }
    }

    a4w_call_server( 'register', params, a4w_add_participant_cb, 'POST' );
}

function a4w_add_participant_cb( response ) {
	var domid = 'a4w-admin-participant-form-contents';
    var boxwrap = 'Response';
	
	if (response.status == 'fail') {
		if (response.reason == 'duplicate') {
            boxwrap = 'Verify Duplicate Registration';
		}
	}
    a4w_api_cb_show_response( response.msg, domid, boxwrap );
}

function a4w_add_participant_adventure_onchange() {
    var advlist = document.getElementById( 'a4w-admin-participant-adventure' );
    var event_id = advlist.options[ advlist.selectedIndex ].value;
    
    var minimum_donation = 0;
    
    if (event_id != 0) {
        minimum_donation = a4w_get_event_minimum_donation( event_id );
    }

    document.getElementById( 'a4w-admin-participant-sponsor-amt' ).value = minimum_donation.toFixed( 2 );
}

function a4w_add_duplicate_participant() {
	var p_name = document.getElementById( 'a4w-admin-participant-name' ).value.trim();
	var allow_dup = document.getElementById( 'a4w-admin-participant-duplicate-ok' ).checked;
	var errmsg;
	
    if (allow_dup) {
        a4w_add_participant( a4w_get_resubmitted_participant_form() );
    } else {
		errmsg = 'Duplicate participant \'' + p_name + '\' NOT registered in adventure.';
		a4w_api_cb_show_response( msg, 'a4w-admin-participant-form-contents' );
	}
}

function a4w_get_resubmitted_form_values( form_prefix, fields ) {
    var field_ids = Object.keys( fields );
    var field_id, field_name;
    
    var form_values = {};

    for (const i in field_ids) {
        field_id = field_ids[ i ];
        field_name = form_prefix + '-' + fields[ field_id ];

        form_values[ field_id ] = document.getElementById( field_name ).value;
    }
    
    return form_values;
}

function a4w_get_resubmitted_participant_form() {
    var fields = {
        eventid       : 'adventure',
        ticketid      : 'tickettype',
        entryid       : 'entryid',
        partname      : 'name',
        partemail     : 'email',
        partphone     : 'phone',
        extrainfo     : 'extra-info',
        sponsorplan   : 'plan',
        selfsponsored : 'selfsponsor',
        sponsamt      : 'sponsor-amt',
        sponsordernum : 'sponsor-ordernum',
        waiver        : 'waiver',
        notify        : 'notify',
        confirm       : 'confirm'
    };

    var form_params = a4w_get_resubmitted_form_values( 'a4w-admin-participant', fields );
    form_params[ 'duplicateok' ] = true;
    
    return form_params;
}

function a4w_show_subform( ckb_selfsponsor, subform_domid, show_when_unchecked = false ) {
    var show_subform = ckb_selfsponsor.checked;
    if (show_when_unchecked) {
        show_subform = ! show_subform;
    }

    var subform = document.getElementById( subform_domid );
    var displaystyle = 'none';
    if (show_subform) {
        if (subform instanceof HTMLSpanElement) {
            displaystyle = 'inline';
        } else if (subform instanceof HTMLDivElement) {
            displaystyle = 'block';
        }
    }
    
    subform.style.display = displaystyle;
}

function a4w_remove_participant() {
    var submitbutton = document.getElementById( 'a4w-admin-xparticipant-submit' );
    submitbutton.disabled = true;
    
    var params = {};
    
    var advlist = document.getElementById( 'a4w-admin-xparticipant-adventure' );
    params[ 'eventid' ]   = advlist.options[ advlist.selectedIndex ].value;

    var namelist = document.getElementById( 'a4w-admin-xparticipant-name' );
    if (namelist === null) {
        alert( 'No participants to remove from this adventure.' );
        submitbutton.disabled = false;
        return;
    }
    params[ 'bookingid' ] = namelist.options[ namelist.selectedIndex ].value;
    
    var aname = advlist.options[ advlist.selectedIndex ].innerHTML.replace( /&nbsp;/g, ' ' ).trim();
    var pname = namelist.options[ namelist.selectedIndex ].innerHTML.replace( /&nbsp;/g, ' ' ).trim();
    
    if (confirm( 'Really unregister ' + pname + ' from ' + aname + '? This cannot be undone.' )) {
        a4w_call_server( 'unregister', params, a4w_remove_participant_cb, 'POST' );
    } else {
        submitbutton.disabled = false;
    }
}

function a4w_remove_participant_cb( response ) {
    a4w_api_cb_show_response( response.msg, 'a4w-admin-xparticipant-form-contents' );
}

function a4w_get_personal_page_link() {
    var submitbutton = document.getElementById( 'a4w-admin-personal-page-submit' );
    submitbutton.disabled = true;
    
    var params = {};
    
    var namelist = document.getElementById( 'a4w-admin-personal-page-name' );
    if (namelist === null) {
        alert( 'No participants in this adventure to get a personal page for.' );
        submitbutton.disabled = false;
        return;
    }
    params[ 'bookingid' ] = namelist.options[ namelist.selectedIndex ].value;
    
    a4w_call_server( 'get_personal_page_link', params, a4w_get_personal_page_link_cb, 'GET' );
}

function a4w_get_personal_page_link_cb( response ) {
    var submitbutton = document.getElementById( 'a4w-admin-personal-page-submit' );
    var responseblock = document.getElementById( 'a4w-admin-personal-page-form-response' );
    var output;
    
    if (response.status == 'ok') {
        var page_link = response.link;
        output = '<a href="' + page_link + '" style="font-family:monospace;" target="_blank">' + page_link + '</a>';
    } else {
        output = response.msg;
    }
    a4w_api_cb_show_response( output, 'a4w-admin-personal-page-link', null );

    responseblock.style.display = 'block';
    submitbutton.disabled = false;
}

function a4w_add_sponsor() {
    var submitbutton = document.getElementById( 'a4w-admin-sponsor-submit' );
    submitbutton.disabled = true;

    var params = {};
    
    var advlist = document.getElementById( 'a4w-admin-sponsor-adventure' );
    var event_id = advlist.options[ advlist.selectedIndex ].value;

    if (event_id == 0) {
        alert( 'Please select an adventure.' );
        submitbutton.disabled = false;
        return;
    }

    var namelist = document.getElementById( 'a4w-admin-sponsor-participant' );
    var booking_id;
    if (namelist === null) {
        booking_id = 0;
    } else {
        booking_id = namelist.options[ namelist.selectedIndex ].value;
    }

    params[ 'eventid' ]     = event_id;
    params[ 'ordernum' ]    = document.getElementById( 'a4w-admin-sponsor-ordernum' ).value.trim();
    params[ 'sponsname' ]   = document.getElementById( 'a4w-admin-sponsor-name' ).value.trim();
    params[ 'sponsemail' ]  = document.getElementById( 'a4w-admin-sponsor-email' ).value.trim();
    params[ 'sponsamt' ]    = document.getElementById( 'a4w-admin-sponsor-amount' ).value;
    params[ 'bookingid' ]   = booking_id;
    params[ 'participant' ] = document.getElementById( 'a4w-admin-sponsor-participant-alt' ).value.trim();
    params[ 'notes' ]       = document.getElementById( 'a4w-admin-sponsor-notes' ).value;

    a4w_call_server( 'sponsor', params, a4w_add_sponsor_cb, 'POST' );
}

function a4w_add_sponsor_cb( response ) {
    a4w_api_cb_show_response( response.msg, 'a4w-admin-sponsor-form-contents' );
}

function a4w_assign_sponsor() {
    var submitbutton = document.getElementById( 'a4w-admin-assignment-submit' );
    submitbutton.disabled = true;
    
    var params = {};
    
    var sponsorlist = document.getElementById( 'a4w-admin-assignment-sponsor' );
    var sponsor_id = sponsorlist.options[ sponsorlist.selectedIndex ].value;
    
    if (sponsor_id == 0) {
        alert( 'Please select a sponsor.' );
        submitbutton.disabled = false;
        return;
    }
    
    var general_sponsor = document.getElementById( 'a4w-admin-assignment-general' ).checked;
    
    var booking_id = 0;
    if (! general_sponsor) {
        var participantlist = document.getElementById( 'a4w-admin-assignment-participant' );
        
        if (participantlist != null) {
            booking_id = participantlist.options[ participantlist.selectedIndex ].value;
        }
        
        if (booking_id == 0) {
            alert( 'Please select a participant to assign to.' );
            submitbutton.disabled = false;
            return;
        }
    }
    
    params[ 'sponsorid' ] = sponsor_id;
    params[ 'togeneral' ] = general_sponsor;
    params[ 'bookingid' ] = booking_id;
    
    a4w_call_server( 'assign_sponsor', params, a4w_assign_sponsor_cb, 'POST' );
}

function a4w_assign_sponsor_cb( response ) {
    a4w_api_cb_show_response( response.msg, 'a4w-admin-assignment-form-contents' );
}

function a4w_split_sponsorship() {
    var submitbutton = document.getElementById( 'a4w-admin-split-submit' );
    submitbutton.disabled = true;
    
    var params = {};
    
    var sponsorlist = document.getElementById( 'a4w-admin-split-sponsor' );
    var sponsor_id = sponsorlist.options[ sponsorlist.selectedIndex ].value;
    
    if (sponsor_id == 0) {
        alert( 'Please select a sponsor.' );
        submitbutton.disabled = false;
        return;
    }
    
    var split_amount = document.getElementById( 'a4w-admin-split-amount' ).value;
    
    params[ 'sponsorid' ] = sponsor_id;
    params[ 'amount' ] = split_amount;
    
    a4w_call_server( 'split_sponsor', params, a4w_split_sponsorship_cb, 'POST' );
}

function a4w_split_sponsorship_cb( response ) {
    a4w_api_cb_show_response( response.msg, 'a4w-admin-split-form-contents' );
}

function a4w_import_adventure() {
    var submitbutton = document.getElementById( 'a4w-admin-adventure-submit' );
    submitbutton.disabled = true;
    
    var entrylist = document.getElementById( 'a4w-admin-adventure-entry' );
    var entry_id = entrylist.options[ entrylist.selectedIndex ].value;
    
    if (entry_id == 0) {
        alert( 'Please select a form entry to import.' );
        submitbutton.disabled = false;
        return;
    }
    
    window.open( '/create-new-adventure/?form_entry_id=' + entry_id, '_blank' );
    submitbutton.disabled = false;
}

function a4w_view_fundraising() {
    document.getElementById( 'a4w-view-fundraising-submit' ).disabled = true;

    var params = {};
    
    document.getElementById( 'a4w-view-fundraising-form-message' ).style.display = 'none';
    
    var advlist = document.getElementById( 'a4w-view-fundraising-adventure' );
    params[ 'eventid' ]   = advlist.options[ advlist.selectedIndex ].value;
    params[ 'secretcode' ]  = document.getElementById( 'a4w-view-fundraising-secret-code' ).value;

    a4w_call_server( 'view_fundraising', params, a4w_view_fundraising_cb, 'GET' );
}

function a4w_view_fundraising_cb( response ) {
    if (response.status == 'fail') {
        a4w_show_api_message( 'a4w-view-fundraising-form-message', response.msg );
        return;
    }

    var output = '<h4>' + response.event_name + '</h4>\n' +
                 '<p>Thank you ' + response.coordinator_name + ' for your ' +
                 '   great efforts raising funds for AWA!</p>\n' +
                 '<table id="a4w-fundraising-results-table">\n' +
                 '<tr>\n' +
                 '    <td colspan="2">Participant</td>\n' +
                 '    <td>Sponsor</td>\n' +
                 '    <td>Amount</td>\n' +
                 '</tr>\n';
    if (response.coord.length > 0) {
        output += '<tr><th colspan="4">Coordinator</th></tr>\n' +
                  '<tr>\n' +
                  '    <td style="max-width:2em;" colspan="2">' + response.coordinator_name + '</td>\n' +
                  '    <td>' + response.coord[ 0 ][ 0 ] + '</td>\n' +
                  '    <td>$' + parseFloat( response.coord[ 0 ][ 1 ] ).toFixed( 2 ) + '</td>\n' +
                  '</tr>\n';
        if (response.coord.length > 1) {
            for (var j = 1; j < response.coord.length; j++) {
                var sponsor = response.coord[ j ];
                output += '<tr>\n' +
                          '    <td colspan="2">&nbsp;</td>\n' +
                          '    <td>' + sponsor[ 0 ] + '</td>\n' +
                          '    <td>' + parseFloat( sponsor[ 1 ] ).toFixed( 2 ) + '</td>\n' +
                          '</tr>\n';
            }
        }
    }
    if (response.parts.length > 0) {
        output += '<tr><th colspan="4">Registered Participants</th></tr>\n';
        for (var i = 0; i < response.parts.length; i++) {
            var part = response.parts[ i ];
            if (part.length == 0) {
                output += '<tr>\n<td style="max-width:2em;display:block;">' + (i + 1) + '.</td><td colspan="3">&nbsp;</td></tr>\n';
            } else {
                output += '<tr>\n' +
                          '    <td style="max-width:2em;">' + (i + 1) + '.</td><td>' + part[ 0 ] + '</td>\n';
                if (part[ 1 ].length == 0) {
                    output += '    <td colspan="2">&nbsp;</td>\n';
                } else {
                    output += '    <td>' + part[ 1 ][ 0 ][ 0 ] + '</td>\n' +
                              '    <td>$' + parseFloat( part[ 1 ][ 0 ][ 1 ] ).toFixed( 2 ) + '</td>\n' +
                              '</tr>\n';
                    if (part[ 1 ].length > 1) {
                        for (var j = 1; j < part[ 1 ].length; j++) {
                            var sponsor = part[ 1 ][ j ];
                            output += '<tr>\n' +
                                      '    <td colspan="2">&nbsp;</td>\n' +
                                      '    <td>' + sponsor[ 0 ] + '</td>\n' +
                                      '    <td>$' + parseFloat( sponsor[ 1 ] ).toFixed( 2 ) + '</td>\n' +
                                      '</tr>\n';
                        }
                    }
                }
            }
        }
    }
    if (Object.keys( response.pending ).length > 0) {
        output += '<tr><th colspan="4">Unregistered Participants</th></tr>\n';
        for (var name in response.pending) {
            var unreg = response.pending[ name ];
            output += '<tr>\n' +
                      '    <td colspan="2">' + name + '</td>\n';
            if (unreg.length == 0) {
                output += '    <td colspan="2">&nbsp;</td>\n';
            } else {
                output += '    <td>' + unreg[ 0 ][ 0 ] + '</td>\n' +
                          '    <td>$' + parseFloat( unreg[ 0 ][ 1 ] ).toFixed( 2 ) + '</td>\n' +
                          '</tr>\n';
                if (unreg.length > 1) {
                    for (var j = 1; j < unreg.length; j++) {
                        var sponsor = unreg[ j ];
                        output += '<tr>\n' +
                                  '    <td colspan="2">&nbsp;</td>\n' +
                                  '    <td>' + sponsor[ 0 ] + '</td>\n' +
                                  '    <td>$' + parseFloat( sponsor[ 1 ] ).toFixed( 2 ) + '</td>\n' +
                                  '</tr>\n';
                    }
                }
            }
        }
    }
    if (response.general.length > 0) {
        output += '<tr><th colspan="4">General Sponsorships</th></tr>\n';
        for (var i = 0; i < response.general.length; i++) {
            var sponsor = response.general[ i ];
            output += '<tr>\n' +
                      '    <td colspan="2">&nbsp;</td>\n' +
                      '    <td>' + sponsor[ 0 ] + '</td>\n' +
                      '    <td>$' + parseFloat( sponsor[ 1 ] ).toFixed( 2 ) + '</td>\n' +
                      '</tr>\n';
        }
    }
    
    output += '<tr><th colspan="4">TOTAL RAISED</th><tr>\n' +
              '<tr>\n' +
              '    <td colspan="3">&nbsp;</td>\n' +
              '    <td style="text-decoration:underline;font-weight:bold;font-size:larger;">$' + parseFloat( response.total ).toFixed( 2 ) + '</td>\n' +
              '</tr>\n' +
              '<tr><th colspan="4">EVENT GOAL</th><tr>\n' +
              '<tr>\n' +
              '    <td colspan="3">&nbsp;</td>\n' +
              '    <td style="text-decoration:underline;font-weight:bold;font-size:larger;">$' + parseFloat( response.goal ).toFixed( 2 ) + '</td>\n' +
              '</tr>\n' +
              '</table>\n';
    
    var e = document.getElementById( 'a4w-view-fundraising-form-contents' );
    e.innerHTML = output;
}

function a4w_download_spreadsheet( datatype, callback ) {
    var submitbutton = document.getElementById( 'a4w-admin-download-' + datatype + '-submit' );
    submitbutton.disabled = true;

    var params = {};

    var advlist = document.getElementById( 'a4w-admin-download-' + datatype + '-adventure' );
    var event_id = advlist.options[ advlist.selectedIndex ].value;

    if (event_id == 0) {
        alert( 'Please select an adventure.' );
        submitbutton.disabled = false;
        return;
    }

    params[ 'eventid' ] = event_id;

    a4w_call_server( 'download_' + datatype, params, callback, 'GET' );
}

function a4w_download_donations() {
    a4w_download_spreadsheet( 'donations', a4w_download_donations_cb );
}

function a4w_download_donations_cb( response ) {
    if (response.status == 'fail') {
        // Fail silently here.
        return;
    }
    
    var filename = response.filename;
    var datetime = response.datetime;
    var sponsorships = response.data;

    var spreadsheet = a4w_create_donations_spreadsheet( datetime, sponsorships );
    spreadsheet.generate( filename + '.xlsx' );
}

function a4w_download_donations_adventure_onchange() {
    document.getElementById( 'a4w-admin-download-donations-submit' ).disabled = false;
}

function a4w_download_participants() {
    a4w_download_spreadsheet( 'participants', a4w_download_participants_cb );
}

function a4w_download_participants_cb( response ) {
    if (response.status == 'fail') {
        var msg = 'Oops! Something went wrong trying to generate a table of participants. ' +
                  'The reason given is: ' +
                  '"<i style="color:black;">' + response.msg + '</i>"<br><br>';
        
        var output = '<table><tr><th>response</th></tr>\n' +
                     '<tr><td style="color:#a00000; bgcolor:#fee6ef;">' + msg + '</td></tr>\n' +
                     '</table>\n';
        
        var e = document.getElementById( 'a4w-admin-download-participants-form-contents' );
        e.innerHTML = output;

        return;
    }
    
    var filename = response.filename;
    var datetime = response.datetime;
    var event_id = response.eventid;
    var participants = response.data;
    
    // var e = document.getElementById( 'a4w-admin-download-participants-form-contents' );
    // e.innerHTML = '<pre>' + response.raw_data + '</pre>';
    
    var spreadsheet = a4w_create_participants_spreadsheet( datetime, event_id, participants );
    spreadsheet.generate( filename + '.xlsx' );
}

function a4w_download_participants_adventure_onchange() {
    document.getElementById( 'a4w-admin-download-participants-submit' ).disabled = false;
}

function a4w_get_website_stats_year_onchange() {
    var year_domid = 'a4w-admin-stats-year-select';
    
    var yearlist = document.getElementById( year_domid );
    var statsyear = yearlist.options[ yearlist.selectedIndex ].value;
    
    var params = {};
    
    if (statsyear != 0) {
        params[ 'year' ] = statsyear;

        a4w_call_server( 'get_website_stats', params, a4w_get_website_stats_year_cb, 'GET' );
    }
}

function a4w_get_website_stats_year_cb( response ) {
    var output = '';
    if (response.status == 'fail') {
        output += '        <tr><th colspan="2">Website DB Error</th></tr>\n' +
                  '        <tr>\n' +
                  '            <td>' + response.msg + '</td>\n' +
                  '        </tr>\n';
    } else {
        var total_participants = 0;
        var formatter = new Intl.NumberFormat( 'en-US' );
        var total_sponsored = formatter.format( parseFloat( response.total_sponsored ).toFixed( 2 ) );
        output += '        <tr><th colspan="2">Adventures</th></tr>\n' +
                  '        <tr>\n' +
                  '            <td>Number of adventures:</td><td>' + Object.keys( response.events ).length + '</td>\n' +
                  '        </tr>\n' +
                  '        <tr><th colspan="2">Sponsorships</th></tr>\n' +
                  '        <tr>\n' +
                  '            <td>Number of sponsors:</td><td>' + response.num_sponsors + '</td>\n' +
                  '        </tr><tr>\n' +
                  '            <td>Total amount sponsored:</td><td>$' + total_sponsored + '</td>\n' +
                  '        </tr>\n' +
                  '        <tr><th colspan="2">Participants</th></tr>\n';
        
        for (var i in response.events) {
            var adventure = response.events[ i ];
            var participants = parseInt( adventure[ 1 ] );
            if (participants > 0) {
                output += '        <tr><td>' + adventure[ 0 ] + '</td><td>' + participants + '</td></tr>\n';
                total_participants += participants;
            }
        }
        output += '        <tr><td><b>Total:</b></td><td><b>' + total_participants + '</b></td></tr>\n';
    }

    var e = document.getElementById( 'a4w-website-stats-table-contents' );
    e.innerHTML = output;
}

function a4w_select_tickettype( adv_domid, tix_domid ) {
    var advlist = document.getElementById( adv_domid );
    var event_id = advlist.options[ advlist.selectedIndex ].value;
    var output = '';
    
    if (a4w_ticket_types.hasOwnProperty( event_id ) && Object.keys( a4w_ticket_types[ event_id ].tickets ).length > 0) {
        output = '<select id="' + tix_domid + '" class="a4w-admin-form-select-long">\n';
        
        var tickets = a4w_ticket_types[ event_id ].tickets;
        for (var i in tickets) {
            var ticket_label = tickets[ i ].name;
            if (tickets[ i ].description !== null) {
                ticket_label += ' (' + tickets[ i ].description + ')';
            }
            ticket_label += '  -- ' + tickets[ i ].available + ' left';
            
            output += '<option value="' + tickets[ i ].id + '">' + ticket_label + '</option>\n';
        }

        output += '</select>\n';
    } else {
        output = '<input type="text" disabled class="a4w-admin-form-text-long" value="[No registration categories available for this adventure]">';
    }
    
    var c = document.getElementById( tix_domid + '-container' );
    c.innerHTML = output;
}

function a4w_get_event_minimum_donation( event_id ) {
    var minimum = 0;
    
    if (a4w_minimum_fundraising_amounts.hasOwnProperty( event_id )) {
        var event_amounts = a4w_minimum_fundraising_amounts[ event_id ];
        if (Object.keys( event_amounts ).length > 0) {
            if (event_amounts.minimum !== null) {
                minimum = event_amounts.minimum;
            } else if (event_amounts.suggestion !== null) {
                minimum = event_amounts.suggestion;
            }
        }
    }
    
    return minimum;
}

function a4w_get_adv_participants( adv_domid, nam_domid, writein_prompt ) {
    var advlist = document.getElementById( adv_domid );
    var event_id;
    var output = '';
    
    if (advlist instanceof HTMLSelectElement) {
        event_id = advlist.options[ advlist.selectedIndex ].value;
    } else if (advlist instanceof HTMLInputElement) {
        event_id = advlist.value;
    }
    
    if (a4w_participant_names.hasOwnProperty( event_id ) && Object.keys( a4w_participant_names[ event_id ] ).length > 0) {
        output = '<select id="' + nam_domid + '" class="a4w-admin-form-select-long">\n';

        var participants = a4w_participant_names[ event_id ];
        if (writein_prompt) {
            output += '<option value="0" selected>Select a participant or write in name below.</option>\n';
        }
        for (var i in participants) {
            output += '<option value="' + participants[ i ].booking + '">' + participants[ i ].name + ' (' + participants[ i ].category + ')</option>\n';
        }

        output += '</select>\n';
    } else {
        output = '<input type="text" disabled class="a4w-admin-form-text-long" value="[No participants yet registered in this adventure]">';
    }
    
    var c = document.getElementById( nam_domid + '-container' );
    c.innerHTML = output;
}

function a4w_validate_email( domid ) {
    var e = document.getElementById( domid );
    var em = e.value.trim();
    e.value = em;
}

function a4w_validate_phone( domid ) {
    var e = document.getElementById( domid );
    var pn = e.value.replace( /\D/g, '' );
    
    e.value = '(' + pn.substring( 0, 3 ) + ') ' +
              pn.substring( 3, 6 ) + '-' + pn.substring( 6, 10 );
}
