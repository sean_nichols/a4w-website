function a4w_create_donations_spreadsheet( datetime, sponsor_data ) {
    var excel = $JExcel.new( 'Calibri 11 #000000' );
    
    var headerStyle = excel.addStyle( {
        font:   'Calibri 11 #000000 B'
    } );
    
    var currencyStyle = excel.addStyle( {
        format: '$#,##0.00'
    } );
    
    var dateStyle = excel.addStyle( {
        format: 'yyyy-mm-dd hh:mm'
    } );
    
    excel.set( { sheet: 0, value: 'A4W Sponsors ' + datetime } );
    
    var alignments = {
        L : excel.addStyle( { align: 'L' } ),
        R : excel.addStyle( { align: 'R' } )
    };
    
    var headers = [
        'ID', 'Order Number', 'Event Name', 'Donation Date', 'Sponsor Name',
        'Sponsor Email', 'Amount', 'Anonymous', 'Participant Name', 'Notes'
    ];
    var colwidth = [
        4, 15, 35, 18, 25, 32, 11, 5, 32, 255
    ];
    var colalign = [
        'L', 'R', 'L', 'R', 'L', 'L', 'R', 'L', 'L', 'L'
    ];
    
    for (var c = 0; c < headers.length; c++) {
        excel.set( { sheet: 0, row: 0, column: c, value: headers[ c ] } );
        excel.set( { sheet: 0, column: c, style: alignments[ colalign[ c ] ] } );
        excel.set( { sheet: 0, column: c, value: colwidth[ c ] } );
    }
    excel.set( { sheet: 0, row: 0, style: headerStyle } );
    excel.freezePane( 0, 0, 2 );
    
    for (var r = 0; r < sponsor_data.length; r++) {
        for (var c = 0; c < headers.length; c++) {
            var cellvalue = sponsor_data[ r ][ c ];
            
            // JSExcel does not like nulls. At all.
            if (cellvalue === null) {
                cellvalue = '';
            }
            
            // No order number - blanks are "nicer" than zeroes
            if (c == 1 && cellvalue == 0) {
                cellvalue = '';
            }
            
            excel.set( { sheet: 0, row: r + 1, column: c, value: cellvalue } );
        }
    }
    
    excel.set( { sheet: 0, column: 3, style: dateStyle } );
    excel.set( { sheet: 0, column: 6, style: currencyStyle } );
    
    return excel;
}

function a4w_create_participants_spreadsheet( datetime, adventure_id, data ) {
    var excel = $JExcel.new( 'Calibri 11 #000000' );
 
    var titleStyle = excel.addStyle( {
        font:   'Calibri 12 #000000 B'
    } );

    var headerStyle = excel.addStyle( {
        font:   'Calibri 11 #000000 B'
    } );

    var currencyStyle = excel.addStyle( {
        format: '$#,##0.00'
    } );

    excel.set( { sheet: 0, value: 'Participants ' + datetime } );

    var alignments = {
        L : excel.addStyle( { align: 'L' } ),
        R : excel.addStyle( { align: 'R' } )
    };
    
    var headers = [
        'Reg Entry No.', 'Category', 'Name', 'Email', 'Phone', 'Waiver Signed', 'Amount Sponsored', 'Fundraising Plan'
    ];
    var colwidth = [
        5, 32, 32, 32, 20, 5, 11, 8
    ];
    var colalign = [
        'L', 'L', 'L', 'L', 'L', 'L', 'R', 'L'
    ];
    
    var has_extra_data = false;
    var extra_data = [];
    
    var title = 'A4W' + (new Date()).getFullYear() + ': ';
    if (adventure_id == '*') {
        headers.unshift( 'Adventure Name' );
        colwidth.unshift( 35 );
        colalign.unshift( 'L' );

        title += 'All Adventures';
    } else {
        // Just grab the adventure name from the first row
        title += data[ 0 ][ 1 ];
    }
    
    excel.set( { sheet: 0, row: 0, column: 0, value: title } );
    excel.set( { sheet: 0, row: 0, column: 0, style: titleStyle } );

    for (var c = 0; c < headers.length; c++) {
        excel.set( { sheet: 0, row: 2, column: c, value: headers[ c ] } );
        excel.set( { sheet: 0, column: c, style: alignments[ colalign[ c ] ] } );
        excel.set( { sheet: 0, column: c, value: colwidth[ c ] } );
    }
    excel.set( { sheet: 0, row: 2, style: headerStyle } );
    excel.freezePane( 0, 0, 4 );

    for (var r = 0; r < data.length; r++) {
        var p_event = data[ r ][ 1 ];
        var p_entryid = data[ r ][ 2 ];
        if (p_entryid == null) {
            p_entryid = '';
        }
        var p_category = data[ r ][ 10 ];
        var p_name = data[ r ][ 3 ];
        var p_email = data[ r ][ 4 ];
        var p_phone = data[ r ][ 5 ];
        if (p_phone.length == 10) {
            p_phone = '(' + p_phone.substr( 0, 3 ) + ') ' +
                      p_phone.substr( 3, 3 ) + '-' + p_phone.substr( 6 );
        }
        var p_waiver = (data[ r ][ 6 ] == 1 ? 'Yes' : 'No' );
        var p_sponsored = data[ r ][ 7 ];
        var p_plan = (data[ r ][ 8 ] == 1 ? 'Self' : 'Others' );
        var p_extra = data[ r ][ 9 ];
        if (! p_extra) {
            extra_data.push( '' );
        } else {
            extra_data.push( p_extra );
            has_extra_data = true;
        }

        var p_row = [p_entryid, p_category, p_name, p_email, p_phone, p_waiver, p_sponsored, p_plan];
        if (adventure_id == '*') {
            p_row.unshift( p_event );
        }

        for (var c in p_row) {
            excel.set( { sheet: 0, row: r + 3, column: c, value: p_row[ c ] } );
        }
    }
    
    if (has_extra_data) {
        var xc = (adventure_id == '*' ? 9 : 8);
        excel.set( { sheet: 0, row: 2, column: xc, value: 'Extra Info' } );
        excel.set( { sheet: 0, column: xc, style: alignments[ 'L' ] } );
        excel.set( { sheet: 0, column: xc, value: 50 } ); // width
        
        var xr = 2;
        for (var d in extra_data) {
            xr++;
            excel.set( { sheet: 0, row: xr, column: xc, value: extra_data[ d ] } );
        }
    }

    var sponsored_col = (adventure_id == '*' ? 7 : 6);
    
    excel.set( { sheet: 0, column: sponsored_col, style: currencyStyle } );

    return excel;
}