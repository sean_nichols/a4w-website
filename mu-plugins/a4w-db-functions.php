<?php

/****************************************************************************
 *
 * Various functions, hooks, etc. to be used by customizations for the
 *  A4W administration page.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-01-16
 *
 ****************************************************************************/


require_once( __DIR__ . '/a4w-general-functions.php' );

define( 'A4W_BOOKINGS_TABLE',     a4w_db_get_table_name( 'em_bookings' ) );
define( 'A4W_BOOKINGS_XT_TABLE',  a4w_db_get_table_name( 'em_tickets_bookings' ) );
define( 'A4W_EVENTS_TABLE',       a4w_db_get_table_name( 'em_events' ) );
define( 'A4W_FORMDATA_TABLE',     a4w_db_get_table_name( 'gf_form_meta' ) );
define( 'A4W_META_TABLE',         a4w_db_get_table_name( 'postmeta' ) );
define( 'A4W_PARTICIPANTS_TABLE', a4w_db_get_table_name( 'a4w_participants' ) );
define( 'A4W_RELATIONS_TABLE',    a4w_db_get_table_name( 'term_relationships' ) );
define( 'A4W_SPONSORS_TABLE',     a4w_db_get_table_name( 'a4w_sponsors' ) );
define( 'A4W_TERMS_TABLE',        a4w_db_get_table_name( 'terms' ) );
define( 'A4W_TICKETS_TABLE',      a4w_db_get_table_name( 'em_tickets' ) );


function a4w_db_get_dbh() {
    static $dbh = NULL;
    
    if (is_null( $dbh )) {
        $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
        $opts = [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . DB_CHARSET,
            PDO::ATTR_PERSISTENT => false
        ];
        
        try {
            $dbh = new PDO( $dsn, DB_USER, DB_PASSWORD, $opts );
            $results = [TRUE, $dbh];
        } catch (PDOException $e) {
            $dbh = NULL;
            $results = [FALSE, 'PDO Error ' . $e->getMessage()];
        }
    } else {
        $results = [TRUE, $dbh];
    }

    return $results;
}

function a4w_db_get_result( $dbh, $sql, $params = NULL ) {
    // returns Array r where:
    //  r[ 0 ] is a boolean indicating success
    //  r[ 1 ] is either the requested value 
    //                or an error message
    //
    // Note 1: An empty result returns `[TRUE, NULL]`
    
    try {
        $stmt = $dbh->prepare( $sql );
        if (isset( $params )) {
            $stmt->execute( $params );
        } else {
            $stmt->execute();
        }
        $row = $stmt->fetch();
        $results = [TRUE, ($row ? $row[ 0 ] : NULL)];
    } catch (PDOException $e) {
        $results = [FALSE, 'PDO Error ' . $e->getMessage()];
    }
    
    return $results;
}

function a4w_db_get_results( $dbh, $sql, $params = NULL, $rstype = PDO::FETCH_NUM ) {
    // Returns Array r where:
    //  r[ 0 ] is a boolean indicating success
    //  r[ 1 ] is either the recordset (an array with each element being a successive row)
    //                or an error message
    //
    // Note 1: each row is itself an array, even if a singleton
    // Note 2: An empty result returns `[TRUE, []]`
    
    try {
        $stmt = $dbh->prepare( $sql );
        if (isset( $params )) {
            $stmt->execute( $params );
        } else {
            $stmt->execute();
        }
        $rs = [];
        while (($row = $stmt->fetch( $rstype )) !== FALSE) {
            array_push( $rs, $row );
        }
        $results = [TRUE, $rs];
    } catch (PDOException $e) {
        $results = [FALSE, 'PDO Error ' . $e->getMessage()];
    }
    
    return $results;
}

function a4w_db_get_table_name( $table ) {
    return $GLOBALS[ 'table_prefix' ] . $table . ' ';
}

function a4w_db_put_data( $dbh, $sql, $params ) {
    try {
        $stmt = $dbh->prepare( $sql );
        if (isset( $params )) {
            $stmt->execute( $params );
        } else {
            $stmt->execute();
        }
        $results = [TRUE, $dbh->lastInsertId()];
    } catch (PDOException $e) {
        $results = [FALSE, 'PDO Error ' . $e->getMessage()];
    }

    return $results;
}

function a4w_db_update_data( $dbh, $sql, $params ) {
    return a4w_db_change_data( $dbh, $sql, $params, 'udpate' );
}

function a4w_db_delete_data( $dbh, $sql, $params ) {
    return a4w_db_change_data( $dbh, $sql, $params, 'delete' );
}

function a4w_db_change_data( $dbh, $sql, $params, $operation ) {
    if (stripos( $sql, ' WHERE ' ) === FALSE) {
        $results = [FALSE, 'Trying to ' . $operation . ' without a WHERE clause'];
    }

    try {
        $stmt = $dbh->prepare( $sql );
        if (isset( $params )) {
            $stmt->execute( $params );
        } else {
            $stmt->execute();
        }
        $results = [TRUE, $stmt->rowCount()];
    } catch (PDOException $e) {
        $results = [FALSE, 'PDO Error ' . $e->getMessage()];
    }

    return $results;
}

function a4w_db_safe_search_pattern( $str ) {
    return str_replace( '%', '\\%', str_replace( '_', '\\_', $str ) );
}

function a4w_db_get_event_amt_raised( $dbh, $eventid ) {
    $sql = 'SELECT coalesce(sum(amount), 0) ' .
           'FROM   ' . A4W_SPONSORS_TABLE .
           'WHERE  event_id = ?;';
    $params = [$eventid];
    
    return a4w_db_get_result( $dbh, $sql, $params );
}

function a4w_db_get_next_adventures( $dbh, $size = 2 ) {
    $category_date_tbd = 26;
    $category_no_date = 27;
    $formatstr = '\'%b %e, %Y\'';
    
    $sql = 'SELECT   event_name, coordinator, event_slug, ' .
           '         date_format(sort_date, ' . $formatstr . ') ' .
           'FROM ' .
           '( ' .
           '    SELECT    event_name, meta_value AS coordinator, event_slug, ' .
           '              a4w_get_adventure_sort_date(event_id) AS sort_date ' .
           '    FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           '    LEFT JOIN ' . A4W_META_TABLE . ' as m ' .
           '    ON        e.post_id = m.post_id ' .
           '    WHERE     meta_key = ? AND ' .
           '              event_private = 0 AND ' .
           '              a4w_is_active_adventure(event_id) ' .
           ') ' .
           'AS       a ' .
           'WHERE    sort_date IS NOT NULL ' .
           'ORDER BY sort_date, event_name ASC ' .
           'LIMIT    ' . $size . ';';
    $params = ['coordinator_name']; //, $category_date_tbd, $category_no_date];
    
    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_top_adventures( $dbh, $size = 10, $year = NULL ) {
    // Note NULL is different from other falsy values, for example: blank.
    // If blank is specified, then $yearspec will become 'a4w%' which
    //  will match all adventures across all years.
    if (is_null( $year )) {
        $year = current_time( 'Y' );
    }
    $yearspec = 'a4w' . $year . '%';
    
    $sql = 'SELECT     event_name, meta_value, event_slug, ' .
           '           coalesce(sum(amount), 0) ' .
           'FROM       (' . A4W_SPONSORS_TABLE . ' AS s ' .
           'RIGHT JOIN  ' . A4W_EVENTS_TABLE . ' AS e ' .
           'ON          s.event_id = e.event_id) ' .
           'LEFT JOIN  ' . A4W_META_TABLE . ' AS m ' .
           'ON         e.post_id = m.post_id ' .
           'WHERE      meta_key = ? AND ' .
           '           event_private = 0 AND ' . 
           '           event_slug LIKE ? ' .
           'GROUP BY   event_name ' .
           'ORDER BY   sum(amount) DESC, event_name ASC ' .
           'LIMIT      ' . intval( $size ) . ';';
    $params = ['coordinator_name', $yearspec];
    
    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_top_sponsors( $dbh, $size = 10, $scope = NULL ) {
    if (is_null( $scope )) {
        $scope = ['year', current_time( 'Y' )];
    }

    $groupby = 'sponsor_email ';
    if ($scope[ 0 ] == 'year') {
        $where = 'event_slug LIKE ? ';
        $params = ['a4w' . $scope[ 1 ] . '%'];
    } else {
        $where = 'e.event_id = ? AND NOT anonymous ';
        $params = [$scope[ 1 ]];
        $groupby .= ', anonymous ';
    }
    
    $sql = 'SELECT    if( anonymous, \'Anonymous\', sponsor_name ) AS sponsor_name, ' .
           '          coalesce(sum(amount), 0) AS total_amt ' .
           'FROM      ' . A4W_SPONSORS_TABLE . ' AS s ' .
           'LEFT JOIN ' . A4W_EVENTS_TABLE . ' AS e ' .
           'ON        s.event_id = e.event_id ' .
           'WHERE     ' . $where .
           'GROUP BY  ' . $groupby .
           'ORDER BY  total_amt DESC, anonymous ' .
           'LIMIT     ' . intval( $size ) . ';';

    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_sponsors_in_range( $dbh, $ranges, $year = NULL ) {
    if (is_null( $year )) {
        $year = current_time( 'Y' );
    }
    
    $params = ['a4w' . $year . '%'];

    $where = implode( ' OR ', array_fill( 0, count( $ranges ), '(total_amt >= ? AND total_amt < ?)' ) );
    foreach ($ranges as $range) {
        array_push( $params, ...$range );
    }

    $sql = 'SELECT   DISTINCT sponsor_name ' .
           'FROM ' .
           '( ' .
           '    SELECT    sponsor_name, coalesce(sum(amount), 0) AS total_amt ' .
           '    FROM      ' . A4W_SPONSORS_TABLE . ' AS s ' .
           '    LEFT JOIN ' . A4W_EVENTS_TABLE . ' AS e ' .
           '    ON        s.event_id = e.event_id ' .
           '    WHERE     event_slug LIKE ? AND NOT anonymous ' .
           '    GROUP BY  sponsor_email ' .
           ') ' .
           'AS       a ' .
           'WHERE    ' . $where . ' ' .
           'ORDER BY sponsor_name;';
    
    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_top_participants( $dbh, $size = 10, $scope = NULL ) {
    if (is_null( $scope )) {
        $scope = ['year', current_time( 'Y' )];
    }
    
    if ($scope[ 0 ] == 'year') {
        $where = 'event_slug LIKE ? ';
        $params = ['a4w' . $scope[ 1 ] . '%'];
    } else {
        $where = 'e.event_id = ? ';
        $params = [$scope[ 1 ]];
    }
    
    $sql = 'SELECT    p.name, coalesce(sum(s.amount), 0) AS amt_raised ' .
           'FROM      ' . A4W_SPONSORS_TABLE . ' AS s ' . 
           'LEFT JOIN ' . A4W_BOOKINGS_TABLE . ' AS b ' .
           'ON        b.booking_id = s.participant_id ' .
           'LEFT JOIN ' . A4W_PARTICIPANTS_TABLE . ' AS p ' .
           'ON        b.booking_id = p.booking_id ' . 
           'LEFT JOIN ' . A4W_EVENTS_TABLE . ' AS e ' .
           'ON        b.event_id = e.event_id ' .
           'WHERE     ' . $where .
           'GROUP BY  b.booking_id ' . 
           'ORDER BY  amt_raised DESC ' .
           'LIMIT     ' . intval( $size ) . ';';
    
    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_ticket_types( $eventid = -1 ) {
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }
    
    $dbh = $r[ 1 ];
    
    if (is_null( $eventid ) || $eventid == -1) {
        $event_filter = '';
        $params = NULL;
    } else {
        $event_filter = 'WHERE     e.event_id = ? ';
        $params = [$eventid];
    }
    
    $sql = 'SELECT    e.event_id, tb.* ' .
           'FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           'LEFT JOIN ( ' .
           '    SELECT    event_id, t.ticket_id, ticket_name, ticket_description, ' .
           '              ticket_spaces, coalesce(sum(ticket_booking_spaces),0) ' .
           '    FROM      ' . A4W_TICKETS_TABLE . ' AS t ' .
           '    LEFT JOIN ' . A4W_BOOKINGS_XT_TABLE . ' AS b ' .
           '    ON        t.ticket_id = b.ticket_id ' .
           '    GROUP BY  t.ticket_id ' .
           ') AS tb ' .
           'ON        e.event_id = tb.event_id ' .
           $event_filter .
           'ORDER BY  e.event_id;';

    $r = a4w_db_get_results( $dbh, $sql, $params );
    if (! $r[ 0 ]) {
        return [];
    }
    
    $rs = $r[ 1 ];
    
    $events = [];
    foreach ($rs as $row) {
        $event_id = $row[ 0 ];
        if (array_key_exists( $event_id, $events )) {
            $total_available = $events[ $event_id ][ 'available' ];
        } else {
            $total_spaces = a4w_db_get_event_spaces( $dbh, $event_id );
            $total_regs = a4w_db_get_num_registrations( $dbh, $event_id );
            $total_available = $total_spaces - $total_regs;
            $events[ $event_id ] = [
                'spaces'    => $total_spaces,
                'bookings'  => $total_regs,
                'available' => $total_available,
                'tickets'   => []
            ];
        }
        if (! is_null( $row[ 2 ] )) {
            $ticket = [
                'id'          => $row[ 2 ],
                'name'        => $row[ 3 ],
                'description' => $row[ 4 ],
                'spaces'      => $row[ 5 ],
                'bookings'    => $row[ 6 ]
            ];
            $spaces = $row[ 5 ];
            $bookings = $row[ 6 ];
            $available = $spaces - $bookings;
            if ($available > $total_available) {
                $available = $total_available;
            }
            $ticket[ 'available' ] = $available;
            
            $events[ $event_id ][ 'tickets' ][] = $ticket;
        }
    }
    
    return $events;
}

function a4w_db_put_new_sponsor( $dbh, $event_id, $sponsor_name, $sponsor_email, $sponsor_amt,
                                 $booking_id, $participant, $notes, $anonymous, $order_num, $assigned ) {

    /*
    $participant_id = NULL;
    if (! is_null( $booking_id )) {
        if ($booking_id == 0) {
            $booking_id = NULL;
        } else {
            $sql = 'SELECT contact_id ' .
                   'FROM   ' . A4W_PARTICIPANTS_TABLE . ' ' .
                   'WHERE  booking_id = ?;';
            $params = [$booking_id];

            $r = a4w_db_get_result( $dbh, $sql, $params );
            if ($r[ 0 ]) {
                $participant_id = $r[ 1 ];
            }
        }
    }
    */

    a4w_admin_log( LOG_DEBUG );
    
    if (is_null( $participant )) {
        $participant = '';
    }
    if ($booking_id === 0) {
        $booking_id = NULL;
    } else if ((! is_null( $booking_id )) && $booking_id !== 0) {
		$assigned = TRUE;
	}

    $sql = 'INSERT INTO ' . A4W_SPONSORS_TABLE . ' (' .
           '    event_id, donation_date, sponsor_name, sponsor_email, ' .
           '    amount, is_tax_receipt, participant_name, notes, ' .
           '    anonymous, order_number, participant_id, assigned ' .
           ') VALUES (' .
           '    ?, CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ' .
           ');';
    $params = [
        $event_id, $sponsor_name, $sponsor_email, $sponsor_amt * 100, 1,
        $participant, $notes, $anonymous, $order_num, $booking_id, $assigned
    ];

    a4w_admin_log( LOG_DEBUG, print_r( [$sql, $params], TRUE ) );

    return a4w_db_put_data( $dbh, $sql, $params );
}

function a4w_db_put_new_participant( $dbh, $event_id, $ticket_id, $entry_id, $name,
                                     $email, $plan, $phone, $extra, $waiver ) {
    // returns [FALSE, error_msg] on failure
    //      or [TRUE, booking_id] on success

    $phone_digits = substr( preg_replace( '/\\D/', '', $phone ), 0, 10 );
    if (! $entry_id) {
        $entry_id = NULL;
    }

    $num_tix = 1;

    $sql = 'INSERT INTO ' . A4W_BOOKINGS_TABLE . ' (' .
           '    event_id, person_id, booking_spaces, booking_tax_rate, booking_meta ' .
           ') VALUES (' .
           '    ?, ?, ?, ?, ? ' .
           ');';
    $params = [$event_id, get_current_user_id(), $num_tix, 0, 'a:0:{}'];

    $r = a4w_db_put_data( $dbh, $sql, $params );
    if (! $r[ 0 ]) {
        return $r;
    }

    $booking_id = $r[ 1 ];

    $sql = 'INSERT INTO ' . A4W_PARTICIPANTS_TABLE . ' (' .
           '    booking_id, entry_id, name, email, phone, waiver_signed, plan_selfsponsor, extra_data ' .
           ') VALUES (' .
           '    ?, ?, ?, ?, ?, ?, ?, ? ' .
           ');';
    $params = [$booking_id, $entry_id, $name, $email, $phone_digits, intval( $waiver ), $plan, $extra];

    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sql', 'params' ), TRUE ) );
    $r = a4w_db_put_data( $dbh, $sql, $params );
    if (! $r[ 0 ]) {
        return $r;
    }

    if ($ticket_id > 0) {
        $sql = 'INSERT INTO ' . A4W_BOOKINGS_XT_TABLE . ' (' .
               '    booking_id, ticket_id, ticket_booking_spaces, ticket_booking_price ' .
               ') VALUES (' .
               '    ?, ?, ?, ? ' .
               ');';
        $params = [$booking_id, $ticket_id, $num_tix, 0.0];

        $r = a4w_db_put_data( $dbh, $sql, $params );
    }

    if (! $r[ 0 ]) {
        return $r;
    }

    return [TRUE, $booking_id];
}

function a4w_db_assign_sponsor( $dbh, $sponsor_id, $booking_id, $to_general ) {
    // returns [FALSE, error_msg] on failure
    //      or [TRUE, event_id] on success
    
    $r = a4w_db_get_sponsor_event( $dbh, $sponsor_id );
    
    if (! $r[ 0 ]) {
        return $r;
    }
    $event_id = $r[ 1 ];
    
    $booking_clause = '';
    $params = [TRUE];
    if (! $to_general) {
        $booking_clause = ', participant_id = ? ';
        $params[] = $booking_id;
    }
    $params[] = $sponsor_id;
    
    $sql = 'UPDATE ' . A4W_SPONSORS_TABLE .
           'SET    assigned = ? ' .
           $booking_clause .
           'WHERE  id = ?;';

    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sql', 'params' ), TRUE ) );
    $r = a4w_db_update_data( $dbh, $sql, $params );
    
    if (! r[ 0 ]) {
        return $r;
    }
    
    return [TRUE, $event_id];
}

function a4w_db_split_sponsor( $dbh, $sponsor_id, $new_amount ) {
    // returns [FALSE, error_msg] on failure
    //      or [TRUE, [sponsor_name, old_amt, new_amt_1, new_amt_2]] on success
    
    $r = a4w_db_get_sponsor_name( $dbh, $sponsor_id );
    if (! $r[ 0 ]) {
        return $r;
    }
    $name = $r[ 1 ];
    
    $r = a4w_db_get_sponsor_amount( $dbh, $sponsor_id );
    if (! $r[ 0 ]) {
        return $r;
    }
    $old_amount = $r[ 1 ];
    
    $sql = 'UPDATE ' . A4W_SPONSORS_TABLE .
           'SET    amount = ? ' .
           'WHERE  id = ?;';
    $params = [$new_amount, $sponsor_id ];
    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sql', 'params' ), TRUE ) );
    $r = a4w_db_update_data( $dbh, $sql, $params );
    
    if (! $r[ 0 ]) {
        return $r;
    }
    
    $new_amount_2 = $old_amount - $new_amount;
    $sql = 'INSERT INTO ' . A4W_SPONSORS_TABLE . ' (event_id, donation_date, ' .
           '            sponsor_name, sponsor_email, amount, ' .
           '            is_tax_receipt, participant_name, notes, ' .
           '            anonymous, order_number, participant_id, assigned) ' .
           'SELECT      event_id, donation_date, sponsor_name, ' .
           '            sponsor_email, ?, is_tax_receipt, ' .
           '            participant_name, notes, anonymous, order_number, ' .
           '            participant_id, assigned ' .
           'FROM        ' . A4W_SPONSORS_TABLE . ' ' .
           'WHERE       id = ?;';
    $params = [$new_amount_2, $sponsor_id];
    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sql', 'params' ), TRUE ) );
    $r = a4w_db_update_data( $dbh, $sql, $params );
    
    if (! $r[ 0 ]) {
        return $r;
    }
    
    return [TRUE, [$name, $old_amount, $new_amount, $new_amount_2]];
}

function a4w_db_get_participant_name( $dbh, $event_id, $booking_id ) {
    $sql = 'SELECT    name ' .
           'FROM      ' . A4W_BOOKINGS_TABLE . ' AS b ' .
           'LEFT JOIN ' . A4W_PARTICIPANTS_TABLE . ' AS p ' .
           'ON        b.booking_id = p.booking_id ' .
           'WHERE     event_id = ? ' .
           'AND       b.booking_id = ?;';
    $params = [$event_id, $booking_id];

    $r = a4w_db_get_result( $dbh, $sql, $params );
    if (is_null( $r[ 1 ] )) {
        $r = [FALSE, 'No booking found with given booking_id and event_id.'];
    }

    return $r;
}

function a4w_db_get_participant_details( $dbh, $event_id, $booking_id ) {
    $sql = 'SELECT    p.name, p.email, p.phone, p.waiver_signed, ' . 
           '          p.plan_selfsponsor, p.extra_data, tx.ticket_name ' .
           'FROM      ' . A4W_BOOKINGS_TABLE . ' AS b ' .
           'LEFT JOIN ' . A4W_PARTICIPANTS_TABLE . ' AS p ' .
           'ON        b.booking_id = p.booking_id ' .
           'LEFT JOIN ( ' .
           '    SELECT booking_id, ticket_name ' .
           '    FROM ' . A4W_BOOKINGS_XT_TABLE . ' AS tb ' .
           '    LEFT JOIN ' . A4W_TICKETS_TABLE . ' AS t ' .
           '    ON tb.ticket_id = t.ticket_id ' .
           ') AS tx ' .
           'ON        p.booking_id = tx.booking_id ' .
           'WHERE     event_id = ? ' .
           'AND       b.booking_id = ?;';
    $params = [$event_id, $booking_id];

    $r = a4w_db_get_results( $dbh, $sql, $params );
    if (is_null( $r[ 1 ] )) {
        return [FALSE, 'No booking found with given booking_id and event_id.'];
    }

    a4w_admin_log( LOG_DEBUG, print_r( $r, TRUE ) );
    
    $pdata = $r[ 1 ][ 0 ];
    $r = [
        TRUE, [
            'name'        => $pdata[ 0 ],
            'email'       => $pdata[ 1 ],
            'phone'       => $pdata[ 2 ],
            'waiver'      => $pdata[ 3 ],
            'selfsponsor' => $pdata[ 4 ],
            'extra'       => $pdata[ 5 ],
            'category'    => $pdata[ 6 ]
        ]
    ];

    return $r;
}

function a4w_db_get_participant_sponsors( $dbh, $event_id, $booking_id ) {
    $sql = 'SELECT    donation_date AS date, ' .
           "          IF( anonymous, '(Anonymous)', sponsor_name ) AS name, " .
           '          (amount / 100) AS amount ' .
           'FROM      ' . A4W_SPONSORS_TABLE . ' ' .
           'WHERE     event_id = ? ' .
           'AND       participant_id = ? ' .
           'ORDER BY  donation_date DESC;';
    $params = [$event_id, $booking_id];

    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sql', 'params' ), TRUE ) );
    
    $r = a4w_db_get_results( $dbh, $sql, $params );
    if (is_null( $r[ 1 ] )) {
        return [FALSE, 'No booking found with given booking_id and event_id.'];
    }
    
    a4w_admin_log( LOG_DEBUG, print_r( $r, TRUE ) );
    
    return $r;
}

function a4w_db_remove_participant( $dbh, $event_id, $booking_id ) {
    $r = a4w_db_get_participant_name( $dbh, $event_id, $booking_id );
    if (! $r[ 0 ]) {
        return $r;
    }
    $participant_name = $r[ 1 ];

    $params = [$booking_id];
    foreach ([A4W_BOOKINGS_TABLE, A4W_PARTICIPANTS_TABLE, A4W_BOOKINGS_XT_TABLE] as $table) {
        $sql = 'DELETE FROM ' . $table . ' ' .
               'WHERE       booking_id = ?;';
        $r = a4w_db_delete_data( $dbh, $sql, $params );
        
        if (! $r[ 0 ]) {
            return $r;
        } elseif ($r[ 1 ] == 0) {
            return [FALSE, 'No record deleted from ' . $table . ' with given booking_id.'];
        }
    }

    return [TRUE, $participant_name];
}

function a4w_db_get_event_id_from_slug( $dbh, $slug ) {
    $sql = 'SELECT event_id ' .
           'FROM   ' . A4W_EVENTS_TABLE . ' ' .
           'WHERE  event_slug = ?;';
    $params = [$slug];
    
    $r = a4w_db_get_result( $dbh, $sql, $params );
    return $r[ 1 ];
}

function a4w_db_get_sponsor_event( $dbh, $sponsor_id ) {
    $sql = 'SELECT event_id ' .
           'FROM   ' . A4W_SPONSORS_TABLE . ' ' .
           'WHERE  id = ?;';
    $params = [$sponsor_id];
    
    return a4w_db_get_result( $dbh, $sql, $params );
}

function a4w_db_get_sponsor_name( $dbh, $sponsor_id ) {
    $sql = 'SELECT sponsor_name ' .
           'FROM   ' . A4W_SPONSORS_TABLE . ' ' .
           'WHERE  id = ?;';
    $params = [$sponsor_id];
    
    return a4w_db_get_result( $dbh, $sql, $params );
}

function a4w_db_get_sponsor_amount( $dbh, $sponsor_id ) {
    $sql = 'SELECT amount ' .
           'FROM   ' . A4W_SPONSORS_TABLE . ' ' .
           'WHERE  id = ?;';
    $params = [$sponsor_id];
    
    return a4w_db_get_result( $dbh, $sql, $params );
}

function a4w_db_get_event_meta( $dbh, $event_id, $metakey, $default = NULL ) {
    $sql = 'SELECT    meta_value ' .
           'FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS m ' .
           'ON        e.post_id = m.post_id ' .
           'WHERE     event_id = ? ' .
           'AND       meta_key = ?';
    $params = [$event_id, $metakey];

    if (is_null( $default )) {
        $sql .= ';';
    } else {
        $sql = 'SELECT coalesce((' . $sql . '), ' . $default . ');';
    }

    return a4w_db_get_result( $dbh, $sql, $params );
}

function a4w_db_is_valid_code( $dbh, $event_id, $secret_code ) {
    $r = a4w_db_get_event_meta( $dbh, $event_id, 'admin_code' );

    return ($r[ 1 ] === $secret_code);
}

function a4w_db_get_fundraising_amount( $dbh, $event_id = NULL ) {
    if ($event_id === NULL) {
        return a4w_db_get_all_fundraising_amounts( $dbh );
    }
    
    $minimum = a4w_db_get_event_meta( $dbh, $event_id, 'minimum_perperson', -1 );
    $suggestion = a4w_db_get_event_meta( $dbh, $event_id, 'suggestion_perperson', -1 );
    
    return ['minimum' => $minimum[ 1 ], 'suggestion' => $suggestion[ 1 ]];
}

function a4w_db_get_all_fundraising_amounts( $dbh ) {
    $amounts = [];
    
    $minimum = 'minimum_perperson';
    $suggestion = 'suggestion_perperson';

    $sql = 'SELECT    event_id, meta_key, meta_value ' .
           'FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS m ' .
           'ON        e.post_id = m.post_id ' .
           'WHERE     meta_key IN ( ?, ? ) ' .
           'ORDER BY  event_id;';
    $params = [$minimum, $suggestion];

    $r = a4w_db_get_results( $dbh, $sql, $params );
    
    if (! $r[ 0 ]) {
        return $amounts;
    }

    foreach ($r[ 1 ] as $row) {
        $event_id = $row[ 0 ];
        
        if (! array_key_exists( $event_id, $amounts )) {
            $amounts[ $event_id ] = ['minimum' => NULL, 'suggestion' => NULL];
        }
        
        $key = ($row[ 1 ] == $minimum) ? 'minimum' : 'suggestion';
        $amounts[ $event_id ][ $key ] = $row[ 2 ];
    }
    
    return $amounts;
}

function a4w_db_get_coordinator_name( $dbh, $event_id ) {
    $r = a4w_db_get_event_meta( $dbh, $event_id, 'coordinator_name' );
    return $r[ 1 ];
}

function a4w_db_get_event_field( $dbh, $event_id, $fieldname ) {
    $sql = 'SELECT ' . $fieldname . ' FROM ' . A4W_EVENTS_TABLE . ' WHERE event_id = ?;';
    $params = [$event_id];

    $r = a4w_db_get_result( $dbh, $sql, $params );
    return $r[ 1 ];
}

function a4w_db_get_event_name( $dbh, $event_id ) {
    return a4w_db_get_event_field( $dbh, $event_id, 'event_name' );
}

function a4w_db_get_event_spaces( $dbh, $event_id ) {
    return a4w_db_get_event_field( $dbh, $event_id, 'coalesce(event_spaces, 0)' );
}

function a4w_db_get_event_slug( $dbh, $event_id ) {
    return a4w_db_get_event_field( $dbh, $event_id, 'event_slug' );
}

function a4w_db_get_num_registrations( $dbh, $event_id ) {
    $sql = 'SELECT sum(booking_spaces) FROM ' . A4W_BOOKINGS_TABLE . ' WHERE event_id = ? AND booking_status = 1;';
    $params = [$event_id];

    $r = a4w_db_get_result( $dbh, $sql, $params );
    return $r[ 1 ];
}

function a4w_db_get_event_amount_raised( $dbh, $event_id ) {
    $sql = 'SELECT coalesce(sum(amount), 0) FROM ' . A4W_SPONSORS_TABLE . ' WHERE event_id = ?;';
    $params = [$event_id];

    $r = a4w_db_get_result( $dbh, $sql, $params );
    return ($r[ 1 ] / 100);
}

function a4w_db_get_event_goal( $dbh, $event_id ) {
    $r = a4w_db_get_event_meta( $dbh, $event_id, 'fundraising_goal' );
    $goal = $r[ 1 ];

    return intval( str_replace( ',', '', $goal ) );
}

function a4w_db_get_event_metrics( $dbh, $event_id ) {
    $metric_categories = ['distance', 'elevation', 'totaltime', 'difficulty', 'otherlabel', 'other'];
    
    $sql = 'SELECT    meta_key, meta_value ' .
           'FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS m ' .
           'ON        e.post_id = m.post_id ' .
           'WHERE     event_id = ? ' .
           'AND       meta_key IN ( ?, ?, ?, ?, ?, ? ) ' .
           'ORDER BY  event_id;';
    $params = array_map( fn( $cat ) => 'metric_' . $cat, $metric_categories );
    array_unshift( $params, $event_id );
    
    $metrics = [];
    $r = a4w_db_get_results( $dbh, $sql, $params );
    if ($r[ 0 ]) {
        foreach ($r[ 1 ] as $row) {
            $cat = explode( '_', $row[ 0 ] )[ 1 ];
            $metrics[ a4w_propercase( $cat ) ] = $row[ 1 ];
        }
        if (array_key_exists( 'Other', $metrics ) && $metrics[ 'Other' ]) {
            $metrics[ $metrics[ 'Otherlabel' ] ] = $metrics[ 'Other' ];
        }
        unset( $metrics[ 'Otherlabel' ] );
        unset( $metrics[ 'Other' ] );
    }
    a4w_admin_log( LOG_DEBUG, 'Metrics: ' . print_r( $metrics, TRUE ) );
    
    return $metrics;
}

function a4w_db_get_event_categories( $dbh, $event_id ) {
    $sql = 'SELECT    t.slug ' .
           'FROM      ' . A4W_TERMS_TABLE . ' AS t ' .
           'LEFT JOIN ' . A4W_RELATIONS_TABLE . ' AS tr ' .
           'ON        tr.term_taxonomy_id = t.term_id ' .
           'LEFT JOIN ' . A4W_EVENTS_TABLE . ' AS e ' .
           'ON        tr.object_id = e.post_id ' .
           'WHERE     event_id = ?;';
    $params = [$event_id];
    
    $cats = [];
    $r = a4w_db_get_results( $dbh, $sql, $params );
    if ($r[ 0 ]) {
        foreach ($r[ 1 ] as $row) {
            $cats[] = $row[ 0 ];
        }
    }
    
    return $cats;
}

function a4w_db_get_coordinator_info( $dbh, $event_id ) {
    $sql = 'SELECT    meta_key, meta_value ' .
           'FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS m ' .
           'ON        e.post_id = m.post_id ' .
           'WHERE     event_id = ? ' .
           'AND       meta_key LIKE \'coordinator\\_%\';';
    $params = [$event_id];

    $info = [];
    $r = a4w_db_get_results( $dbh, $sql, $params );
    if ($r[ 0 ]) {
        foreach ($r[ 1 ] as $row) {
            $info[ $row[ 0 ] ] = $row[ 1 ];
        }
    }

    return $info;
}

function a4w_db_get_event_outline( $dbh, $event_id ) {
    $event = [
        'adv_name' => a4w_db_get_event_name( $dbh, $event_id ),
        'adv_slug' => a4w_db_get_event_slug( $dbh, $event_id )
    ];
    $coordinator = a4w_db_get_coordinator_info( $dbh, $event_id );

    return array_merge( $event, $coordinator );
}

function a4w_db_get_event_participant_details( $dbh, $event_id = NULL ) {
    if (is_null( $event_id ) || $event_id == '*') {
        $event_filter = 'b.event_id IS NOT NULL ';
        $params = NULL;
    } else {
        $event_filter = 'b.event_id = ? ';
        $params = [$event_id];
    }

    $sql = 'SELECT     p.booking_id, event_name, entry_id, name, email, phone, ' .
           '           waiver_signed, coalesce(total_amt, 0.0) as amt_sponsored, ' .
           '           plan_selfsponsor, extra_data, ticket_name ' .
           'FROM       ' . A4W_PARTICIPANTS_TABLE . ' AS p ' .
           'INNER JOIN ' . A4W_BOOKINGS_TABLE . ' AS b ' .
           'ON         p.booking_id = b.booking_id ' .
           'LEFT JOIN  ' . A4W_EVENTS_TABLE . ' AS e ' .
           'ON         b.event_id = e.event_id ' .
           'LEFT JOIN ( ' .
           '    SELECT   participant_id, (sum(amount) / 100) as total_amt ' .
           '    FROM     ' . A4W_SPONSORS_TABLE . ' ' .
           '    GROUP BY participant_id ' .
           ') AS s ' .
           'ON        p.booking_id = s.participant_id ' .
           'LEFT JOIN ( ' .
           '    SELECT booking_id, ticket_name ' .
           '    FROM ' . A4W_BOOKINGS_XT_TABLE . ' AS tb ' .
           '    LEFT JOIN ' . A4W_TICKETS_TABLE . ' AS t ' .
           '    ON tb.ticket_id = t.ticket_id ' .
           ') AS tx ' .
           'ON        p.booking_id = tx.booking_id ' .
           'WHERE     ' . $event_filter .
           'ORDER BY  event_name, name;';

    a4w_admin_log( LOG_DEBUG, print_r( [$sql, $params], TRUE ) );

    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_event_participants( $dbh = NULL, $event_id = NULL ) {
    if (is_null( $event_id ) || $event_id == -1) {
        $event_filter = '';
        $params = NULL;
        $events = [];
    } else {
        $event_filter = 'WHERE     e.event_id = ? ';
        $params = [$event_id];
        $events = [$event_id => []];
    }

    if (is_null( $dbh )) {
        $r = a4w_db_get_dbh();
        if (! $r[ 0 ]) {
            return $events;
        }

        $dbh = $r[ 1 ];
    }

    $sql = 'SELECT    e.event_id, bp.* ' .
           'FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           'LEFT JOIN ( ' .
           '    SELECT    p.booking_id, name, b.event_id, t.ticket_name ' .
           '    FROM      ' . A4W_PARTICIPANTS_TABLE . ' AS p ' .
           '    LEFT JOIN ' . A4W_BOOKINGS_TABLE . ' AS b ' .
           '    ON        p.booking_id = b.booking_id ' .
           '    LEFT JOIN ' . A4W_BOOKINGS_XT_TABLE . ' AS bt ' .
           '    ON        p.booking_id = bt.booking_id ' .
           '    LEFT JOIN ' . A4W_TICKETS_TABLE . ' AS t ' .
           '    ON        bt.ticket_id = t.ticket_id ' .
           ') AS bp ' .
           'ON        e.event_id = bp.event_id ' .
           $event_filter .
           'ORDER BY  e.event_id, bp.name, bp.booking_id DESC;';

    $r = a4w_db_get_results( $dbh, $sql, $params );
    if (! $r[ 0 ]) {
        return $events;
    }

    foreach ($r[ 1 ] as $row) {
        $id = $row[ 0 ];
        if (! array_key_exists( $id, $events )) {
            $events[ $id ] = [];
        }

        if (! is_null( $row[ 1 ] )) {
            $events[ $id ][] = [
                'booking'  => $row[ 1 ],
                'name'     => $row[ 2 ],
                'category' => $row[ 4 ]
            ];
        }
    }
    return $events;
}

function a4w_db_get_event_sponsorships( $dbh, $event_id ) {
    $sql = "SELECT coalesce(participant_name, ''), sponsor_name, (amount / 100) " .
           'FROM   ' . A4W_SPONSORS_TABLE . ' ' .
           'WHERE  event_id = ?;';
    $params = [$event_id];

    $parts = [];
    $r = a4w_db_get_results( $dbh, $sql, $params );
    if ($r[ 0 ]) {
        foreach ($r[ 1 ] as $row) {
            if (array_key_exists( $row[ 0 ], $parts )) {
                $parts[ $row[ 0 ] ][] = [$row[ 1 ], $row[ 2 ]];
            } else {
                $parts[ $row[ 0 ] ] = [[$row[ 1 ], $row[ 2 ]]];
            }
        }
    }

    return $parts;
}

function a4w_db_get_event_sponsorship_details( $dbh, $event_id = NULL ) {
    if (is_null( $event_id ) || $event_id == '*') {
        $params = NULL;
    } else {
        $event_filter = 'WHERE     s.event_id = ? ';
        $params = [$event_id];
    }

    $sql = 'SELECT    s.id, s.order_number, e.event_name, s.donation_date, ' .
           '          s.sponsor_name, s.sponsor_email, (s.amount/100) AS sponsor_amt, ' .
           "          IF(s.anonymous = 1, 'Y', '') AS anon, " . 
		   "          COALESCE(p.name, '') AS participant_name, " .
		   "          COALESCE(s.notes, '') AS sponsor_notes " .
           'FROM      ' . A4W_SPONSORS_TABLE . ' AS s ' .
           'LEFT JOIN ' . A4W_EVENTS_TABLE . ' AS e ' .
           'ON        s.event_id = e.event_id ' .
		   'LEFT JOIN ' . A4W_PARTICIPANTS_TABLE . ' AS p ' .
		   'ON        s.participant_id = p.booking_id ' .
           $event_filter .
           'ORDER BY  s.id;';
    
    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_event_summary_table( $dbh, $year = NULL ) {
    $yearspec = 'a4w' . $year . '%';
    $sql = 'SELECT    event_name, cname, me.meta_value AS `coordinator email`, ' .
           '          msd.meta_value AS `start date`, md.meta_value AS `end date`, ' .
           '          mc.meta_value AS `admin code` ' .
           'FROM      ' . A4W_EVENTS_TABLE . ' AS e ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS me ' .
           'ON        e.post_id = me.post_id ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS msd ' .
           'ON        e.post_id = msd.post_id ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS md ' .
           'ON        e.post_id = md.post_id ' .
           'LEFT JOIN ' . A4W_META_TABLE . ' AS mc ' .
           'ON        e.post_id = mc.post_id ' .
           'LEFT JOIN ' .
           '( ' .
           '    SELECT   post_id, ' .
           "             trim( group_concat( meta_value ORDER BY meta_key DESC SEPARATOR ' ')) AS cname " .
           '    FROM     ' . A4W_META_TABLE . ' ' .
           "    WHERE    meta_key IN ('coordinator_name', 'coordinator_extra') " .
           '    GROUP BY post_id ' .
           ') AS cn ' .
           'ON        e.post_id = cn.post_id ' .
           'WHERE     event_slug LIKE ? AND ' .
           "          me.meta_key = 'coordinator_email' AND " .
           "          msd.meta_key = 'adventure_start_date' AND " .
           "          md.meta_key = 'adventure_date' AND " .
           "          mc.meta_key = 'admin_code' " .
           'ORDER BY  event_name;';
    $params = [$yearspec];

    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_event_dates( $dbh, $event_id ) {
    $sql = 'SELECT a4w_get_adventure_start_date( ? ), ' .
           '       a4w_get_adventure_end_date( ? );';
    $params = [$event_id, $event_id];
    
    $r = a4w_db_get_results( $dbh, $sql, $params );
    $dates = [];
    if (! $r[ 0 ]) {
        return NULL;
    }
    
    $dates = [
        'start' => strtotime( $r[ 1 ][ 0 ][ 0 ] ),
        'end'   => strtotime( $r[ 1 ][ 0 ][ 1 ] )
    ];
    
    return $dates;
}

function a4w_db_get_event_header_info( $dbh, $event_id ) {
    $sql = 'SELECT post_id, event_name, event_slug ' .
           'FROM   ' . A4W_EVENTS_TABLE . ' ' .
           'WHERE  event_id = ?;';
    $params = [$event_id];

    return a4w_db_get_results( $dbh, $sql, $params );
}

function a4w_db_get_ticket_name( $dbh, $ticket_id ) {
    $sql = 'SELECT ifnull( (' .
           '    SELECT ticket_name ' .
           '    FROM   ' . A4W_TICKETS_TABLE . ' ' .
           '    WHERE  ticket_id = ? ' .
           '), NULL );';
    $params = [$ticket_id];

    return a4w_db_get_result( $dbh, $sql, $params );
}

function a4w_db_get_invite_form_data( $dbh ) {
    $form_id = 6;

    $sql = 'SELECT display_meta ' .
           'FROM   ' . A4W_FORMDATA_TABLE . ' ' .
           'WHERE  form_id = ?;';
    $params = [$form_id];

    return a4w_db_get_result( $dbh, $sql, $params );
}

function a4w_db_verify_participant_info( $dbh, $booking_id, $token, $name ) {
    $name = trim( $name );
    
    $sql = 'SELECT    b.event_id ' .
           'FROM      ' . A4W_BOOKINGS_TABLE . ' AS b ' .
           'LEFT JOIN ' . A4W_PARTICIPANTS_TABLE . ' AS p ' .
           'ON        b.booking_id = p.booking_id ' .
           'WHERE     b.booking_id = ? ' .
           'AND       p.token = ? ' .
           'AND       (p.name = ? ' .
           '    OR     p.name LIKE ?);';
    $params = [$booking_id, $token, $name, (a4w_db_safe_search_pattern( $name ) . ' %')];
    
    $r = a4w_db_get_result( $dbh, $sql, $params );
    if (! $r[ 0 ]) {
        return FALSE;
    }
    
    return $r[ 1 ];
}

function a4w_db_get_personal_page_params( $dbh, $booking_id ) {
    $sql = 'SELECT token, ' . 
           '       name ' . 
           'FROM   ' . A4W_PARTICIPANTS_TABLE . ' ' .
           'WHERE  booking_id = ?;';
    $params = [$booking_id];
    
    $r = a4w_db_get_results( $dbh, $sql, $params );
    if (! $r[ 0 ]) {
        return $r;
    }
    
    list( $token, $pname ) = $r[ 1 ][ 0 ];
    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sql', 'params', 'r', 'token', 'pname' ), TRUE ) );
    
    if ((! $token) || (! is_numeric( $token )) || (strlen( intval( $token ) ) != 8)) {
        return [FALSE, 'Unable to get participant\'s verification token.'];
    }
    
    if (! $pname) {
        return [FALSE, 'Unable to get participant\'s name.'];
    }
    
    return [
        TRUE,
        [
            'token' => $token,
            'name'  => a4w_propercase( explode( ' ', $pname )[ 0 ] )
        ]
    ];
}

?>