<?php

/****************************************************************************
 *
 * Various functions, hooks, etc. to be used by code piggybacking on the
 *  Stonehenge Events Manager OSM plugin, to display a provincial map
 *  showing all the events across the province.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-01-25
 *
 ****************************************************************************/

require_once( __DIR__ . '/a4w-db-functions.php' );
require_once( __DIR__ . '/events-manager-functions.php' );

//
// A shortcode that allows us to insert the provincial map
//****************************************************************************

add_shortcode( 'a4w-provincial-osm-map', 'a4w_sc_show_provincial_map' );

function a4w_sanitize_js_string( $str ) {
    $from = array( '"', "\r", "\n" );
    $to = array( '&quot;', '', '' );    

    return str_replace( $from, $to, $str );
}

function a4w_sc_show_provincial_map( $atts, $content = NULL ) {
    $nl = "\n";
    
    $defaults = [
        'years' => NULL
    ];
    extract( shortcode_atts( $defaults, $atts ) );
    
    $sosm_folder = plugins_url( 'stonehenge-em-osm' );
    $js_folder = content_url( '/mu-plugins/a4w-osm' );
    
    $domid = 'a4w-provincial-map';
    
    $events = a4w_osm_get_event_locations( $years );
    
    $output = '<div class="em-osm-container-outer" ' . $nl .
              '     style="width:calc(100% - 20px);padding-bottom:calc(120% - 24px);margin:0px auto 20px auto;position:relative;">' . $nl .
              '<div id="em-osm-provincial-map-container" class="em-osm-container" ' . $nl .
              '     style="border:2px solid #2e633d;position:absolute;top:0;bottom:0;left:0;right:0;">' . $nl .
              '<link rel="stylesheet" href="' . $sosm_folder . '/assets/public-stonehenge-em-osm.min.css">' . $nl .
              '<script src="' . $sosm_folder . '/assets/public-stonehenge-em-osm.min.js"></script>' . $nl .
              '<script src="' . $js_folder . '/a4w-mapping-functions.js"></script>' . $nl .
              '<div id="' . $domid . '" class="em-osm-map" style="position:relative;width:100%;height:100%;"></div>' . $nl .
              '<script>' . $nl .
              '    var adventures = [' . $nl;
    foreach ($events as $event) {
        $output .= '        {' .
                   'name: "' . $event[ 'name' ] . '", ' .
                   'coordinator: "' . $event[ 'coordinator' ] . '", ' .
                   'slug: "' . $event[ 'slug' ] . '", ' .
                   'excerpt: "' . a4w_sanitize_js_string( $event[ 'excerpt' ] ) . '", ' .
                   'location: "' . $event[ 'location' ] . '", ' .
                   'lat: ' . $event[ 'lat' ] . ', ' .
                   'lng: ' . $event[ 'lng' ] . ', ' .
                   'thumbnail: "' . $event[ 'thumbnail' ] . '"},' . $nl;
    }
    $output .= '    ];' . $nl .
               '    render_provincial_map( "' . $domid . '", "' . $sosm_folder . '", adventures );' . $nl .
               '</script>' . $nl .
               '</div>' . $nl .
               '</div>';
    
    return $output;
}

function a4w_osm_get_event_locations( $years = NULL ) {
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return array();
    }
    
    $dbh = $r[ 1 ];
    
    return a4w_db_get_event_location_snippets( $dbh, $years );
}

function a4w_db_get_event_location_snippets( $dbh, $years ) {
    $events_table = a4w_db_get_table_name( 'em_events' );
    $locations_table = a4w_db_get_table_name( 'em_locations' );
    $posts_table = a4w_db_get_table_name( 'posts' );
    $meta_table = a4w_db_get_table_name( 'postmeta' );
    
    $years = a4w_db_sanitize_years( $years );
    if ($years == NULL) {
        $yearfilter = '';
    } else {
        $yearfilter = 'AND        SUBSTRING( event_slug, 4, 4 ) in (' . $years . ')';
    }
    
    $sql = 'SELECT     event_name, meta_value, event_slug, post_excerpt, ' .
           '           location_name, location_latitude, location_longitude, p.id ' .
           'FROM       ((' . $events_table . ' AS e ' .
           'INNER JOIN   ' . $locations_table . ' AS l ' .
           'ON           e.location_id = l.location_id) ' .
           'LEFT JOIN   ' . $posts_table . ' AS p ' .
           'ON          e.post_id = p.id) ' .
           'LEFT JOIN  ' . $meta_table . ' AS m ' .
           'ON         e.post_id = m.post_id ' .
           'WHERE      meta_key = ? ' .
           'AND        NOT event_private ' .
           $yearfilter . ';';
    
    $params = array( 'coordinator_name' );
    
    $events = array();
    
    $r = a4w_db_get_results( $dbh, $sql, $params );
    if ($r[ 0 ]) {
        foreach ($r[ 1 ] as $event) {
            $events[] = array(
                'name'        => $event[ 0 ],
                'coordinator' => $event[ 1 ],
                'slug'        => $event[ 2 ],
                'excerpt'     => $event[ 3 ],
                'location'    => $event[ 4 ],
                'lat'         => $event[ 5 ],
                'lng'         => $event[ 6 ],
                'thumbnail'   => get_the_post_thumbnail_url( $event[ 7 ] )
            );
        }
    }
    
    return $events;
}

function a4w_db_sanitize_years( $years ) {
    if ($years == NULL) {
        return NULL;
    }
    
    $intyears = [];
    foreach (explode( ',', $years ) as $year) {
        $intyears[] = intval( $year );
    }
    
    return implode( ',', $intyears );
}

?>