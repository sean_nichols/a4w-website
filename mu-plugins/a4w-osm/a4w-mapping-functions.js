var prov_map;

function render_provincial_map( domid, sosm_path, adventures ) {
    ismobile = L.Browser.mobile;
    
    var mobileDrag = ! ismobile;
    var mobileZoom = ! ismobile;
    var zoomButtons = ! ismobile;
    var showScale = ! ismobile;
    
    var zoomLevel = (ismobile ? 5 : 7);
    
    var pluginUrl = sosm_path + '/assets/images/marker-icon-2x-',
        shadowUrl = sosm_path + '/assets/images/marker-shadow.png',
        mapOptions = {
            zoom               : zoomLevel,
            zoomSnap           : 0.25,
            zoomControl        : true,
            zoomDisplayControl : false,
            scrollWheelZoom    : mobileZoom,
            dragging           : true
        },
        mapIcon = new L.Icon( {} );
        LeafIcon = L.Icon.extend( {
            options : {
                iconSize    : [ 25, 41 ],
                iconAnchor  : [ 12, 41 ],
                popupAnchor : [ 1, -40 ],
                shadowSize  : [ 41, 41 ],
                shadowUrl   : sosm_path + '/assets/images/marker-shadow.png'
            }
        } ),
        showFullscreen = false,
        markerName = 'blue',
        mapTile = '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        
    prov_map = L.map( domid, mapOptions );

    var setMaxZoom = 20,
        setNatZoom = 18;

    prov_map.setView( [ 51.539515, -113.340537 ] );

    L.tileLayer( mapTile, {
        attribution   : '&copy; <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">OpenStreetMap</a>',
        reuseTiles    : true,
        detectRetina  : true,
        minZoom       : 1,
        maxZoom       : L.Browser.retina ? setMaxZoom : setMaxZoom - 1,
        maxNativeZoom : L.Browser.retina ? setNatZoom : setNatZoom + 1
    } ).addTo( prov_map );

    mapIcon = new LeafIcon( {
        iconUrl   : pluginUrl + markerName + '.png',
        shadowUrl : shadowUrl
    } );

    for (var i = 0; i < adventures.length; i++) {
        var adventure = adventures[ i ];
        
        var content = '<table style="border-collapse:separate;border-spacing:3px;max-width:400px;"><tr>' +
                      '<td style="vertical-align:top;">' +
                      '    <img src="' + adventure.thumbnail + '" style="max-width:100px;max-height:200px;width:auto;height:auto;border:1px solid #2e633d;">' +
                      '</td>' +
                      '<td style="vertical-align:top;">' +
                      '    <span style="font-weight:bold;font-size:larger;"><a href="/adventures/' + adventure.slug + '/">' + adventure.name + '</a></span><br/>' +
                      '    <span style="font-style:italic;padding-left:30px;">with ' + adventure.coordinator + '</span><br/><br/>' +
                      '    ' + adventure.location + '<br/>' +
                      '    <p>' + adventure.excerpt + '</p>' +
                      '</td>' +
                      '</tr></table>';
        var opts = {
            maxWidth : 400
        };

        var marker = L.marker( [ adventure.lat, adventure.lng ], { icon : mapIcon } ).addTo( prov_map ).bindPopup( content, opts ).dragging.disable();
    }

    if (showScale) {
        L.control.scale().addTo( prov_map );
    }

    setTimeout( function() { prov_map.invalidateSize(); } , 400 );
}
