<?php

/****************************************************************************
 *
 * Various functions, hooks, etc. to be used by customizations for the
 *  A4W administration page.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-01-16
 *
 ****************************************************************************/

require_once( __DIR__ . '/a4w-db-functions.php' );
require_once( __DIR__ . '/a4w-version.php' );

if (! function_exists( 'a4w_parse_bool' )) {
    function a4w_parse_bool( $boolstr ) {
        $falses = ['f', 'false', 'n', 'no', '', '0'];
        if (in_array( strtolower( $boolstr ), $falses, TRUE )) {
            return FALSE;
        }
        
        return TRUE;
    }
}

//
// First set up the AJAX REST API Stuff
//****************************************************************************

add_action( 'wp_enqueue_scripts', 'a4w_admin_setup_api_client_scripts' );
add_action( 'rest_api_init', 'a4w_admin_register_api_endpoints' );

function a4w_admin_register_api_endpoints() {
    $apis = [
//        ['name' => 'test',                  'method' => 'GET',  'who' => 'user'     ],
        ['name' => 'sponsor',                'method' => 'POST', 'who' => 'admin'    ],
        ['name' => 'register',               'method' => 'POST', 'who' => 'admin'    ],
        ['name' => 'unregister',             'method' => 'POST', 'who' => 'admin'    ],
        ['name' => 'get_personal_page_link', 'method' => 'GET',  'who' => 'admin'    ],
        ['name' => 'assign_sponsor',         'method' => 'POST', 'who' => 'admin'    ],
        ['name' => 'split_sponsor',          'method' => 'POST', 'who' => 'admin'    ],
        ['name' => 'transfer_shop_order',    'method' => 'POST', 'who' => 'anyone'   ],
        ['name' => 'download_participants',  'method' => 'GET',  'who' => 'admin'    ],
        ['name' => 'download_donations',     'method' => 'GET',  'who' => 'financial'],
        ['name' => 'view_fundraising',       'method' => 'GET',  'who' => 'anyone'   ],
        ['name' => 'get_invite_adv',         'method' => 'GET',  'who' => 'anyone'   ],
        ['name' => 'get_website_stats',      'method' => 'GET',  'who' => 'admin'    ]
    ];

    foreach ($apis as $api) {
        register_rest_route( 'a4w-admin/v1', '/' . $api[ 'name' ], [
            'methods'             => $api[ 'method' ],
            'callback'            => 'a4w_admin_api_' . $api[ 'name' ] ,
            'permission_callback' => 'a4w_admin_permission_cb_' . $api[ 'who' ]
        ] );
    }
}

function a4w_admin_permission_cb_anyone() {
    return true;
}

function a4w_admin_permission_cb_user() {
    return is_user_logged_in();
}

function a4w_admin_permission_cb_financial() {
    return (get_current_user_id() == A4W_COORDINATOR_USERID || current_user_can( 'view_finances' ));
}

function a4w_admin_permission_cb_admin() {
    $allowed_roles = [
        'editor',
        'author',
        'administrator'
    ];
    return (count( array_intersect( $allowed_roles, wp_get_current_user()->roles ) ) > 0);
}

function a4w_admin_setup_api_client_scripts() {
    wp_enqueue_script( 'a4w-admin-script', '/wp-content/mu-plugins/a4w-admin/a4w-admin-functions.js', [], A4W_ADMIN_VERSION );

    // Register custom variables for the AJAX script.
    wp_localize_script( 'a4w-admin-script', 'wpApiSettings', [
        'root'  => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );

    wp_enqueue_style( 'a4w-admin-style', '/wp-content/mu-plugins/a4w-admin/a4w-admin-style.css', [], A4W_ADMIN_VERSION );
}

function a4w_admin_api_get_optional_param( $params, $key, $default = NULL ) {
    if (array_key_exists( $key, $params )) {
        return $params[ $key ];
    }
    return $default;
}

function a4w_admin_api_get_bool_param( $params, $key ) {
    return a4w_parse_bool( a4w_admin_api_get_optional_param( $params, $key, FALSE ) );
}

function a4w_admin_api_test( $request ) {
    $data = ['ok' => TRUE, 'revision' => A4W_ADMIN_VERSION];
    if (isset( $request[ 'cparam' ] )) {
        $data[ 'param' ] = $request[ 'cparam' ];
    }
    $data[ 'user' ] = [
        'logged_in' => is_user_logged_in(),
        'roles'     => wp_get_current_user()->roles
    ];

    $msg = print_r( $data, TRUE );
    $content = [
        'plain' => $msg,
        'html' => '<pre>' . $msg . '</pre>'
    ];

    $to = a4w_admin_make_qualified_email_address( A4W_COORDINATOR_NAME, A4W_COORDINATOR_EMAIL );

    $d = a4w_admin_send_html_email( $to, 'Test Email Revision ' . A4W_ADMIN_VERSION, $content );
    $data[ 'mail' ] = $d;

    return a4w_admin_api_success( $data );
}

function a4w_admin_api_sponsor( $request ) {
    $params = $request->get_params();

    $event_id = $params[ 'eventid' ];
    $order_num = $params[ 'ordernum' ];
    $sponsor_name = $params[ 'sponsname' ];
    $sponsor_email = $params[ 'sponsemail' ];
    $sponsor_amt = $params[ 'sponsamt' ];
    $booking_id = $params[ 'bookingid' ];
    $participant = $params[ 'participant' ];
    $notes = $params[ 'notes' ];

    if (! $booking_id) {
        $booking_id = NULL;
    }
    if (! $participant) {
        $participant = NULL;
    }
    if (! $notes) {
        $notes = NULL;
    }

    a4w_admin_log( LOG_DEBUG );
    $r = a4w_admin_add_sponsorship( $event_id, $order_num, $sponsor_name,
            $sponsor_email, $sponsor_amt, FALSE, $booking_id, $participant,
            $notes, TRUE );

    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }
    
    return a4w_admin_api_success( $r[ 1 ] );
}

function a4w_admin_api_register( $request ) {
    $params = $request->get_params();

    $event_id = $params[ 'eventid' ];
    $entry_id = intval( $params[ 'entryid' ] );
    $participant_name = $params[ 'partname' ];
    $participant_email = $params[ 'partemail' ];
    $participant_phone = $params[ 'partphone' ];
    $participant_extra = $params[ 'extrainfo' ];
    $sponsor_plan = ($params[ 'sponsorplan' ] == 'self' ? 1 : 0);
    $sponsor_amt = $params[ 'sponsamt' ];
    $sponsor_ordernum = $params[ 'sponsordernum' ];

    $notify_coordinator = a4w_admin_api_get_bool_param( $params, 'notify' );
    $send_confirmation = a4w_admin_api_get_bool_param( $params, 'confirm' );
    $waiver_signed = a4w_admin_api_get_bool_param( $params, 'waiver' );
    $self_sponsored = a4w_admin_api_get_bool_param( $params, 'selfsponsored' );
    $duplicate_ok = a4w_admin_api_get_bool_param( $params, 'duplicateok' );

    $ticket_id = a4w_admin_api_get_optional_param( $params, 'ticketid', -1 );
    
    if (! $participant_extra) {
        $participant_extra = NULL;
    }

    $data = [
        'api_revision' => A4W_ADMIN_VERSION
    ];

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }

    $dbh = $r[ 1 ];
    $participant_list = a4w_db_get_event_participants( $dbh, $event_id );
    if (! $duplicate_ok) {
        foreach ($participant_list[ $event_id ] as $p) {
            if ($p[ 'name' ] == $participant_name) {
                $r = a4w_db_get_participant_details( $dbh, $event_id, $p[ 'booking' ] );
                a4w_admin_log( LOG_DEBUG, print_r( $r, TRUE ) );
                
                return a4w_admin_api_fail(
                    a4w_admin_add_duplicate_participant_form(
                        $event_id, $ticket_id, $entry_id, $participant_name, $participant_email,
                        $participant_phone, $participant_extra, $sponsor_plan, $self_sponsored,
                        $sponsor_amt, $sponsor_ordernum, $waiver_signed, $notify_coordinator,
                        $r[ 1 ][ 'email' ]
                    ), 200, [ 'reason' => 'duplicate' ]
                );
            }
        }
    }
    extract( a4w_db_get_event_outline( $dbh, $event_id ) );

    $num_regs = a4w_db_get_num_registrations( $dbh, $event_id );
    $capacity = a4w_db_get_event_spaces( $dbh, $event_id );

    $r = a4w_db_put_new_participant( $dbh, $event_id, $ticket_id,
            $entry_id, $participant_name, $participant_email,
            $sponsor_plan, $participant_phone, $participant_extra,
            $waiver_signed );

    if (! $r[ 0 ]) {
        $msg = 'Failed to save participant registration to database because: ' . "\n" . $r[ 1 ];
        return a4w_admin_api_fail( $msg );
    }
    $booking_id = $r[ 1 ];

    $r = a4w_db_get_ticket_name( $dbh, $ticket_id );
    if (! $r[ 0 ]) {
        $category = NULL;
    } else {
        $category = $r[ 1 ];
    }

    $data[ 'msg' ] = $participant_name . ' has been successfully registered in ' . $adv_name;

    if ($notify_coordinator) {
        $sent = a4w_admin_send_reg_notification_email( $coordinator_name, $coordinator_email,
                    $participant_name, $participant_email, $participant_phone, $category,
                    $participant_extra, ($num_regs + 1), $capacity, $adv_name, $adv_slug );

        if ($sent) {
            $data[ 'msg' ] .= ', notification email sent to coordinator at ' .
                              '<tt>' . $coordinator_email . '</tt>.';
        } else {
            $data[ 'msg' ] .= '. However notification email FAILED to send to coordinator at ' .
                              '<tt>' . $coordinator_email . '</tt>.';
        }
    } else {
        $data[ 'msg' ] .= '. Notification email not sent to coordinator.';
    }
    
    $data[ 'msg' ] .= '<br><br>';
    if ($send_confirmation) {
        a4w_admin_log( LOG_DEBUG, 'Sending confirmation email.' );
        
        $adv_date_sc = '[show-a4w-event-date eventid="' . $event_id . '" formattime="false" nodate=""]';
        $adv_date = do_shortcode( $adv_date_sc );

        $donation_req = a4w_db_get_fundraising_amount( $dbh, $event_id );
        $adv_metrics = a4w_db_get_event_metrics( $dbh, $event_id );
        $req_equipment = a4w_db_get_event_meta( $dbh, $event_id, 'required_eqp', '' );
        $custom_paragraph = a4w_db_get_event_meta( $dbh, $event_id, 'registrant_email_message', '' );
        
        $sent = a4w_admin_send_reg_confirmation_email( $participant_name, $participant_email,
                    $adv_name, $adv_date, $adv_slug, $category, $coordinator_name, $donation_req,
                    $adv_metrics, $req_equipment, $custom_paragraph );
        if ($sent) {
            $data[ 'msg' ] .= 'Confirmation email sent to registrant at ' .
                              '<tt>' . $participant_email . '</tt>.';
        } else {
            $data[ 'msg' ] .= 'Confirmation email FAILED to send to registrant at ' .
                              '<tt>' . $participant_email . '</tt>.';
        }
    } else {
        $data[ 'msg' ] .= 'Confirmation email not sent to participant.';
    }
    
    if ($self_sponsored) {
        a4w_admin_log( LOG_DEBUG, 'Over to self-sponsorship.' );
        $notes = NULL;
        $r = a4w_admin_add_sponsorship( $event_id, $sponsor_ordernum,
                $participant_name, $participant_email, $sponsor_amt,
                FALSE, $booking_id, $participant_name, $notes, TRUE );

        $data[ 'msg' ] .= '<br><br>' . ($r[ 0 ] ? $r[ 1 ][ 'msg' ] : $r[ 1 ]);
    }

    return a4w_admin_api_success( $data );
}

function a4w_admin_api_unregister( $request ) {
    $params = $request->get_params();

    $event_id = $params[ 'eventid' ];
    $booking_id = $params[ 'bookingid' ];

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }

    $dbh = $r[ 1 ];

    $r = a4w_db_remove_participant( $dbh, $event_id, $booking_id );
    if (! $r[ 0 ]) {
        $msg = 'Failed to remove participant registration because: ' . "\n" . $r[ 1 ];
        return a4w_admin_api_fail( $msg );
    }

    $participant_name = print_r($r[ 1 ], true);
    $adv_name = a4w_db_get_event_name( $dbh, $event_id );

    $data = [
        'api_revision' => A4W_ADMIN_VERSION,
        'msg'          => $participant_name . ' has been unregistered from ' . $adv_name .
                          '. No email has been sent to coordinator (you need to do this yourself).'
    ];

    return a4w_admin_api_success( $data );
}

function a4w_admin_api_get_personal_page_link( $request ) {
    $params = $request->get_params();

    a4w_admin_log( LOG_DEBUG, print_r( $params, TRUE ) );

    $booking_id = $params[ 'bookingid' ];
    $r = a4w_admin_get_personal_page_link( $booking_id );
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }
    
    $data = [
        'api_revision' => A4W_ADMIN_VERSION,
        'link'         => $r[ 1 ]
    ];

    a4w_admin_log( LOG_DEBUG, print_r( $data, TRUE ) );

    return a4w_admin_api_success( $data );
}

function a4w_admin_api_assign_sponsor( $request ) {
    $params = $request->get_params();
    
    $sponsor_id = $params[ 'sponsorid' ];
    $booking_id = $params[ 'bookingid' ];
    
    $to_general = a4w_admin_api_get_bool_param( $params, 'togeneral' );
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }

    $dbh = $r[ 1 ];
    
    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sponsor_id', 'booking_id', 'to_general' ), TRUE ) );
    $r = a4w_db_assign_sponsor( $dbh, $sponsor_id, $booking_id, $to_general );
    if (! $r[ 0 ]) {
        $target = ($to_general ? 'adventure' : 'participant');
        $msg = 'Failed to assign sponsor to ' . $target . ' because: ' . "\n" . $r[ 1 ];
        return a4w_admin_api_fail( $msg );
    }
    
    $event_id = $r[ 1 ];
    a4w_admin_log( LOG_DEBUG, $event_id );
    if ($to_general) {
        $target = a4w_db_get_event_name( $dbh, $event_id ) . ' (general)';
    } else {
        $target = a4w_db_get_participant_name( $dbh, $event_id, $booking_id )[ 1 ];
    }

    $sponsor_name = a4w_db_get_sponsor_name( $dbh, $sponsor_id )[ 1 ];
    
    a4w_admin_log( LOG_DEBUG );
    $data = [
        'api_revision' => A4W_ADMIN_VERSION,
        'msg'          => 'Sponsorship from ' . $sponsor_name . ' has been assigned to ' . $target . '.'
    ];
    
    return a4w_admin_api_success( $data );
}

function a4w_admin_api_split_sponsor( $request ) {
    $params = $request->get_params();
    
    $sponsor_id = $params[ 'sponsorid' ];
    $amount = intval( $params[ 'amount' ] ) * 100;
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }
    
    $dbh = $r[ 1 ];
    
    a4w_admin_log( LOG_DEBUG, print_r( compact( 'sponsor_id', 'amount' ), TRUE ) );
    $r = a4w_db_split_sponsor( $dbh, $sponsor_id, $amount );
    if (! $r[ 0 ]) {
        $msg = 'Failed to split sponsorship because: ' . "\n" . $r[ 1 ];
        return a4w_admin_api_fail( $msg );
    }
    
    $data = [
        'api_revision' => A4W_ADMIN_VERSION,
        'msg'          => 'Sponsorship ' . $sponsor_id . ' from ' . $r[ 1 ][ 0 ] . ' of $' .
                          number_format( $r[ 1 ][ 1 ] / 100 ) . ' has been split with amounts $' .
                          number_format( $r[ 1 ][ 2 ] / 100 ) . ' and $' .
                          number_format( $r[ 1 ][ 3 ] / 100 ) . '.'
    ];
    
    return a4w_admin_api_success( $data );
}

// This is for calls made from the AWA site when a sponsorship has been made through WooCommerce.
// The AWA website has a hook that will call this API endpoint to transfer the data over, which
//  will do security verification, parse out the data from the serialized WooCommerce Order
//  object, and then (locally) call the "sponsor" routine.
// This endpoint takes 3 mandatory arguments:
//  - 'order'     : the serialized (i.e.: JSONified) Order object
//  - 'timestamp' : self explanatory - UNIX timestamp; transfers have a lifetime of 30 seconds
//                  after which the transfer will be rejected
//  - 'token'     : this is a bad name for it. It's not actually a token; rather it's just a
//                  SHA512 hash of the order + timestamp + a salt pre-agreed on by both websites
//                  (defined at the top of this file).
function a4w_admin_api_transfer_shop_order( $request ) {
    $params = $request->get_params();

    $order = urldecode( $params[ 'order' ] );
    $timestamp = $params[ 'timestamp' ];
    $token = $params[ 'token' ];
    
    $hash = hash( 'sha512', $order . $timestamp . A4W_WEBSITE_SALT );

    $msg = '';
    if ($hash != $token) {
        return a4w_admin_api_fail( 'Token verification fail: ' . $order . '.' );
    }
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r;
    }
    $dbh = $r[ 1 ];

    $order = json_decode( $order, TRUE );
    
    $records = [];
    
    $order_num = $order[ 'number' ];
    $sponsor_name = $order[ 'sponsor' ];
    $sponsor_email = $order[ 'email' ];
    foreach ($order[ 'targets' ] as $target) {
        $event_id = a4w_db_get_event_id_from_slug( $dbh, $target[ 'adventure' ] );
        $sponsor_amt = $target[ 'amount' ];
        $booking_id = NULL;
        $notes = NULL;
        $participant = NULL;
        $assigned = FALSE;
        if ($target[ 'notes' ]) {
            $notes = $target[ 'notes' ];
        }
        $anonymous = $target[ 'anonymous' ];

        if ($notes) {
            // Don't include sponsor name; if it fails to match
            //  programatically, there's a good chance the notes will require
            //  human interpretation to correctly determine the sponsor.
            $match_names = [$notes];
        } else {
            // On the other hand, if there aren't any notes then this person
            //  is likely trying to sponsor themselves, assuming they are
            //  registered.
            $match_names = [$sponsor_name];
        }
        
        $match = a4w_admin_match_participant( $dbh, $match_names, $event_id );
        a4w_admin_log( LOG_INFO, 'Matches are: ' . print_r( $match ) );
        if ($match) {
            $booking_id = $match[ 'booking' ];
            $participant = $match[ 'name' ];
            if ($match[ 'index' ] == 0) {
                $notes = NULL;
            }
            $assigned = TRUE;
        }
        
        $r = a4w_admin_add_sponsorship( $event_id, $order_num, $sponsor_name, $sponsor_email,
                                        $sponsor_amt, $anonymous, $booking_id, $participant,
                                        $notes, $assigned );
        $response = $r[ 1 ];

        $records[] = compact(
            'event_id', 'order_num', 'sponsor_name', 'sponsor_email', 'sponsor_amt',
            'booking_id', 'participant', 'notes', 'assigned', 'response'
        );
    }

    $filename = 'transfer_temp/' . $timestamp . '.txt';
    $data = print_r( compact( 'order', 'timestamp', 'records' ), TRUE );
    a4w_admin_log( LOG_INFO, $data, $filename );
    
    $msg = '';
    
    return a4w_admin_api_success( ['msg' => $msg] );
}

function a4w_admin_api_view_fundraising( $request ) {
    $params = $request->get_params();

    $event_id = $params[ 'eventid' ];
    $secret_code = $params[ 'secretcode' ];

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }

    $dbh = $r[ 1 ];

    if (! a4w_db_is_valid_code( $dbh, $event_id, $secret_code )) {
        $msg = 'Incorrect secret code provided. Adventure coordinators can contact ' .
               'the A4W Program Coordinator at ' . a4w_admin_generate_email_link_tag( A4W_COORDINATOR_EMAIL ) . ' ' .
               'if they do not remember their code.';
        return a4w_admin_api_fail( $msg );
    }

    $name = a4w_db_get_event_name( $dbh, $event_id );
    $coordinator = a4w_db_get_coordinator_name( $dbh, $event_id );
    $capacity = a4w_db_get_event_spaces( $dbh, $event_id );
    $event_parts = a4w_db_get_event_participants( $dbh, $event_id );
    $parts = $event_parts[ $event_id ];
    $sponsors = a4w_db_get_event_sponsorships( $dbh, $event_id );
    $total_raised = a4w_db_get_event_amount_raised( $dbh, $event_id );
    $goal = a4w_db_get_event_goal( $dbh, $event_id );

    $data = [
        'api_revision'     => A4W_ADMIN_VERSION,
        'event_name'       => $name,
        'coordinator_name' => $coordinator
    ];

    if (array_key_exists( $coordinator, $sponsors )) {
        $data[ 'coord' ] = $sponsors[ $coordinator ];
        unset( $sponsors[ $coordinator ] );
    } else {
        $data[ 'coord' ] = [];
    }
    if ($capacity > 0) {
        $data[ 'parts' ] = array_fill( 0, $capacity, [] );
        $i = 0;
        foreach ($parts as $participant) {
            $partname = $participant[ 'name' ];
            if (array_key_exists( $partname, $sponsors )) {
                $data[ 'parts' ][ $i ] = [$partname, $sponsors[ $partname ]];
                unset( $sponsors[ $partname ] );
            } else {
                $data[ 'parts' ][ $i ] = [$partname, []];
            }
            // Add unassigned sponsors as well
            if (array_key_exists( '', $sponsors )) {
                for ($j = count( $sponsors[ '' ] ) - 1; $j >= 0; $j--) {
                    if ($sponsors[ '' ][ $j ][ 0 ] == $partname) {
                        $data[ 'parts' ][ $i ][ 1 ][] = $sponsors[ '' ][ $j ];
                        unset( $sponsors[ '' ][ $j ] );
                    }
                }
            }
            $i++;
        }
    } else {
        $data[ 'parts' ] = [];
    }
    if (array_key_exists( '', $sponsors )) {
        $data[ 'general' ] = $sponsors[ '' ];
        unset( $sponsors[ '' ] );
    } else {
        $data[ 'general' ] = [];
    }
    $data[ 'pending' ] = $sponsors;

    $data[ 'total' ] = $total_raised;
    $data[ 'goal' ] = $goal;

    return a4w_admin_api_success( $data );
}

function a4w_admin_api_get_invite_adv( $request ) {
    $params = $request->get_params();

    $invite_code = $params[ 'invitecode' ];

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }

    $dbh = $r[ 1 ];

    $r = a4w_admin_get_adventure_from_invite_code( $dbh, $invite_code );
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }

    $data = [
        'api_revision'   => A4W_ADMIN_VERSION,
        'adventure_name' => $r[ 1 ]
    ];

    return a4w_admin_api_success( $data );
}

function a4w_admin_api_download_participants( $request ) {
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return a4w_admin_api_fail( $r[ 1 ] );
    }
    $dbh = $r[ 1 ];

    $params = $request->get_params();

    $event_id = $params[ 'eventid' ];
    if ($event_id == '*') {
        $advname = 'all_adventures';
    } else {
        $advname = a4w_db_get_event_slug( $dbh, $event_id );
        if (preg_match( '/^a4w20[0-9]{2}[_-]/', $advname )) {
            $advname = substr( $advname, 8 );
        }
    }

    $r = a4w_db_get_event_participant_details( $dbh, $event_id );
    if (! $r[ 0 ]) {
        $msg = 'Failed to retrieve participant list because:' . "\n" . $r[ 1 ];
        return a4w_admin_api_fail( $msg );
    }
    $participant_data = $r[ 1 ];

    $datetime = current_time( 'Y-m-d @ H.i' );
    $filename = current_time( 'Ymd' ) . '_a4w_participants_' . $advname;
    $data = [
        'datetime' => $datetime,
        'filename' => $filename,
        'eventid'  => $event_id,
        'data'     => $participant_data
    ];

    a4w_admin_log( LOG_DEBUG, print_r( $participant_data, TRUE ) );

    return a4w_admin_api_success( $data );
}

function a4w_admin_api_download_donations( $request ) {
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        $msg = $r[ 1 ];
        return a4w_admin_api_fail( $msg, $http_status = 500 );
    }
    $dbh = $r[ 1 ];

    $params = $request->get_params();

    $event_id = $params[ 'eventid' ];
    if ($event_id == '*') {
        $advname = 'all_adventures';
    } else {
        $advname = a4w_db_get_event_slug( $dbh, $event_id );
        if (preg_match( '/^a4w20[0-9]{2}[_-]/', $advname )) {
            $advname = substr( $advname, 8 );
        }
    }

    $r = a4w_db_get_event_sponsorship_details( $dbh, $event_id );
    if (! $r[ 0 ]) {
        $msg = "No sponsor data.";
        return a4w_admin_api_fail( $msg, $http_status = 400 );
    }
    $sponsor_data = $r[ 1 ];
    
    $datetime = current_time( 'Y-m-d @ H.i' );
    $filename = current_time( 'Ymd' ) . '_a4w_sponsors_' . $advname;
    $data = [
        'datetime' => $datetime,
        'filename' => $filename,
        'eventid'  => $event_id,
        'data'     => $sponsor_data
    ];
    
    a4w_admin_log( LOG_DEBUG, print_r( $sponsor_data, TRUE ) );
    
    return a4w_admin_api_success( $data );
}

function a4w_admin_api_get_website_stats( $request ) {
    $params = $request->get_params();
    
    $year = intval( a4w_admin_api_get_optional_param( $params, 'year', current_time( 'Y' ) ) );
    
    $stats = a4w_admin_get_website_stats( $year );
    if (! $stats[ 'ok' ]) {
        return a4w_admin_api_fail( $stats[ 'msg' ] );
    }
    
    return a4w_admin_api_success( $stats );
}

function a4w_admin_api_success( $data = [] ) {
    $data[ 'status' ] = 'ok';

    $response = new WP_REST_Response( $data );
    $response->set_status( 200 );

    return $response;
}

function a4w_admin_api_fail( $msg, $http_status = 200, $extra_fields = NULL ) {
    $data = [
        'status' => 'fail',
        'msg'    => $msg
    ];
    if (! is_null( $extra_fields )) {
        $data = array_merge( $data, $extra_fields );
    }
    $response = new WP_REST_Response( $data );
    $response->set_status( $http_status );

    return $response;
}

function a4w_admin_get_website_stats( $year = NULL ) {
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return ['ok' => FALSE, 'msg' => $r[ 1 ]];
    }

    $dbh = $r[ 1 ];
    
    $stats = ['ok' => TRUE];
    
    $events_table = a4w_db_get_table_name( 'em_events' );
    $bookings_table = a4w_db_get_table_name( 'em_bookings' );
    $participants_table = a4w_db_get_table_name( 'a4w_participants' );
    $sponsors_table = a4w_db_get_table_name( 'a4w_sponsors' );

    $yearspec = 'a4w' . $year . '%';

    $sql = 'SELECT    event_name, coalesce(num_participants,0) ' .
           'FROM      ' . $events_table . ' AS e ' . 
           'LEFT JOIN (' .
           '    SELECT    event_id, count(*) AS num_participants ' .
           '    FROM      ' . $bookings_table . ' AS b ' .
           '    LEFT JOIN ' . $participants_table . ' AS p ' .
           '    ON        b.booking_id = p.booking_id ' .
           '    GROUP BY  event_id' .
           ') AS bp ' . 
           'ON        e.event_id = bp.event_id ' .
           'WHERE     NOT event_private AND ' .
           '          event_slug LIKE ? ' .
           'ORDER BY  event_name;';
    $params = [$yearspec];

    $r = a4w_db_get_results( $dbh, $sql, $params );
    $stats[ 'events' ] = ($r[ 0 ] ? $r[ 1 ] : []);

    $sql = 'SELECT    count(*), sum(amount)/100 ' .
           'FROM      ' . $sponsors_table . ' AS s ' .
           'LEFT JOIN ' . $events_table . ' AS e ' .
           'ON        s.event_id = e.event_id ' .
           'WHERE     event_slug LIKE ? ;';
    $r = a4w_db_get_results( $dbh, $sql, $params );
    
    $stats[ 'num_sponsors' ] = ($r[ 0 ] ? $r[ 1 ][ 0 ][ 0 ] : 0);
    $stats[ 'total_sponsored' ] = ($r[ 0 ] ? $r[ 1 ][ 0 ][ 1 ] : 0);
    
    return $stats;
}

function a4w_admin_add_sponsorship( $event_id, $order_num, $sponsor_name,
                                    $sponsor_email, $sponsor_amt, $anonymous,
                                    $booking_id, $participant, $notes,
                                    $assigned = TRUE ) {

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r;
    }

    $dbh = $r[ 1 ];
    extract( a4w_db_get_event_outline( $dbh, $event_id ) );

    $total_raised = a4w_db_get_event_amount_raised( $dbh, $event_id );
    $adv_goal = a4w_db_get_event_goal( $dbh, $event_id );

    $data = [
        'api_revision' => A4W_ADMIN_VERSION
    ];

    if (! is_null( $booking_id )) {
        $r = a4w_db_get_participant_name( $dbh, $event_id, $booking_id );
        if ($r[ 0 ]) {
            $participant = $r[ 1 ];
        } else {
            $booking_id = NULL;
            $participant = NULL;
        }
    }

    a4w_admin_log( LOG_DEBUG );

    $r = a4w_db_put_new_sponsor( $dbh, $event_id, $sponsor_name, $sponsor_email,
                $sponsor_amt, $booking_id, $participant, $notes, $anonymous,
                $order_num, $assigned );
    if (! $r[ 0 ]) {
        $msg = 'Failed to save sponsorship info to database because: ' . "\n" . $r[ 1 ];
        return [FALSE, $msg];
    }

    a4w_admin_log( LOG_DEBUG );
    $sent = a4w_admin_send_sponsor_notification_email( $coordinator_name, $coordinator_email,
                $sponsor_name, $sponsor_amt, $sponsor_email, $participant, $notes, $adv_name,
                ($total_raised + $sponsor_amt), $adv_goal, $adv_slug );

    if ($sent) {
        $data[ 'msg' ] = 'Sponsorship of $' . number_format( $sponsor_amt, 2 ) . ' from ' . $sponsor_name .
                         ' has been successfully applied toward ' . $adv_name . ', and email sent to ' .
                         'coordinator at <tt>' . $coordinator_email . '</tt>.';
    } else {
        $data[ 'msg' ] = 'Sponsorship of $' . number_format( $sponsor_amt, 2 ) . ' from ' . $sponsor_name .
                         ' has been successfully applied toward ' . $adv_name . '. However email FAILED ' .
                         'to send to coordinator at <tt>' . $coordinator_email . '</tt>.';
    }

    return [TRUE, $data];
}

function a4w_admin_normalize_string( $s ) {
    return preg_replace( '/[^ a-z]/', '', preg_replace( '/\\s+/', ' ', strtolower( trim( $s ) ) ) );
}

function a4w_admin_match_participant( $dbh, $matches, $event_id ) {
    foreach ($matches as $i => $match) {
        $normalized = a4w_admin_normalize_string( $match );
        if (! $normalized) {
            // This is a dummy value that should never be returned by the
            //  normalizer. It's here to ensure we don't accidentally match
            //  when both sides are (or reduce to) blank.
            $normalized = '!';
        }
        a4w_admin_log( LOG_INFO, 'Match [' . $matches[ $i ] . '] normalized to [' . $normalized . '].' );
        $matches[ $i ] = $normalized;
    }
    
    foreach (a4w_db_get_event_participants( $dbh, $event_id ) as $participant) {
        foreach ($matches as $i => $match) {
            if ($match == a4w_admin_normalize_string( $participant[ 'name' ] )) {
                $msg = 'Match on ' . $i . ' found: [' . $match . '] with [' . $participant[ 'name' ] . '].';
                a4w_admin_log( LOG_INFO, $msg );
                return [
                    'booking' => $participant[ 'booking' ],
                    'name'    => $participant[ 'name' ],
                    'index'   => $i
                ];
            }
        }
    }
    
    return FALSE;
}

function a4w_admin_get_adventure_from_invite_code( $dbh, $invite_code ) {
    $r = a4w_db_get_invite_form_data( $dbh );
    if (! $r[ 0 ]) {
        return [FALSE, 'Database error on server.'];
    }

    $formdata = json_decode( $r[ 1 ] );
    foreach ($formdata->fields as $field) {
        if ($field->id == 2) {
            foreach ($field->choices as $choice) {
                if ($choice->value == $invite_code) {
                    $adventure = $choice->text;

                    $d = strpos( $adventure, ' with ' );
                    if ($d !== FALSE) {
                        $adventure = substr( $adventure, 0, $d ) . ' <i>' . substr( $adventure, $d + 1 ) . '</i>';
                    }
                    return [TRUE, $adventure];
                }
            }
        }
    }

    return [FALSE, 'Invalid invite code supplied or adventure not found.'];
}

function a4w_admin_get_personal_page_link( $booking_id ) {
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r;
    }

    $dbh = $r[ 1 ];

    $r = a4w_db_get_personal_page_params( $dbh, $booking_id );
    if (! $r[ 0 ]) {
        return $r;
    }
    
    a4w_admin_log( LOG_DEBUG, print_r( $r, TRUE ) );
    
    $page_url = A4W_SITE_URL . 'sponsor/' . $r[ 1 ][ 'token' ] . $booking_id . '/' . $r[ 1 ][ 'name' ];
    
    return [TRUE, $page_url];
}

function a4w_admin_get_fundraising_request_amounts( $event_id = NULL ) {
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return [];
    }

    $dbh = $r[ 1 ];
    
    return a4w_db_get_fundraising_amount( $dbh, $event_id );
}

function a4w_admin_generate_email_link_tag( $address ) {
    return '<a href="mailto:' . $address . '">' . $address . '</a>';
}

function a4w_admin_generate_img_tag( $imgdata ) {
    return '<img src="cid:' . $imgdata[ 'cid' ] . '" height="' . $imgdata[ 'height' ] . '" width="' . $imgdata[ 'width' ] . '">';
}

function a4w_admin_simple_html_to_plaintext( $str ) {
    $crlf = "\r\n";

    $str = str_replace( '<br>', $crlf . $crlf, $str );
    $str = str_replace( '<li>', '    * ', $str );
    $str = str_replace( '</li>', $crlf, $str );

    return strip_tags( $str );
}

function a4w_admin_make_qualified_email_address( $name, $address ) {
    if (strpbrk( $name, '"\'’' ) !== FALSE) {
        $name = '"' . str_replace( '"', '\\"', str_replace( '’', '\'', $name ) ) . '"';
    }
    
    return $name . ' <' . $address . '>';
}

function a4w_admin_get_users_name( $first = TRUE, $last = TRUE ) {
    $cur_user = wp_get_current_user();
    
    if (! $cur_user) {
        return '';
    }
    
    $name = '';
    if ($first) {
        $name .= $cur_user->first_name . ' ';
    }
    if ($last) {
        $name .= ' ' . $cur_user->last_name;
    }
    
    return trim( $name );
}

function a4w_admin_send_reg_notification_email( $coord_name, $coord_email, $participant_name,
                                                $participant_email, $participant_phone, $category,
                                                $extra_info, $num_participants, $capacity,
                                                $adv_name, $slug ) {

    $crlf = "\r\n";
    $nl = "\n";

    $c_names = explode( ' ', trim( $coord_name ) );
    $c_firstname = $c_names[ 0 ];
    $p_names = explode( ' ', trim( $participant_name ) );
    $p_firstname = $p_names[ 0 ];
    $adv_url = A4W_ADVENTURES_URL . $slug;
    
    $admin_name = a4w_admin_get_users_name();

    $subj = 'Your Adventure for Wilderness has a new participant';
    $content = [
        'plain' =>
            'Dear ' . $c_firstname . ',' . $crlf . $crlf .
            'Your "' . $adv_name . '" Adventure for Wilderness has a new participant!' . $crlf . $crlf .
            $participant_name . ' has just signed up to to join you. Their contact' . $crlf .
            '  details are below if you wish to get in touch.' . $crlf . $crlf .
            'Once they start raising funds and making sponsorships, those notifications' . $crlf .
            '  will arrive as separate emails. Be sure to let ' . $p_firstname . ' know of any sponsorship' . $crlf .
            '  expectations you have for participants on your adventure!' . $crlf . $crlf .
            'This brings the total number of participants registered on your adventure to' . $crlf .
            '  ' . $num_participants . ', out of a stated capacity of ' . $capacity . '.' . $crlf . $crlf .
            'Participant details:' . $crlf .
            '    * Participant name: ' . $participant_name . $crlf .
            '    * Participant email: ' . $participant_email . $crlf .
            '    * Participant phone: ' . $participant_phone . $crlf .
            (isset( $category ) ? '    * Category: ' . $category . $crlf : '') . $crlf .
            (isset( $extra_info ) ? '    * Other participant info: ' . $extra_info . $crlf : '') . $crlf .
            'Remember that at any time, you can see how your adventure is doing, and check out the' . $crlf .
            '  leaderboards on your page at:' . $crlf . $crlf .
            '     ' . $adv_url . '.' . $crlf . $crlf .
            'That’s also a great page to send potential sponsors to so they can see what your adventure' . $crlf .
            '  is all about, and easily make a donation.' . $crlf . $crlf .
            'Thank you for helping to raise funds to protect Alberta’s wildlands, wildlife and wild' . $crlf .
            '  waters!' . $crlf . $crlf .
            $admin_name . $crlf .
            'For the Adventures For Wilderness Team' . $crlf .
            A4W_COORDINATOR_EMAIL . $crlf .
            '(403) 283-2025',
        'html' =>
            '<h2>Your adventure is filling up!</h2>' . $nl .
            '<p>Dear ' . $c_firstname . ',</p>' . $nl .
            '<p>Your <b>' . $adv_name . '</b> <i>Adventure for Wilderness</i> has a new participant!</p>' . $nl .
            '<p><b>' . $participant_name . '</b> has just signed up to to join you. Their contact ' . $nl .
            '   details are below if you wish to get in touch.</p>' . $nl .
            '<p>Once they start raising funds and making sponsorships, those notifications ' . $nl .
            '   will arrive as separate emails. Be sure to let ' . $p_firstname . ' know of any ' . $nl .
            '   sponsorship expectations you have for participants on your adventure!</p>' . $nl .
            '<p>This brings the total number of participants registered on your adventure to' . $nl .
            '   ' . $num_participants . ', out of a stated capacity of ' . $capacity . '.</p>' . $nl .
            '<p>Participant details:' . $nl .
            '   <ul>' . $nl .
            '        <li>Participant name: ' . $participant_name . '</li>' . $nl .
            '        <li>Participant email: <a href="mailto:' . $participant_email . '">' . $participant_email . '</a></li>' . $nl .
            '        <li>Participant phone: ' . $participant_phone . '</li>' . $nl .
            (isset( $category ) ? '        <li>Category: ' . $category . '</li>' . $nl : '') .
            (isset( $extra_info ) ? '        <li>Other participant info: ' . $extra_info . '</li>' . $nl : '') .
            '   </ul></p>' . $nl .
            '<p>Remember that at any time, you can see how your adventure is doing, and check out the' . $nl .
            '   leaderboards on your page at:' . $nl .
            '   <div style="margin:20px;"><a href="' . $adv_url . '">' . $adv_url . '</a></div></p>' . $nl .
            '<p>That’s also a great page to send potential sponsors to so they can see what your adventure' . $nl .
            '   is all about, and easily make a donation.</p>' . $nl .
            '<p>Thank you for helping to raise funds to protect Alberta’s wildlands, wildlife and wild' . $nl .
            '   waters!' . $nl .
            '<p><b>' . $admin_name . '</b><br>' . $nl .
            '   For the <i>Adventures For Wilderness</i> Team<br>' . $nl .
            '   ' . a4w_admin_generate_email_link_tag( A4W_COORDINATOR_EMAIL ) . '<br>' . $nl .
            '   (403) 283-2025</p>'
    ];

    $to = a4w_admin_make_qualified_email_address( $coord_name, $coord_email );

    return a4w_admin_send_html_email( $to, $subj, $content );
}

function a4w_admin_send_sponsor_notification_email( $coord_name, $coord_email, $sponsor_name, $sponsor_amt, $sponsor_email,
                                                    $participant, $notes, $adv_name, $total_amt, $adv_goal, $slug ) {

    $crlf = "\r\n";
    $nl = "\n";

    $names = explode( ' ', trim( $coord_name ) );
    $firstname = $names[ 0 ];
    $amt_fmt = '$' . number_format( $sponsor_amt, 2 );
    $total_fmt = '$' . number_format( $total_amt, 2 );
    $goal_fmt = '$' . number_format( $adv_goal, 0 );
    $pct = round( $total_amt * 100 / $adv_goal ) . '%';
    $adv_url = A4W_ADVENTURES_URL . $slug;

    $has_participant = ! (is_null( $participant ) || trim( $participant ) == '');
    $has_notes       = ! (is_null( $notes )       || trim( $notes )       == '');

    $admin_name = a4w_admin_get_users_name();

    $subj = 'Your Adventure for Wilderness has just received a sponsorship';
    $content = [
        'plain' =>
            'Dear ' . $firstname . ',' . $crlf . $crlf .
            'Thank you for your ongoing efforts to coordinate the "' . $adv_name . '" Adventure' . $crlf .
            '  for Wilderness and raise sponsorship money.' . $crlf . $crlf .
            'This is a courtesy email to let you know that your adventure was just sponsored by' . $crlf .
            '  ' . $sponsor_name . ' for ' . $amt_fmt . '. This brings the total amount your adventure' . $crlf .
            '  has raised to ' . $total_fmt . '; which is ' . $pct . ' of your adventure’s stated goal' . $crlf .
            '  of ' . $goal_fmt . '.' . $crlf . $crlf .
            'Sponsorship details:' . $crlf .
            '    * Sponsor name: ' . $sponsor_name . $crlf .
            '    * Sponsor email: ' . $sponsor_email . $crlf .
            ($has_participant ? '    * Participant sponsored: ' . $participant . $crlf : '') .
            ($has_notes       ? '    * Note to team: "'         . $notes . '"' . $crlf : '') . $crlf .
            'Remember that at any time, you can see how your adventure is doing, and check out the' . $crlf .
            '  leaderboards on your page at:' . $crlf . $crlf .
            '     ' . $adv_url . '.' . $crlf . $crlf .
            'That’s also a great page to send potential sponsors to so they can see what your adventure' . $crlf .
            '  is all about, and easily make a donation.' . $crlf . $crlf .
            'Thank you again for helping to raise funds to protect Alberta’s wildlands, wildlife and wild' . $crlf .
            '  waters!' . $crlf . $crlf .
            $admin_name . $crlf .
            'For the Adventures For Wilderness Team' . $crlf .
            A4W_COORDINATOR_EMAIL . $crlf .
            '(403) 283-2025',
        'html' =>
            '<h2>Thanks for supporting Adventures for Wilderness!</h2>' . $nl .
            '<p>Dear ' . $firstname . ',</p>' . $nl .
            '<p>Thank you for your ongoing efforts to coordinate the <b>' . $adv_name . '</b> <i>Adventure ' . $nl .
            '   for Wilderness</i> and raise sponsorship money.</p>' . $nl .
            '<p>This is a courtesy email to let you know that your adventure was just sponsored by' . $nl .
            '   <b>' . $sponsor_name . '</b> for <b>' . $amt_fmt . '</b>. This brings the total amount your' . $nl .
            '   adventure has raised to ' . $total_fmt . '; which is ' . $pct . ' of your adventure’s stated' . $nl .
            '   goal of ' . $goal_fmt . '.</p>' . $nl .
            '<p>Sponsorship details:' . $nl .
            '   <ul>' . $nl .
            '        <li>Sponsor name: ' . $sponsor_name . '</li>' . $nl .
            '        <li>Sponsor email: <a href="mailto:' . $sponsor_email . '">' . $sponsor_email . '</a></li>' . $nl .
            ($has_participant ? '       <li>Participant sponsored: ' . $participant . '</li>' . $nl : '') .
            ($has_notes       ? '       <li>Note to team: "<em>' . $notes . '</em>"' . $nl          : '') .
            '   </ul></p>' . $nl .
            '<p>Remember that at any time, you can see how your adventure is doing, and check out the' . $nl .
            '   leaderboards on your page at:' . $nl .
            '   <div style="margin:20px;"><a href="' . $adv_url . '">' . $adv_url . '</a></div></p>' . $nl .
            '<p>That’s also a great page to send potential sponsors to so they can see what your adventure' . $nl .
            '   is all about, and easily make a donation.</p>' . $nl .
            '<p>Thank you again for helping to raise funds to protect Alberta’s wildlands, wildlife and wild' . $nl .
            '   waters!' . $nl .
            '<p><b>' . $admin_name . '</b><br>' . $nl .
            '   For the <i>Adventures For Wilderness</i> Team<br>' . $nl .
            '   ' . a4w_admin_generate_email_link_tag( A4W_COORDINATOR_EMAIL ) . '<br>' . $nl .
            '   (403) 283-2025</p>'
    ];

    $to = a4w_admin_make_qualified_email_address( $coord_name, $coord_email );

    return a4w_admin_send_html_email( $to, $subj, $content );
}

function a4w_admin_send_reg_confirmation_email( $participant_name, $participant_email, $adv_name, $adv_dates,
                                                $slug, $category, $coord_name, $donation_req, $adv_metrics,
                                                $req_equipment, $custom_paragraph = '' ) {

    $crlf = "\r\n";
    $nl = "\n";
    
    $p_names = explode( ' ', trim( $participant_name ) );
    $p_firstname = $p_names[ 0 ];
    $c_names = explode( ' ', trim( $coord_name ) );
    $c_firstname = $c_names[ 0 ];

    $adv_shortname = $adv_name;
    if (strpos( $adv_name, ' with ' ) !== FALSE) {
        $nameparts = explode( ' with ', $adv_name );
        if (strpos( array_pop( $nameparts ), $c_firstname ) !== FALSE) {
            $adv_shortname = implode( ' with ', $nameparts );
        }
    }
    
    $date_parts = explode( '<br>', $adv_dates );
    $adv_shortdate = $date_parts[ 0 ];
    $adv_fulldate = (count( $date_parts ) == 1 ? $adv_shortdate : implode( ' at ', $date_parts ));

    $don_url = A4W_SITE_URL . 'donate?adventure=' . $slug . '&message=' . urlencode( $participant_name );
    
    $make_donation_link = function( $format ) {
        if ($format == 'html') {
            $link_text = 'by clicking on <a href="' . $don_url . '">this link</a>';
        } else {
            $link_text = 'found at ' . $don_url;
        }
        
        return $link_text;
    };
    
    $empty_text = ['plain' => '', 'html' => ''];
    
    if ($donation_req[ 'minimum' ] > 0) {
        $don_text = 'by donation, with a required minimum of $' . number_format( $donation_req[ 'minimum' ], 2 ) . '. ';
    } elseif ($donation_req[ 'suggestion' ] > 0) {
        $don_text = 'by donation. You are welcome to make a donation of any amount; the coordinator has ' .
                    'suggested $' . number_format( $donation_req[ 'suggestion' ], 2 ) . '. ';
    } else {
        $don_text = 'free. Any donation you wish to make in support of AWA and this adventure is greatly ' .
                    'appreciated and will be applied to our work defending Alberta’s wilderness, wild ' .
                    'waters and wildlife. ';
    }
    
    if ($category) {
        $category_text = [
            'plain' =>
                'Your Registration Category:' . $crlf . $crlf .
                '    * ' . $category . $crlf . $crlf,
            'html' =>
                'Your Registration Category:' . $nl .
                '<ul><li>' . $category . '</li></ul>' . $nl
        ];
    } else {
        $category_text = $empty_text;
    }
    
    if ($adv_metrics) {
        $plain_met_list = '';
        $html_met_list = '';
        $labelsz = max( array_map( 'strlen', array_keys( $adv_metrics ) ) );
        foreach ($adv_metrics as $label => $value) {
            $indent = str_repeat( ' ', $labelsz - strlen( $label ) );
            $plain_met_list .= '    ' . $label . ': ' . $indent . $value . $crlf;
            $html_met_list  .= '<dd>' . $label . '</dd><dt>' . $value . '</dt>' . $nl;
        }
        $metrics_text = [
            'plain' => 'Adventure metrics:' . $crlf . $plain_met_list . $crlf,
            'html'  => 'Adventure metrics:<br>' . $nl . '<dl>' . $nl . $html_met_list . '</dl>' . $nl
        ];
    } else {
        $metrics_text = $empty_text;
    }
    
    if ($req_equipment) {
        $to_bring_text = [
            'plain' =>
                'Things to bring:' . $crlf . $crlf .
                a4w_admin_simple_html_to_plaintext( $req_equipment ) . $crlf . $crlf,
            'html'  =>
                'Things to bring:<br><br>' . $nl .
                '<p style="margin-left: 40px;">' . $req_equipment . '</p>' . $nl
        ];
    } else {
        $to_bring_text = $empty_text;
    }
    
    $extra_adv_info = ($category || $adv_metrics || $req_equipment);

    $admin_name = a4w_admin_get_users_name();
    
    $subj = 'Thank you for registering for ' . $adv_name;
    $content = [
        'plain' =>
            'Dear ' . $p_firstname . ',' . $crlf . $crlf .
            'This is a courtesy email to confirm your registration on the "' . $adv_shortname . '" ' . $crlf .
            'Adventure for Wilderness, with ' . $coord_name . ' on ' . $adv_fulldate . '.' . $crlf . $crlf  .
            'We’ve added you to the participant list, and you will receive a followup email from ' . $crlf .
            $c_firstname . ' sometime in the week prior to the Adventure with information about ' . $crlf .
            'meeting time and location (if applicable), and any other relevant details.' . $crlf . $crlf .
            ($custom_paragraph ? strip_tags( $custom_paragraph ) . $crlf . $crlf : '') . 
            'As a reminder, participation on this adventure is ' . $don_text . 'If you have already ' . $crlf .
            'made a donation, then thank you! Otherwise, you can make a donation at any time, via ' . $crlf .
            'our secure online donation form, ' . $make_donation_link( 'plain' ) . '. You are also ' . $crlf .
            'welcome to raise funds by collecting sponsorships from friends and family - just send ' . $crlf .
            'them that link and any money received will be applied to your registration. All donors ' . $crlf .
            'will receive a charitable tax receipt from AWA for the full amount.' . $crlf .
            ($extra_adv_info ? ($crlf . 'Things To Note About the Adventure:' . $crlf . $crlf) : '') .
            $category_text[ 'plain' ] .
            $metrics_text[ 'plain' ] .
            $to_bring_text[ 'plain' ] .
            $crlf .
            'Thanks for registering! We look forward to seeing you on ' . $adv_shortdate . '. And thank ' . $crlf .
            'you for helping to raise funds to protect Alberta’s wildlands, wildlife and wild ' . $crlf .
            'waters!' . $crlf . $crlf .
            $admin_name . $crlf .
            'For the Adventures For Wilderness Team' . $crlf .
            A4W_COORDINATOR_EMAIL . $crlf .
            '(403) 283-2025',
        'html' =>
            '<h2>Thank you for registering for registering for <i>' . $adv_name . '</i>!</h2>' . $nl .
            '<p>Dear ' . $p_firstname . ',</p>' . $nl .
            '<p>This is a courtesy email to confirm your registration on the' . $nl .
            '   <b>' . $adv_shortname . '</b> <i>Adventure for Wilderness</i>, with ' . $coord_name . $nl .
            '   on <b>' . $adv_fulldate . '</b>.</p>' . $nl .
            '<p>We’ve added you to the participant list, and you will receive a followup email from' . $nl .
            '   ' . $c_firstname . ' sometime in the week prior to the Adventure with information about' . $nl .
            '   meeting time and location (if applicable), and any other relevant details.</p>' . $nl .
            ($custom_paragraph ? '<p>' . $custom_paragraph . '</p>' . $nl : '') .
            '<p>As a reminder, participation on this adventure is ' . $don_text . 'If you have already' . $nl .
            '   made a donation, then thank you! Otherwise, you can make a donation at any time, via' . $nl .
            '   our secure online donation form, ' . $make_donation_link( 'html' ) . '. You are also' . $nl .
            '   welcome to raise funds by collection sponsorships from friends and family &mdash; just' . $nl .
            '   send them that link and any money received wtill be applied to your registration. All' . $nl .
            '   donors will receive a charitable tax receipt from AWA for the full amount.</p>' . $nl .
            ($extra_adv_info ? ('<h3>Things to Note About the Adventure:</h3>' . $nl) : '') .
            $category_text[ 'html' ] .
            $metrics_text[ 'html' ] .
            $to_bring_text[ 'html' ] .
            '<p>Thanks for registering! We look forward to seeing you on ' . $adv_shortdate . '. And' . $nl .
            '   thank you for helping to rainse funds to protect Alberta’s wildlands, wildlife and wild ' . $nl .
            '   waters!</p>' . $nl .
            '<p><b>' . $admin_name . '</b><br>' . $nl .
            '   For the <i>Adventures For Wilderness</i> Team<br>' . $nl .
            '   ' . a4w_admin_generate_email_link_tag( A4W_COORDINATOR_EMAIL ) . '<br>' . $nl .
            '   (403) 283-2025</p>'
    ];

    $to = a4w_admin_make_qualified_email_address( $participant_name, $participant_email );

    return a4w_admin_send_html_email( $to, $subj, $content );
}
                                                
function a4w_admin_generate_html_email_template( $headers, $subj, $content ) {
    $nl = "\n";
    $crlf = "\r\n";

    $boundary = [];
    for ($i = 0; $i < 2; $i++) {
        $boundary[] = uniqid( '----=_NextPart_00' . $i . '_' );
    }

    $curdir = 'https://adventuresforwilderness.ca/wp-content/mu-plugins';
    $cid = [];
    $imagedata = [];
    for ($i = 0; $i < 3; $i++) {
        $cid[] = uniqid();

        $filename = $curdir . '/a4w-admin/a4w_email_image_' . $i . '_b64.txt';
        $imagedata[] = file_get_contents( $filename );
    }

    $images = [
        'a4w_logo' => [
            'cid'    => 'image000.jpg@' . $cid[ 0 ],
            'width'  => '113',
            'height' => '113'
        ],
        'awa_logo' => [
            'cid'    => 'image001.jpg@' . $cid[ 1 ],
            'width'  => '204',
            'height' => '113'
        ],
        '55_ann_banner' => [
            'cid'    => 'image002.jpg@' . $cid[ 2 ],
            'width'  => '750',
            'height' => '173'
        ]

        // TO RE-ENABLE THIS, YOU MUST CHANGE THE EXIT CONDITION IN THE for LOOP ABOVE!
        // 'a4w_logo_old' => [
            // 'cid'    => 'image003.jpg@' . $cid[ 3 ],
            // 'width'  => '200',
            // 'height' => '113'
        // ]
    ];

    $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">' . $nl .
            '<html>' . $nl .
            '<head>' . $nl .
            '    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />' . $nl .
            '    <title>' . $subj . '</title>' . $nl .
            '    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />' . $nl .
            '    <style>' . $nl .
            'body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}' . $nl .
            '.ExternalClass {width:100%;}' . $nl .
            '.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}' . $nl .
            'a:link, h2, b {color:#006f45;}' . $nl .
            'a:visited {color:#488d5c;}' . $nl .
            '[style*=\'Open Sans\'] {' . $nl .
            '    font-family: \'Open Sans\', Arial, sans-serif !important' . $nl .
            '}' . $nl .
            '    </style>' . $nl .
            '</head>' . $nl .
            '<body bgcolor="#d6d8e9" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">' . $nl .
            '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>' . $nl .
            '<td width="100%" valign="top" align="center" bgcolor="#d6d8e9">' . $nl .
            '    <table width="750" border="0" cellpadding="0" cellspacing="0" style="width:750px;border:1px solid #074c6d;">' . $nl .
            '    <tr><td width="375" valign="top" align="left" bgcolor="#ffffff">' . $nl .
            '        ' . a4w_admin_generate_img_tag( $images[ 'a4w_logo' ] ) . $nl .
            '    </td><td width="375" valign="top" align="right" bgcolor="#ffffff">' . $nl .
            '        ' . a4w_admin_generate_img_tag( $images[ 'awa_logo' ] ) . $nl .
            '    </td></tr>' . $nl .
            '    <tr><td width="750" colspan="2" valign="top" align="left" bgcolor="#ffffff" ' . $nl .
            '            style="color:#000000;font-family:Arial,sans-serif,\'Open Sans\';">' . $nl .
            '        <div style="margin:15px;">' . $nl .
            $content[ 'html' ] . $nl .
            '        </div>' . $nl .
            '    </td></tr>' . $nl .
            '    <tr><td width="750" colspan="2" valign="top" align="center" bgcolor="#ffffff">' . $nl .
            '        ' . a4w_admin_generate_img_tag( $images[ '55_ann_banner' ] ) . $nl .
            '    </td></tr></table>' . $nl .
            '</td></tr></table></body>' . $nl;

    array_unshift( $headers, 'MIME-Version: 1.0' );
    array_push( $headers, 'Content-Type: multipart/related; boundary="' . $boundary[ 0 ] . '"' );

    $body = 'This is a multi-part message in MIME format.' . $crlf . $crlf .
            '--' . $boundary[ 0 ] . $crlf .
            'Content-Type: multipart/alternative; boundary="' . $boundary[ 1 ] . '"' . $crlf . $crlf .
            '--' . $boundary[ 1 ] . $crlf .
            'Content-Type: text/plain; charset="us-ascii"' . $crlf .
            'Content-Transfer-Encoding: 7bit' . $crlf . $crlf .
            $content[ 'plain' ] . $crlf . $crlf .
            '--' . $boundary[ 1 ] . $crlf .
            'Content-Type: text/html; charset="UTF-8"' . $crlf .
            'Content-Transfer-Encoding: quoted-printable' . $crlf . $crlf .
            $html . $crlf . $crlf .
            '--' . $boundary[ 1 ] . '--' . $crlf . $crlf;

    for ($i = 0; $i < 3; $i++) {
        $body .= '--' . $boundary[ 0 ] . $crlf .
                 'Content-Type: image/jpeg; name="image00' . $i . '.jpg"' . $crlf .
                 'Content-Transfer-Encoding: base64' . $crlf .
                 'Content-ID: <image00' . $i . '.jpg@' . $cid[ $i ] . '>' . $crlf . $crlf .
                 $imagedata[ $i ] . $crlf . $crlf;
    }

    $body .= '--' . $boundary[ 0 ] . '--' . $crlf;

    return ['body' => $body, 'headers' => $headers];
}

function a4w_admin_send_html_email( $to, $subj, $content, $cc = [] ) {
    $from = 'AWA Adventures for Wilderness Coordinator <a4w@abwild.ca>';

    $headers = [
        'From: ' . $from,
        'X-Sender: ' . $from,
        'Return-Path: ' . $from
    ];

    foreach ($cc as $addr) {
        $headers[] = 'Cc: ' . $addr;
    }

    $r = a4w_admin_generate_html_email_template( $headers, $subj, $content );

    return my_wp_mail( $to, $subj, $r[ 'body' ], $r[ 'headers' ] );
}


// ************************************************************************************************
// *                                                                                              *
// *    ADMIN PAGE SHORTCODES                                                                     *
// *                                                                                              *
// ************************************************************************************************

//
// Shortcodes that allow us to insert forms that will call the above API
//****************************************************************************

add_shortcode( 'a4w-add-participant', 'a4w_admin_sc_add_participant' );
add_shortcode( 'a4w-remove-participant', 'a4w_admin_sc_remove_participant' );
add_shortcode( 'a4w-get-personal-page-link', 'a4w_admin_sc_get_personal_page_link' );
add_shortcode( 'a4w-add-sponsor', 'a4w_admin_sc_add_sponsor' );
add_shortcode( 'a4w-create-adventure', 'a4w_admin_sc_create_adventure' );
add_shortcode( 'a4w-assign-sponsor', 'a4w_admin_sc_assign_sponsor' );
add_shortcode( 'a4w-split-sponsor', 'a4w_admin_sc_split_sponsor' );

function a4w_admin_sc_get_adventures_form_select( $domid, $include_all = FALSE, $onchange = NULL ) {
    $nl = "\n";

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $dbh = $r[ 1 ];

    $table = a4w_db_get_table_name( 'em_events' );
    $sql = 'SELECT   event_id, event_name, year(event_end_date) as event_year ' .
           'FROM     ' . $table .
           'WHERE    NOT event_private ' .
           'ORDER BY event_year DESC, event_name ASC;';

    $r = a4w_db_get_results( $dbh, $sql, NULL );
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $rs = $r[ 1 ];
    $prevyear = 0;
    
    $output = '<select id="' . $domid . '" class="a4w-admin-form-select-long">' . $nl .
              '<option value="0" selected>Select an adventure:</option>' . $nl;
    foreach ($rs as $event) {
        if ($event[ 2 ] != $prevyear) {
            $prevyear = $event[ 2 ];
            $output .= '<option value="0">' . $prevyear . ':</option>' . $nl;
        }
        $output .= '<option value="' . $event[ 0 ] . '">&nbsp; &nbsp; ' . $event[ 1 ] . '</option>' . $nl;
    }
    if ($include_all) {
        $output .= '<option value="0">---</option>' . $nl .
                   '<option value="*">(ALL ADVENTURES)</option>' . $nl;
    }
    $output .= '</select>' . $nl;
    
    if ($onchange !== NULL) {
        $output .= '<script>' . $nl .
                   'document.getElementById( "' . $domid . '" ).addEventListener( "change", ' . $onchange . ' );' . $nl .
                   '</script>' . $nl;
    }

    return $output;
}

function a4w_admin_sc_get_pending_sponsors_select( $domid, $adv_domid = NULL, $callback = NULL ) {
    $nl = "\n";
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $dbh = $r[ 1 ];

    $sponsors_table = a4w_db_get_table_name( 'a4w_sponsors' );
    $events_table = a4w_db_get_table_name( 'em_events' );
    
    $sql = 'SELECT    s.id, s.event_id, event_name, ' .
           '          sponsor_name, notes, amount/100 ' .
           'FROM      ' . $sponsors_table . ' AS s ' .
           'LEFT JOIN ' . $events_table . ' AS e ' .
           'ON        s.event_id = e.event_id ' .
           'WHERE     s.assigned = 0 ' .
           'ORDER BY  s.id DESC;';
    
    $r = a4w_db_get_results( $dbh, $sql, NULL );
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $rs = $r[ 1 ];
    
    $event_list = [];
    
    $output = '<select id="' . $domid . '" class="a4w-admin-form-select-long">' . $nl .
              '<option value="0" selected>Select a sponsor:</option>' . $nl;
    foreach ($rs as $sponsor) {
        $text = $sponsor[ 3 ] . ': $' . number_format( $sponsor[ 5 ], 2 ) . ' toward ' . $sponsor[ 2 ];
        if ($sponsor[ 4 ]) {
            $text .= ' ("' . substr( $sponsor[ 4 ], 0, 100 ) . '")';
        }
        $output .= '<option value="' . $sponsor[ 0 ] . '">' . $text . '</option>' . $nl;
        $event_list[ $sponsor[ 0 ] ] = [$sponsor[ 1 ], $sponsor[ 2 ]];
    }

    $output .= '</select>' . $nl;
    if (! is_null( $adv_domid )) {
        $output .= '<input type="text" disabled id="' . $adv_domid . '" value="0" style="display:none;">' . $nl .
                   '<script>' . $nl .
                   'var a4w_pending_sponsor_events = ' . json_encode( $event_list, JSON_FORCE_OBJECT ) . ';' . $nl .
                   'var a4w_pending_sponsors = document.getElementById( "' . $domid . '" );' . $nl .
                   'a4w_pending_sponsors.addEventListener(' . $nl .
                   '    "change", function() {' . $nl .
                   '        var el_adv_id = document.getElementById( "' . $adv_domid . '" );' . $nl .
                   '        el_adv_id.value = a4w_pending_sponsor_events[ this.value ][ 0 ];' . $nl .
                   '        el_adv_id.dispatchEvent( new Event( "change" ) );' . $nl .
                   '    }, false' . $nl .
                   ');' . $nl .
                   '</script>' . $nl;
    }
    if (! is_null( $callback )) {
        $output .= '<script>' . $nl .
                   'var a4w_pending_sponsors = document.getElementById( "' . $domid . '" );' . $nl .
                   'a4w_pending_sponsors.addEventListener( "change", ' . $callback . ' );' . $nl .
                   '</script>' . $nl;
    }
    
    return $output;
}

function a4w_admin_sc_get_ticket_types_select( $domid, $advselect_domid ) {
    $nl = "\n";

    $types = json_encode( a4w_db_get_ticket_types(), JSON_FORCE_OBJECT );

    $output = '<div id="' . $domid . '-container">' . $nl .
              '    <input type="text" disabled class="a4w-admin-form-text-long" value="[No registration categories available for this adventure]">' . $nl .
              '</div>' . $nl .
              '<script>' . $nl .
              'var a4w_ticket_types = ' . $types . ';' . $nl .
              'a4w_select_tickettype( "' . $advselect_domid . '", "' . $domid . '" );' . $nl .
              'document.getElementById( "' . $advselect_domid . '" ).addEventListener(' . $nl .
              '    "change", function() {' . $nl .
              '        a4w_select_tickettype( "' . $advselect_domid . '", "' . $domid . '" );' . $nl .
              '    }, false' . $nl .
              ');' . $nl .
              '</script>' . $nl;

    return $output;
}

function a4w_admin_sc_get_names_select( $domid, $advselect_domid, $allow_writein = FALSE ) {
    $nl = "\n";

    $writein_prompt = ($allow_writein ? 'true' : 'false');
    $names = json_encode( a4w_db_get_event_participants(), JSON_FORCE_OBJECT );

    $output = '<div id="' . $domid . '-container">' . $nl .
              '    <input type="text" disabled class="a4w-admin-form-text-long" value="[No participants yet registered in this adventure]">' . $nl .
              '</div>' . $nl .
              '<script>' . $nl .
              'var a4w_participant_names = ' . $names . ';' . $nl .
              'a4w_get_adv_participants( "' . $advselect_domid . '", "' . $domid . '", ' . $writein_prompt . ' );' . $nl .
              'document.getElementById( "' . $advselect_domid . '" ).addEventListener(' . $nl .
              '    "change", function() {' . $nl .
              '        a4w_get_adv_participants( "' . $advselect_domid . '", "' . $domid . '", ' . $writein_prompt . ' );' . $nl .
              '    }, false' . $nl .
              ');' . $nl .
              '</script>' . $nl;

    return $output;
}

function a4w_admin_sc_get_form_entry_select( $domid, $formid, $fieldname ) {
    $nl = "\n";
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $dbh = $r[ 1 ];

    $table = a4w_db_get_table_name( 'gf_form_meta' );
    $sql = 'SELECT display_meta ' .
           'FROM   ' . $table .
           'WHERE  form_id = ?';
    $params = [$formid];

    $r = a4w_db_get_result( $dbh, $sql, $params );
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $form_meta = json_decode( $r[ 1 ], TRUE );
    
    $formname = $form_meta[ 'title' ];
    
    $fieldids = [];
    foreach ($form_meta[ 'fields' ] as $field) {
        if ($field[ 'label' ] == $fieldname) {
            if (is_array( $field[ 'inputs' ] )) {
                $fieldids = array_map( fn( $input ) => $input[ 'id' ], $field[ 'inputs' ] );
            } else {
                $fieldids[] = $field[ 'id' ];
            }
            break;
        }
    }
    
    if (count( $fieldids ) == 0) {
        return 'No fields with name ' . $fieldname . ' found in form ' . $formname . '.';
    }

    $ids_list = implode( ', ', array_fill( 0, count( $fieldids ), '?' ) );
    $table = a4w_db_get_table_name( 'gf_entry_meta' );
    $sql = 'SELECT   entry_id, ' .
           '         group_concat(meta_value ORDER BY meta_key SEPARATOR \' \') ' .
           'FROM     ' . $table . ' ' .
           'WHERE    form_id = ? ' .
           'AND      meta_key IN (' . $ids_list . ') ' .
           'GROUP BY entry_id ' .
           'ORDER BY entry_id DESC;';
    $params = array_merge( $params, $fieldids );
    
    $r = a4w_db_get_results( $dbh, $sql, $params );
    $rs = $r[ 1 ];
    
    $output = '<select id="' . $domid . '" class="a4w-admin-form-select-long">' . $nl .
              '<option value="0" selected>Select an entry from form ' . $formname . ':</option>' . $nl;
    foreach ($rs as $entry) {
        $output .= '<option value="' . $entry[ 0 ] . '">' . $entry[ 0 ] . ': ' . $entry[ 1 ] . '</option>' . $nl;
    }
    $output .= '</select>' . $nl;

    return $output;
}

function a4w_admin_sc_get_year_select( $domid, $onchange = NULL, $instruction = FALSE ) {
    $nl = "\n";
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $dbh = $r[ 1 ];

    $table = a4w_db_get_table_name( 'em_events' );
    $sql = 'SELECT DISTINCT year( event_end_date ) ' .
           'FROM            ' . $table .
           'ORDER BY        year( event_end_date ) DESC;';

    $r = a4w_db_get_results( $dbh, $sql, NULL );
    if (! $r[ 0 ]) {
        return '[' . $r[ 1 ] . ']';
    }

    $rs = $r[ 1 ];
    
    $output = '<select id="' . $domid . '" class="a4w-admin-form-select-short">' . $nl;
    if ($instruction) {
        $output .= '<option value="0" selected>Select a year:</option>' . $nl;
    }

    $thisyear = current_time( 'Y' );
    $selected = '';
    foreach ($rs as $year) {
        if (! $instruction) {
            $selected = ($year == $thisyear) ? ' selected' : '';
        }
        $output .= '<option value="' . $year[ 0 ] . '"' . $selected . '>' . $year[ 0 ] . '</option>' . $nl;
    }
    $output .= '</select>' . $nl;
    
    if ($onchange !== NULL) {
        $output .= '<script>' . $nl .
                   'document.getElementById( "' . $domid . '" ).addEventListener( "change", ' . $onchange . ' );' . $nl .
                   '</script>' . $nl;
    }
    
    return $output;
}

function a4w_admin_sc_add_participant( $atts, $content = NULL ) {
    $nl = "\n";

    $adv_domid = 'a4w-admin-participant-adventure';
    $tix_domid = 'a4w-admin-participant-tickettype';
    $sub_domid = 'a4w-admin-participant-sponsor-box';

    $adventure_minimums = json_encode( a4w_admin_get_fundraising_request_amounts(), JSON_FORCE_OBJECT );
    
    $output = '<form class="a4w-admin-form" id="a4w-admin-participant-form">' . $nl .
              '    <div id="a4w-admin-participant-form-contents">' . $nl .
              '        <label for="' . $adv_domid . '" class="a4w-admin-form-label">Adventure to Join:</label>' . $nl .
              '        ' . a4w_admin_sc_get_adventures_form_select( $adv_domid, FALSE, 'a4w_add_participant_adventure_onchange' ) . $nl .
              '        <label for="' . $tix_domid . '" class="a4w-admin-form-label">Registration Category:</label>' . $nl .
              '        ' . a4w_admin_sc_get_ticket_types_select( $tix_domid, $adv_domid ) . $nl .
              '        <label for="a4w-admin-participant-entryid" class="a4w-admin-form-label">Registration Form Entry Number:</label>' . $nl .
              '        <input type="text" id="a4w-admin-participant-entryid" maxlength="6" class="a4w-admin-form-text-short">' . $nl .
              '        <label for="a4w-admin-participant-name" class="a4w-admin-form-label">Participant Name:</label>' . $nl .
              '        <input type="text" id="a4w-admin-participant-name" maxlength="50" class="a4w-admin-form-text-long">' . $nl .
              '        <label for="a4w-admin-participant-email" class="a4w-admin-form-label">Participant Email:</label>' . $nl .
              '        <input type="email" id="a4w-admin-participant-email" maxlength="50" class="a4w-admin-form-text-long" ' . $nl .
              '               pattern="[^@\s]+@[^@\s]+\.[^@\s]+" onblur="a4w_validate_email( \'a4w-admin-participant-email\' );">' . $nl .
              '        <label for="a4w-admin-participant-phone" class="a4w-admin-form-label">Participant Phone:</label>' . $nl .
              '        <input type="text" id="a4w-admin-participant-phone" maxlength="14" class="a4w-admin-form-text-short" ' . $nl .
              '               placeholder="(___) ___-____" pattern="\(\d{3}\) \d{3}-\d{4}" onblur="a4w_validate_phone( \'a4w-admin-participant-phone\' );">' . $nl .
              '        <label for="a4w-admin-participant-extra-info" class="a4w-admin-form-label">Other Participant Info:</label>' . $nl .
              '        <input type="text" id="a4w-admin-participant-extra-info" maxlength="255" class="a4w-admin-form-text-long">' . $nl .
              '        <label for="a4w-admin-participant-plan" class="a4w-admin-form-label">Fundraising plan:</label>' . $nl .
              '        <select id="a4w-admin-participant-plan" class="a4w-admin-form-select-short">' . $nl .
              '            <option value="self" selected>Self-sponsorship</option>' . $nl .
              '            <option value="others">Friends and family</option>' . $nl .
              '        </select>' . $nl .
              '        <label for="a4w-admin-participant-selfsponsor" class="a4w-admin-form-label">Participant has self sponsored?</label>' . $nl .
              '        <input type="checkbox" value="yes" id="a4w-admin-participant-selfsponsor" onclick="a4w_show_subform( this, \'' . $sub_domid . '\' );"' . $nl .
              '               style="margin-top:7px;margin-bottom:30px;">' . $nl .
              '        <span id="' . $sub_domid . '" class="a4w-admin-subform">' . $nl .
              '            <label for="a4w-admin-participant-sponsor-amt" style="display:inline;">for</label> ' . $nl .
              '            $<input type="text" id="a4w-admin-participant-sponsor-amt" maxlength="10" ' . $nl .
              '                    class="a4w-admin-form-text-short a4w-admin-form-currency" placeholder="0.00" style="display:inline;">' . $nl .
              '            <label for="a4w-admin-participant-sponsor-ordernum" style="display:inline;">with order number</label> ' . $nl .
              '            <input type="text" id="a4w-admin-participant-sponsor-ordernum" maxlength="6" class="a4w-admin-form-text-short" style="display:inline;">' . $nl .
              '        </span><br>' . $nl .
              '        <label for="a4w-admin-participant-waiver" class="a4w-admin-form-label">Waiver has been signed:</label>' . $nl .
              '        <input type="checkbox" value="yes" checked id="a4w-admin-participant-waiver"><br><br>' . $nl .
              '        <label for="a4w-admin-participant-notify" class="a4w-admin-form-label">Email notification to coordinator:</label>' . $nl .
              '        <input type="checkbox" value="yes" checked id="a4w-admin-participant-notify"><br><br>' . $nl .
              '        <label for="a4w-admin-participant-confirm" class="a4w-admin-form-label">Email confirmation to registrant:</label>' . $nl .
              '        <input type="checkbox" value="yes" id="a4w-admin-participant-confirm"><br><br>' . $nl .
              '        <input type="hidden" value="no" id="a4w-admin-participant-duplicate-ok">' . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-participant-submit" value="ADD PARTICIPANT" onclick="a4w_add_participant();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl .
              '<script>' . $nl .
              'var a4w_minimum_fundraising_amounts = ' . $adventure_minimums . ';' . $nl .
              '</script>' . $nl;

    return $output;
}

function a4w_admin_add_duplicate_participant_form( $event_id, $ticket_id, $entry_id, $part_name, $part_email,
                                                   $part_phone, $part_extrainfo, $plan, $selfsponsor, $sponsor_amt,
                                                   $sponsor_ordernum, $waiver, $notify, $confirm, $prev_email ) {
    $nl = "\n";

    $output = '        <p>' . $nl .
              '            Warning: Someone with the name ' . $part_name . ' has already been registered in this Adventure.<br>' . $nl .
              '               &nbsp; &nbsp; Previous email: <tt>' . $prev_email . '</tt><br>' . $nl .
              '               &nbsp; &nbsp; New email: <tt>' . $part_email . '</tt><br><br>' . $nl .
              '            Check the box below to add another registration for ' . $part_name . '.<br>' . $nl .
              '            <span style="font-size:smaller;font-style:italic;">(Note this will add a new ' . $nl .
              '            registration; it will not overwrite the existing entry.)</span>' . $nl .
              '        </p>' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-adventure" value="' . $event_id . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-tickettype" value="' . $ticket_id . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-entryid" value="' . $entry_id . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-name" value="' . $part_name . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-email" value="' . $part_email . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-phone" value="' . $part_phone . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-extra-info" value="' . $part_extrainfo . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-plan" value="' . $plan . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-selfsponsor" value="' . $selfsponsor . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-sponsor-amt" value="' . $sponsor_amt . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-sponsor-ordernum" value="' . $sponsor_ordernum . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-waiver" value="' . $waiver . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-notify" value="' . $notify . '">' . $nl .
              '        <input type="hidden" id="a4w-admin-participant-confirm" value="' . $confirm . '">' . $nl .
              '        <label for="a4w-admin-participant-duplicate-ok" class="a4w-admin-form-label">' .
              '            Ok to register ' . $part_name . ' in this adventure multiple times:' . $nl .
              '        </label>' . $nl .
              '        <input type="checkbox" value="yes" id="a4w-admin-participant-duplicate-ok"><br><br>' . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-participant-submit" value="ADD DUPLICATE PARTICIPANT" onclick="a4w_add_duplicate_participant();">' . $nl;
              
    return $output;
}

function a4w_admin_sc_remove_participant( $atts, $content = NULL ) {
    $nl = "\n";

    $adv_domid = 'a4w-admin-xparticipant-adventure';
    $nam_domid = 'a4w-admin-xparticipant-name';

    $output = '<form class="a4w-admin-form" id="a4w-admin-xparticipant-form">' . $nl .
              '    <div id="a4w-admin-xparticipant-form-contents">' . $nl .
              '        <label for="' . $adv_domid . '" class="a4w-admin-form-label">Adventure to Remove From:</label>' . $nl .
              '        ' . a4w_admin_sc_get_adventures_form_select( $adv_domid ) . $nl .
              '        <label for="' . $nam_domid . '" class="a4w-admin-form-label">Participant Name:</label>' . $nl .
              '        ' . a4w_admin_sc_get_names_select( $nam_domid, $adv_domid ) . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-xparticipant-submit" value="REMOVE PARTICIPANT" onclick="a4w_remove_participant();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl;

    return $output;
}

function a4w_admin_sc_get_personal_page_link( $atts, $content = NULL ) {
    $nl = "\n";
    
    $adv_domid = 'a4w-admin-personal-page-adventure';
    $nam_domid = 'a4w-admin-personal-page-name';

    $output = '<form class="a4w-admin-form" id="a4w-admin-personal-page-form">' . $nl .
              '    <div id="a4w-admin-personal-page-form-contents">' . $nl .
              '        <label for="' . $adv_domid . '" class="a4w-admin-form-label">Adventure Being Sponsored:</label>' . $nl .
              '        ' . a4w_admin_sc_get_adventures_form_select( $adv_domid ) . $nl .
              '        <label for="' . $nam_domid . '" class="a4w-admin-form-label">Participant Name:</label>' . $nl .
              '        ' . a4w_admin_sc_get_names_select( $nam_domid, $adv_domid ) . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-personal-page-submit" value="GET PERSONAL PAGE LINK" onclick="a4w_get_personal_page_link();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl .
              '<div id="a4w-admin-personal-page-form-response" style="display:none;">' . $nl .
              '    Link to personal page:' . $nl .
              '    <div class="a4w-admin-form-response" id="a4w-admin-personal-page-link">' . $nl .
              '    </div>' . $nl .
              '</div>' . $nl;

    return $output;

}

function a4w_admin_sc_add_sponsor( $atts, $content = NULL ) {
    $nl = "\n";

    $adv_domid = 'a4w-admin-sponsor-adventure';
    $nam_domid = 'a4w-admin-sponsor-participant';

    $output = '<form class="a4w-admin-form" id="a4w-admin-sponsor-form">' . $nl .
              '    <div id="a4w-admin-sponsor-form-contents">' . $nl .
              '        <label for="' . $adv_domid . '" class="a4w-admin-form-label">Adventure to Sponsor:</label>' . $nl .
              '        ' . a4w_admin_sc_get_adventures_form_select( $adv_domid ) . $nl .
              '        <label for="a4w-admin-sponsor-ordernum" class="a4w-admin-form-label">Order Number:</label>' . $nl .
              '        <input type="text" id="a4w-admin-sponsor-ordernum" maxlength="6" class="a4w-admin-form-text-short">' . $nl .
              '        <label for="a4w-admin-sponsor-name" class="a4w-admin-form-label">Sponsor Name:</label>' . $nl .
              '        <input type="text" id="a4w-admin-sponsor-name" maxlength="50" class="a4w-admin-form-text-long">' . $nl .
              '        <label for="a4w-admin-sponsor-email" class="a4w-admin-form-label">Sponsor Email:</label>' . $nl .
              '        <input type="text" id="a4w-admin-sponsor-email" maxlength="50" class="a4w-admin-form-text-long">' . $nl .
              '        <label for="a4w-admin-sponsor-amount" class="a4w-admin-form-label">Sponsorship Amount:</label>' . $nl .
              '        <input type="text" id="a4w-admin-sponsor-amount" maxlength="10" class="a4w-admin-form-text-short a4w-admin-form-currency" placeholder="0.00">' . $nl .
              '        <label for="' . $nam_domid . '" class="a4w-admin-form-label">Participant to Sponsor:</label>' . $nl .
              '        ' . a4w_admin_sc_get_names_select( $nam_domid, $adv_domid, TRUE ) . $nl .
              '        <label for="a4w-admin-sponsor-participant" class="a4w-admin-form-label">Name of Participant to Sponsor (if not in list above):</label>' . $nl .
              '        <input type="text" id="a4w-admin-sponsor-participant-alt" maxlength="50" class="a4w-admin-form-text-long">' . $nl .
              '        <label for="a4w-admin-sponsor-notes" class="a4w-admin-form-label">Notes:</label>' . $nl .
              '        <textarea id="a4w-admin-sponsor-notes" class="a4w-admin-form-textarea"></textarea>' . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-sponsor-submit" value="ADD SPONSOR" onclick="a4w_add_sponsor();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl;

    return $output;
}

function a4w_admin_sc_create_adventure( $atts, $content = NULL ) {
    $nl = "\n";

    $formid = 3;
    $fieldname = 'Adventure Name';
    
    $output = '<form class="a4w-admin-form" id="a4w-admin-adventure-form">' . $nl .
              '    <div id="a4w-admin-adventure-form-contents">' . $nl .
              '        <label for="a4w-admin-adventure-entry" class="a4w-admin-form-label">Import Adventure from form entry:</label>' . $nl .
              '        ' . a4w_admin_sc_get_form_entry_select( 'a4w-admin-adventure-entry', $formid, $fieldname ) . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-adventure-submit" value="IMPORT ADVENTURE" onclick="a4w_import_adventure();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl;

    return $output;
}

function a4w_admin_sc_assign_sponsor( $atts, $content = NULL ) {
    $nl = "\n";
    
    $sponsor_domid = 'a4w-admin-assignment-sponsor';
    $adventure_domid = 'a4w-admin-assignment-adventure';
    $participant_domid = 'a4w-admin-assignment-participant';
    $subform_domid = 'a4w-admin-assignment-participant-box';
    
    $output = '<form class="a4w-admin-form" id="a4w-admin-assignment-form">' . $nl .
              '    <div id="a4w-admin-assignment-form-contents">' . $nl .
              '        <label for="' . $sponsor_domid . '" class="a4w-admin-form-label">Sponsor to assign:</label>' . $nl .
              '        ' . a4w_admin_sc_get_pending_sponsors_select( $sponsor_domid, $adventure_domid ) . $nl .
              '        <label for="a4w-admin-assignment-general" class="a4w-admin-form-label">Assign to adventure in general?</label>' . $nl .
              '        <input type="checkbox" value="yes" id="a4w-admin-assignment-general" onclick="a4w_show_subform( this, \'' . $subform_domid . '\', true );">' . $nl .
              '        <div id="' . $subform_domid . '" class="a4w-admin-subform">' . $nl .
              '            <label for="' . $participant_domid . '" class="a4w-admin-form-label">Participant to assign to:</label>' . $nl .
              '            ' . a4w_admin_sc_get_names_select( $participant_domid, $adventure_domid, TRUE ) . $nl .
              '        </div><br>' . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-assignment-submit" value="ASSIGN SPONSOR" onclick="a4w_assign_sponsor();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl;
              
    return $output;
}

function a4w_admin_sc_split_sponsor( $atts, $content = NULL ) {
    $nl = "\n";
    
    $sponsor_domid = 'a4w-admin-split-sponsor';
    $sponsor_select_cb = 'a4w_admin_split_sponsor_selected';
    $amount_domid = 'a4w-admin-split-amount';
    $amount_range_cb = 'a4w_admin_split_amount_input';
    $amount_display_domid = 'a4w-admin-split-amount-display';
    
    $output = '<form class="a4w-admin-form" id="a4w-admin-split-form">' . $nl .
              '    <div id="a4w-admin-split-form-contents">' . $nl .
              '        <label for="' . $sponsor_domid . '" class="a4w-admin-form-label">Sponsorship to split:</label>' . $nl .
              '        ' . a4w_admin_sc_get_pending_sponsors_select( $sponsor_domid, NULL, $sponsor_select_cb ) . $nl .
              '        <label for="' . $amount_domid . '" class="a4w-admin-form-label">Amount to split at:</label>' . $nl .
              '        <input type="range" id="' . $amount_domid . '" min="1" max="1" value="1" class="a4w-admin-form-range-long"' . $nl .
              '               oninput="' . $amount_range_cb . '( this.value )">' . $nl .
              '        <div class="a4w-admin-form-range-display" id="' . $amount_display_domid . '">$1.00</div>' . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-split-submit" value="SPLIT SPONSORSHIP" onclick="a4w_split_sponsorship();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl;

    $helpers = '<script>' . $nl .
               'function ' . $sponsor_select_cb . '() {' . $nl .
               '    var el_sponsor = document.getElementById( "' . $sponsor_domid . '" );' . $nl .
               '    var el_amount = document.getElementById( "' . $amount_domid . '" );' . $nl .
               '    var opt = el_sponsor.options[ el_sponsor.selectedIndex ].text;' . $nl .
               '    var d1 = opt.indexOf( "$" );' . $nl .
               '    var d2 = opt.indexOf( " ", d1 );' . $nl .
               '    el_amount.max = parseInt( opt.substring( d1 + 1, d2 ) );' . $nl .
               '    el_amount.value = 1;' . $nl .
               '    ' . $amount_range_cb . '( 1 );' . $nl .
               '}' . $nl .
               'function ' . $amount_range_cb . '( newval ) {' . $nl .
               '    var display = document.getElementById( "' . $amount_display_domid . '" );' . $nl .
               '    display.innerHTML = "$" + parseFloat( newval ).toFixed( 2 );' . $nl .
               '}' . $nl .
               '</script>' . $nl;

    return $helpers . $output;
}


//
// Shortcodes for a form to allow coordinators to view their fundraising and
//  participant details
//****************************************************************************

add_shortcode( 'a4w-view-fundraising', 'a4w_admin_sc_view_fundraising' );

function a4w_admin_sc_view_fundraising( $atts, $content = NULL ) {
    $nl = "\n";

    $output = '<form class="a4w-admin-form" id="a4w-view-fundraising-form">' . $nl .
              '    <div id="a4w-view-fundraising-form-message" class="a4w-admin-message">' . $nl .
              '    </div>' . $nl .
              '    <div id="a4w-view-fundraising-form-contents">' . $nl  .
              '        <label for="a4w-view-fundraising-adventure" class="a4w-admin-form-label">Adventure that you are coordinating:</label>' . $nl .
              '        ' . a4w_admin_sc_get_adventures_form_select( 'a4w-view-fundraising-adventure' ) . $nl .
              '        <label for="a4w-view-fundraising-secret-code" class="a4w-admin-form-label">Your secret code:</label>' . $nl .
              '        <input type="password" id="a4w-view-fundraising-secret-code" maxlength="50" class="a4w-admin-form-text-long">' . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-view-fundraising-submit" value="View Participants and Fundraising" onclick="a4w_view_fundraising();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl;

    return $output;
}

//
// Shortcodes for admins to view website stats
//****************************************************************************

add_shortcode( 'a4w-website-stats', 'a4w_admin_sc_view_stats' );

function a4w_admin_sc_view_stats( $atts, $content = NULL ) {
    $nl = "\n";

    $defaults = [
        'year' => current_time( 'Y' )
    ];
    extract( shortcode_atts( $defaults, $atts ) );

    $stats = json_encode( a4w_admin_get_website_stats( $year ) );
    $num_participants = 0;
    
    $dateformat = 'Y-m-d \\a\\t H:i';
    
    // This first one is correct to use date() rather than current_time() because
    //  it's explicitly passing the timestamp in, which was saved to the version
    //  config file.
    $mod_time = date( $dateformat, A4W_ADMIN_DATE );
    $upld_time = wp_date( $dateformat, filemtime( WPMU_PLUGIN_DIR . '/a4w-version.php' ) );
    $stats_time = current_time( $dateformat );

    $year_domid = 'a4w-admin-stats-year-select';
    $year_onchange = 'a4w_get_website_stats_year_onchange';
    
    $output = '<div class="a4w-admin-form">' . $nl .
              '    <p style="font-size:smaller;margin-bottom:20px;">' . $nl .
              '        Stats generated on ' . $stats_time . $nl .
              '    </p>' . $nl .
              '    <label for="' . $year_domid . '" class="a4w-admin-form-label">Year to view stats for:</label>' . $nl .
              '    ' . a4w_admin_sc_get_year_select( $year_domid, $year_onchange ) . $nl .
              '    <table id="a4w-website-stats-table-contents"></table>' . $nl .
              '</div>' . $nl .
              '<script>' . $nl .
              'window.addEventListener(' . $nl .
              '    "DOMContentLoaded", function() {' . $nl .
              '        var website_stats = ' . $stats . ';' . $nl .
              '        a4w_get_website_stats_year_cb( website_stats );' . $nl .
              '    }, false' . $nl .
              ');' . $nl .
              '</script>' . $nl .
              '<p style="font-size:smaller;font-family:helvetica,arial,sans-serif;margin-top:20px;">' . $nl .
              '    (Website code revision ' . A4W_ADMIN_VERSION . ': ' . $nl .
              '     modified ' . $mod_time . ', ' . $nl .
              '     uploaded ' . $upld_time . ')' . $nl .
              '</p>' . $nl;

    return $output;
}

//
// Shortcodes for users to view download event finances, accounting and
//  and participant tables to Excel
//****************************************************************************

add_shortcode( 'a4w-download-donations', 'a4w_admin_sc_download_donations' );
add_shortcode( 'a4w-download-participants', 'a4w_admin_sc_download_participants' );

function a4w_admin_sc_download_donations( $atts, $content = NULL ) {
    $nl = "\n";

    $output = '';
    
    if (a4w_admin_permission_cb_financial()) {
        $adventure_select_list = a4w_admin_sc_get_adventures_form_select( 'a4w-admin-download-donations-adventure', TRUE, 'a4w_download_donations_adventure_onchange' );
        
        $output = '<h5>Download adventure donations table</h5>' . $nl .
                  '<form class="a4w-admin-form" id="a4w-admin-download-donations-form">' . $nl .
                  '    <label for="a4w-admin-download-donations-adventure" class="a4w-admin-form-label">Download Excel table showing donations for Adventure:</label>' . $nl .
                  '    ' . $adventure_select_list . $nl .
                  '    <input type="button" class="a4w-admin-form-submit" id="a4w-admin-download-donations-submit" value="DOWNLOAD TABLE" onclick="a4w_download_donations();">' . $nl .
                  '</form>' . $nl;
    }
    
    return $output;
}

function a4w_admin_sc_download_participants( $atts, $content = NULL ) {
    $nl = "\n";
    
    $adventure_select_list = a4w_admin_sc_get_adventures_form_select( 'a4w-admin-download-participants-adventure', TRUE, 'a4w_download_participants_adventure_onchange' );

    $output = '<form class="a4w-admin-form" id="a4w-admin-download-participants-form">' . $nl .
              '    <div id="a4w-admin-download-participants-form-contents">' . $nl .
              '        <label for="a4w-admin-download-participants-adventure" class="a4w-admin-form-label">Download Excel table showing participants in Adventure:</label>' . $nl .
              '        ' . $adventure_select_list . $nl .
              '        <input type="button" class="a4w-admin-form-submit" id="a4w-admin-download-participants-submit" value="DOWNLOAD TABLE" onclick="a4w_download_participants();">' . $nl .
              '    </div>' . $nl .
              '</form>' . $nl;
    
    return $output;
}


//
// JExcel stuff to allow creating Excel spreadsheets at client end.
//****************************************************************************
add_action( 'wp_enqueue_scripts', 'a4w_admin_setup_spreadsheet_scripts' );

function a4w_admin_setup_spreadsheet_scripts() {
    wp_enqueue_script( 'a4w-spreadsheet-scripts', '/wp-content/mu-plugins/a4w-admin/a4w-spreadsheet-functions.js', [], A4W_ADMIN_VERSION );
    
    if (A4W_ADMIN_LOGGING_LEVEL >= LOG_DEBUG) {
        wp_enqueue_script( 'myexcel-filesaver', '/wp-content/mu-plugins/MyExcel/FileSaver.js' );
        wp_enqueue_script( 'myexcel-jszip', '/wp-content/mu-plugins/MyExcel/jszip.js' );
        wp_enqueue_script( 'myexcel-myexcel', '/wp-content/mu-plugins/MyExcel/myexcel.js' );
    } else {
        wp_enqueue_script( 'myexcel-filesaver', '/wp-content/mu-plugins/MyExcel/FileSaver.min.js' );
        wp_enqueue_script( 'myexcel-jszip', '/wp-content/mu-plugins/MyExcel/jszip.min.js' );
        wp_enqueue_script( 'myexcel-myexcel', '/wp-content/mu-plugins/MyExcel/myexcel.min.js' );
    }
}


?>