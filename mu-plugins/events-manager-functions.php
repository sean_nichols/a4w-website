<?php

/****************************************************************************
 *
 * Various functions, hooks, etc. to be used by customizations for the
 *  Events Manager plugin and associated A4W event posts.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-01-10
 *
 ****************************************************************************/
 

require_once( __DIR__ . '/a4w-db-functions.php' );

// Unable to set EM_CONDITIONAL_RECURSIONS in wp-config.php as recommended by
//  the EM documentation (because Flywheel sets it to readonly), so I'm
//  putting it here instead.
define( 'EM_CONDITIONAL_RECURSIONS', 6 );

// Would like to have a conditional placeholder allowing us to filter by
//  the various ATT custom event attributes (since the built-in placeholders
//  have a limited provision for providing context-sensitive formatting).
remove_action( 'em_event_output_show_condition', 'my_em_att_event_output_show_condition', 1 );
remove_action( 'em_event_output_show_condition', 'my_em_noatt_event_output_show_condition', 1 );
remove_action( 'em_event_output_show_condition', 'my_em_details_event_output_show_condition', 1 );

function event_has_custom_att( $att, $EM_Event ) {
    $has_att = FALSE;
    
    if (!empty( $EM_Event->event_attributes ) &&
        is_array( $EM_Event->event_attributes ) &&
        array_key_exists( $att, $EM_Event->event_attributes )) {

        if (trim( $EM_Event->event_attributes[ $att ] ) !== '') {
            $has_att = TRUE;
        }
    }
    
    return $has_att;
}

add_action( 'em_event_output_show_condition', 'my_em_att_event_output_show_condition', 1, 4 );
function my_em_att_event_output_show_condition( $show, $condition, $full_match, $EM_Event ) {
    if (preg_match( '/^has_custom_att_(.+)$/', $condition, $matches )) {
        $show = event_has_custom_att( $matches[ 1 ], $EM_Event );
    }
    return $show;
}

add_action( 'em_event_output_show_condition', 'my_em_noatt_event_output_show_condition', 1, 4 );
function my_em_noatt_event_output_show_condition( $show, $condition, $full_match, $EM_Event ) {
    if (preg_match( '/^no_custom_att_(.+)$/', $condition, $matches )) {
        $show = !event_has_custom_att( $matches[ 1 ], $EM_Event );
    }
    return $show;
}

add_action( 'em_event_output_show_condition', 'my_em_details_event_output_show_condition', 1, 4 );
function my_em_details_event_output_show_condition( $show, $condition, $full_match, $EM_Event ) {
    if (preg_match( '/^has_details$/', $condition, $matches )) {
        $show = (!empty( $EM_Event->post_content ));
    }
    return $show;
}


// Some of our ATT custom addributes should be shown as-is, but with wpautop added (if it
//  hasn't been disabled for that adventure). The builtin #_ATT{...} placeholder doesn't do
//  that, so will need to add a custom tag for it, which I'm calling #_ATTP.
remove_filter( 'em_event_output_placeholder', 'my_em_att_with_wpautop_placeholder', 1 );

add_filter( 'em_event_output_placeholder', 'my_em_att_with_wpautop_placeholder', 1, 3 );
function my_em_att_with_wpautop_placeholder( $replace, $EM_Event, $result ) {
	if (preg_match( '/^#_ATTP\{?/', $result )) {
		$replace = '';

		$args = explode( ',', preg_replace( '/#_ATTP\{(.+)\}/', '$1', $result));
		$attrname = $args[ 0 ];
		if (!empty( $EM_Event->event_attributes ) &&
			is_array( $EM_Event->event_attributes ) &&
			array_key_exists( $attrname, $EM_Event->event_attributes )) {
				
			$attr = trim( $EM_Event->event_attributes[ $attrname ] );

			$post_id = $EM_Event->post_id;
			$wpautop_disabled = get_post_meta( $post_id, '_lp_disable_wpautop' );
			if ($wpautop_disabled && $wpautop_disabled[ 0 ]) {
				$replace = $attr;
			} else {
				$replace = wpautop( $attr );
			}
		}
	}
	return $replace;
}


// This is how we're going to display the sponsorship thermometers.
//  We have two shortcodes:
//   • get-a4w-sponsorship
//   • get-a4w-thermometer
// Both codes take either the slug (param "event") or ID (param "eventid") of
//  an A4W event as an attribute and pull the total sponsorship amount out of
//  the DB.
// The first tag just prints the total amount raised so far (as a number);
//  the second one outputs HTML that shows a graphical thermometer displaying
//  the amount raised so far.
// Both tags also take an optional argument "pct" that ignores the actual
//  amount raised so far, and instead show the amount (or corresponding
//  thermometer) assuming that percentage of the event's goal has been
//  reached.
// If both eventid and event are supplied (and are in conflict), then the
//  value of eventid takes precedence.
add_shortcode( 'get-a4w-sponsorship', 'sc_get_a4w_sponsorship' );
add_shortcode( 'get-a4w-thermometer', 'sc_get_a4w_thermometer' );

// To display the adventure sponsorship leaderboard, e.g.: on the sidebar.
//  We have one shortcode:
//   • get-a4w-leaderboard
// It takes four parameters:
//   • "type" determines the type of entries on the leaderboard:
//      • if 'adventures' then a list of the top fundraising adventures is
//        returned;
//      • if 'sponsors' then a list of top sponsors is returned;
//      • if 'participants' then a list of top participants (by funds they
//        have raised from themselves or others) is returned.
//     Default is 'adventures'.
//   • "scope" determines the scope of the leaderboard:
//      • if 'year:𝘯' then an aggregate across all adventures in the given
//        year is returned;
//      • if 'adventure:𝘯' then a list scoped to the adventure with the given
//        eventid is returned;
//      • if 'all' then an aggregate across all adventures in all years is
//        returned;
//     This parameter is ignored if "type" == 'adventures' and "scope" is
//     of the format 'event:𝘯'. Default is "year:<𝘤𝘶𝘳𝘳𝘦𝘯𝘵_𝘺𝘦𝘢𝘳>".
//   • "size" determines the size of (number of entries in) the leaderboard to
//     be returned. Default is 10
//   • "list" determines the type of list to be returned: either 'ul' or 'ol'.
//     Default is 'ol'.
add_shortcode( 'get-a4w-leaderboard', 'sc_get_a4w_leaderboard' );

// To display upcoming adventures on the sidebar.
//  We have one shortcode:
//   • get-a4w-upcoming
// It takes two parameters:
//   • "size" determines the size of (number of entries in) the upcoming list to
//     be returned. Default is 10
//   • "list" determines the type of list to be returned: either 'ul' or 'ol'.
//     Default is 'ol'.
add_shortcode( 'get-a4w-upcoming', 'sc_get_a4w_upcoming' );

// To get the event slug from the event id.
//  We have one shortcode:
//   • get-a4w-event-slug
// It takes the parameter "eventid"
add_shortcode( 'get-a4w-event-slug', 'sc_get_a4w_event_slug' );

// To get the number of remaining available slots in an event.
//  We have one shortcode:
//   • get-a4w-event-availability
// It takes two or three parameters:
//   • "eventid"
//   • "display" takes one of three values:
//      • if 'html', a full HTML <div> with display-ready availability in each
//        registration category is returned;
//      • if 'summary', a simple number indicating (maximum) availability is
//        returned;
//      • if 'js', a javascript snippet is returned that initializes a
//        variable to a JSON list with the availability in each category.
//        The snippet is enclosed in <script> tags.
//     Default is 'html'.
//   • "jsvar" is only applicable if "display" == 'js'. It contains the name
//     of the javascript variable to be initialized. This parameter is ignored
//     if "display" is anything else. Defaults to 'event_availability'.
add_shortcode( 'get-a4w-event-availability', 'sc_get_a4w_event_availability' );

// This is for displaying the date (or date range) of an event.
//  We have one shortcode:
//   • show-a4w-event-date
// It takes one mandatory parameter and up to 5 optional parameters:
//   • "eventid"
//   • "format" takes a standard PHP date format string that it uses to
//     format the dates in the range. Default is 'j M Y'.
//   • "formattime" indicates whether or not to also show the event's time of
//     day. It can take several values:
//      • 'false' -> the time will never be displayed
//      • a valid PHP time format string -> the time will be displayed using
//        that format, if a time range is specified for the adventure. If not
//        specified then either the string determined by the "notime"
//        parameter is displayed (in the case of a single-day event) or
//        nothing will be displayed (in the case of a multi-day event).
//     Default is 'g:i a'.
//   • "nodate" is an optional string that gets displayed if the 'No_Date'
//     categorization is set. Default is
//     'No specific date: this Adventure is ongoing'
//   • "tbd" is an optional string that gets displayed if the 'Date_TBD'
//     categorization is set. Default is 'To Be Announced'.
//   • "notime" is an optional string that gets displayed if "formattime" is
//     set (i.e.: not false) AND the adventure is a single day event BUT
//     no time (or time range) has been specified. Default is an empty
//     string ''.
add_shortcode( 'show-a4w-event-date', 'sc_show_a4w_event_date' );

function sc_get_a4w_event_amount( $atts ) {
    $defaults = [
        'eventid' => -1,
        'event'   => '',
        'pct'     => NULL
    ];

    extract( shortcode_atts( $defaults, $atts ) );

    $amt = 0;
    $total = 0;
    $eventid = intval( $eventid );

    if ($eventid == -1 && $event == '') {
        return [
            'ok'  => FALSE,
            'msg' => 'Event not found or no sponsorship goal specified.'
        ];
    }

    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return ['ok' => FALSE, 'msg' => $r[ 1 ]];
    }

    $dbh = $r[ 1 ];
    
    a4w_admin_log( LOG_DEBUG );
    if ($eventid == -1) {
        if ($event != '') {
            $r = a4w_db_get_event_id_from_slug( $dbh, $event );
            if (! $r[ 0 ]) {
                return ['ok' => FALSE, 'msg' => $r[ 1 ]];
            }
            $eventid = $r[ 1 ];
        }
    }
    
    a4w_admin_log( LOG_DEBUG, $eventid );
    $goal = a4w_db_get_event_goal( $dbh, $eventid ) * 100;

    a4w_admin_log( LOG_DEBUG );
    if (isset( $pct )) {
        $amt = intdiv( $goal * $pct, 100 );
    } else {
        $r = a4w_db_get_event_amt_raised( $dbh, $eventid );
        if (! $r[ 0 ]) {
            return ['ok' => FALSE, 'msg' => $r[ 1 ]];
        }
        $amt = $r[ 1 ];
    }        
    
    return [
        'ok'   => TRUE,
        'amt'  => $amt,
        'goal' => $goal
    ];
}

function sc_show_a4w_thermometer( $amt, $goal ) {
    $pct = round( $amt * 100 / $goal );
    $nl = "\n";
    $imgpath = '/wp-content/uploads/2020/01/';
    $imgname = $imgpath . 'thermometer_tree_250x300';
    
    if ($pct >= 100) {
        $sz = 300;
        $topline_display = 'none';
    } elseif ($pct == 0) {
        $sz = 0;
        $topline_display = 'none';
    } else {
        // Interpolate image mask size from:
        //  1% -> $sz =  42
        // 99% -> $sz = 248
        $sz = 42 + round( ($pct - 1) * 2.1 );
        $topline_display = 'block';
    }
    
    $output = '<div class="therm_caption">' . $pct . '% raised of $' . number_format( $goal / 100, 0 ) . ':<br></div>' . $nl .
              '<div class="therm_container">' . $nl .
              '    <img src="' . $imgname . '_bg.png" class="therm_bg">' . $nl .
              '    <div class="therm_fill_container" style="height:' . $sz . 'px;top:' . (300 - $sz) . 'px;">' . $nl .
              '        <img src="' . $imgname . '_fill.png" class="therm_fill" style="margin-top:' . ($sz - 300) . 'px;">' . $nl .
              '        <div class="therm_fill_topline" style="display:' . $topline_display . ';"></div>' . $nl .
              '    </div>' . $nl .
              '</div>' . $nl;

    return $output;
}

function sc_get_a4w_sponsorship( $atts, $content = NULL ) {
    extract( sc_get_a4w_event_amount( $atts ) );
    
    if (! $ok) {
        return '[' . $msg . ']';
    } else {
        return '$' . number_format( $amt / 100, 2 );
    }
}

function sc_get_a4w_thermometer( $atts, $content = NULL ) {
    extract( sc_get_a4w_event_amount( $atts ) );

    if (! $ok) {
        $output = '[' . $msg . ']';
    } else {
        $output = sc_show_a4w_thermometer( $amt, $goal );
    }
    
    return $output;
}

function sc_get_a4w_leaderboard( $atts, $content = NULL ) {
    $defaults = [
        'size'  => 10,
        'list'  => 'ol',
        'type'  => 'adventures',
        'scope' => 'year:' . current_time( 'Y' )
    ];

    extract( shortcode_atts( $defaults, $atts ) );
    
    if (! in_array( strtolower( $list ), ['ul', 'ol'] )) {
        $list = $defaults[ 'list' ];
    }
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r[ 1 ];
    }

    $dbh = $r[ 1 ];
    
    $scope = explode( ':', $scope );
    $thisyear = current_time( 'Y' );
    switch ($scope[ 0 ]) {
        case 'year':
            $scope[ 1 ] = intval( $scope[ 1 ] );
            if (! $scope[ 1 ]) {
                $scope[ 1 ] = $thisyear;
            }
            break;
        case 'adventure':
            if ($type == 'adventures') {
                // This combination doesn't make sense; fall back to this year.
                $scope[ 0 ] = 'year';
                $scope[ 1 ] = $thisyear;
            } else {
                $scope[ 1 ] = intval( $scope[ 1 ] );
            }
            break;
        case 'all':
        case '*':
            // This will grab all adventures across all years. See definition
            //  of e.g.: a4w_db_get_top_adventures() for why this works.
            $scope[ 0 ] = 'year';
            $scope[ 1 ] = '';
            break;
        default:
            $scope[ 0 ] = 'year';
            $scope[ 1 ] = $thisyear;
    }

    switch ($type) {
        case 'adventures':
            $r = a4w_db_get_top_adventures( $dbh, $size, $scope[ 1 ] );
            $output = sc_format_a4w_adventure_list( $r, $list, TRUE );
            break;
        case 'sponsors':
            $r = a4w_db_get_top_sponsors( $dbh, $size, $scope );
            $output = sc_format_a4w_sponsor_list( $r, $list );
            break;
        case 'participants':
            $r = a4w_db_get_top_participants( $dbh, $size, $scope );
            $output = sc_format_a4w_sponsor_list( $r, $list, TRUE );
            break;
        default:
            $output = 'Unrecognized leaderboard type requested.';
    }

    return $output;
}

function sc_get_a4w_upcoming( $atts, $content = NULL ) {
    $defaults = [
        'size' => 2,
        'list' => 'ul'
    ];

    extract( shortcode_atts( $defaults, $atts ) );
    
    if (! in_array( strtolower( $list ), ['ul', 'ol'] )) {
        $list = $defaults[ 'list' ];
    }
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r[ 1 ];
    }

    $dbh = $r[ 1 ];

    $r = a4w_db_get_next_adventures( $dbh, $size );

    return sc_format_a4w_adventure_list( $r, $list );
}

function sc_format_a4w_list( $listitems, $listtype = 'ol' ) {
    $listtype = trim( strtolower( $listtype ) );
    if (! in_array( $listtype, ['ol', 'ul'] )) {
        $listtype = 'ol';
    }
    
    return '<' . $listtype . '>' . "\n" .
           $listitems .
           '</' . $listtype . '>' . "\n";
}

function sc_format_a4w_adventure_list( $rows, $listtype, $iscurrency = FALSE ) {
    if (! $rows[ 0 ]) {
        return $rows[ 1 ];
    }
    
    $rs = $rows[ 1 ];
    
    $listitems = '';
    foreach ($rs as $adv) {
        $name = $adv[ 0 ];
        $url = '/adventure/' . $adv[ 2 ] . '/';
        $coord = $adv[ 1 ];
        if ($iscurrency) {
            $extra = '$' . number_format( round( $adv[ 3 ] / 100 ), 0 );
        } else {
            $extra = $adv[ 3 ];
        }
        $listitems .= '<li>' .
                      '<div class="sidebar_spons_name"><a href="' . $url . '">' . $name . '</a><br>&nbsp; <em>with ' . $coord . '</em></div>' .
                      ' &nbsp; ' .
                      '<div class="sidebar_spons_extra">' . $extra . '</div>' .
                      '</li>' . "\n";
    }

    return sc_format_a4w_list( $listitems, $listtype );
}

function sc_format_a4w_sponsor_list( $rows, $listtype, $showdecimals = FALSE ) {
    if (! $rows[ 0 ]) {
        return $rows[ 1 ];
    }
    
    $rs = $rows[ 1 ];
    
    $listitems = '';
    foreach ($rs as $sponsor) {
        $name = a4w_propercase( $sponsor[ 0 ] );
        $amount = $sponsor[ 1 ] / 100;
        if ($showdecimals) {
            $amount = number_format( $amount, 2 );
        } else {
            $amount = number_format( round( $amount ), 0 );
        }
        $listitems .= '<li>' .
                      '<div class="sidebar_spons_name singleheight">' . $name . '</div> &nbsp; ' .
                      '<div class="sidebar_spons_extra singleheight">$' . $amount . '</div>' .
                      '</li>' . "\n";
    }

    return sc_format_a4w_list( $listitems, $listtype );
}

function sc_get_a4w_event_slug( $atts, $content = NULL ) {
    $defaults = [
        'eventid' => -1
    ];
    
    extract( shortcode_atts( $defaults, $atts ) );

    $eventid = intval( $eventid );
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r[ 1 ];
    }
    
    return a4w_db_get_event_slug( $dbh, $eventid );
}

function sc_get_a4w_event_availability( $atts, $content = NULL ) {
    $defaults = [
        'eventid' => -1,
        'display' => 'html',
        'jsvar'   => 'event_availability'
    ];
    
    extract( shortcode_atts( $defaults, $atts ) );

    $eventid = intval( $eventid );

    if ($eventid == -1) {
        return 'Adventure not found or there is no availability for this adventure.';
    }
    
    $events = a4w_db_get_ticket_types( $eventid );
    $event_tickets = $events[ $eventid ];
    
    $nl = "\n";
    switch ($display) {
        case 'html':
            $output = '<strong>Availability:</strong>' . $nl .
                      '<div style="margin:0px 0px 20px 40px;">' . $nl;
            if (count( $event_tickets[ 'tickets' ] ) > 1) {
                foreach ($event_tickets[ 'tickets' ] as $ticket) {
                    $output .= '    ' . $ticket[ 'name' ] . ': ' . $ticket[ 'available' ] . ' remaining / ' . $ticket[ 'spaces' ] . ' total spaces<br>' . $nl;
                }
            } else {
                $output .= '    ' . $event_tickets[ 'available' ] . ' remaining / ' . $event_tickets[ 'spaces' ] . ' total spaces<br>' . $nl;
            }
            $output .= '</div>' . $nl;
            break;
        case 'summary':
            $output = $event_tickets[ 'available' ];
            break;
        case 'js':
            $output = '<script>' . $nl .
                      'var ' . $jsvar . ' = ' . json_encode( $event_tickets[ 'tickers' ], JSON_NUMERIC_CHECK ) . ';' . $nl .
                      '</script>' . $nl;
            break;
    }
    
    return $output;
}

function sc_show_a4w_event_date( $atts, $content = NULL ) {
    $defaults = [
        'eventid'    => -1,
        'format'     => 'j M Y',
        'formattime' => 'g:i a',
        'nodate'     => 'No specific date: this Adventure is ongoing',
        'tbd'        => 'To Be Announced',
        'notime'     => ''
    ];

    extract( shortcode_atts( $defaults, $atts ) );

    $eventid = intval( $eventid );

    if ($eventid == -1) {
        return 'Adventure not found or there is no date specified for this adventure.';
    }
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r[ 1 ];
    }
    
    $dbh = $r[ 1 ];

    $cats = a4w_db_get_event_categories( $dbh, $eventid );
    if (in_array( 'date_tbd', $cats )) {
        return $tbd;
    } else if (in_array( 'no_date', $cats )) {
        return $nodate;
    }
    
    $d = a4w_db_get_event_dates( $dbh, $eventid );
    if (is_null( $d )) {
        return 'No date found for this adventure';
    }
    
    $zero = strtotime( '0000-00-00 00:00:00' );

    // Use the PHP date() function here rather than WP's wp_date() because
    //  the timzeone offset has already been dealt with by the ACF field.
    $d0 = date( $format, $d[ 'start' ] );
    $d1 = date( $format, $d[ 'end' ] );
    $dzero = date( $format, $zero );
    
    if (($d0 != $d1) && ($d0 != $dzero)) {
        $output = $d0 . ' - ' . $d1;
    } else {
        $output = $d1;
    }
    
    if ((strtolower( $formattime ) != 'false') && ($formattime != '')) {
        $t0 = date( $formattime, $d[ 'start' ] );
        $t1 = date( $formattime, $d[ 'end' ] );
        $tzero = date( $formattime, $zero );
        
        $t_output = '';
        
        if (($t0 == $tzero) && ($t1 == $tzero)) {
            if ($d0 == $d1) {
                $t_output = $notime;
            }
        } elseif ($t0 == $tzero) {
            $t_output = $t1;
        } elseif ($t1 == $tzero) {
            $t_output = $t0;
        } else {
            $t_output = $t0 . ' - ' . $t1;
        }
        
        if ($t_output != '') {
            $output .= '<br>' . $t_output;
        }
    }

    return $output;
}

// To display the "create a new adventure" form.
//  We have one shortcode:
//   • a4w-create-adventure-form
// It takes no (direct) parameters, but anticipates a HTTP GET parameter
//  "form_entry_id" may be present on the page.
// If the form_entry_id parameter is present, the form fields will
//  pre-populated with values read from the form entry indicated by the
//  parameter. Otherwise the fields will be blank.
add_shortcode( 'a4w-create-adventure-form', 'sc_create_adventure_form' );

function sc_create_adventure_form( $atts, $content = NULL ) {
    $form_id = -1;
    if (array_key_exists( 'form_entry_id', $_GET )) {
        $form_id = $_GET[ 'form_entry_id' ];
    }
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r[ 1 ];
    }

    $dbh = $r[ 1 ];
    
    $output = 'Creating new adventure from form with ID ' . $form_id . '.';
    
    return '<p>' . $output . '</p>';
}

?>
