<?php

/****************************************************************************
 *
 * Disabling the plugin update notification for plugins that I don't want
 *  to update, for various reasons.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-03-26
 *
 ****************************************************************************/

function filter_plugin_updates( $value ) {
    $no_updates = [
        // AWA Customizations need to be checked and tested against any
        //  updates to Events Manager
        'events-manager/events-manager.php',
        
        // The Events Manager OSM add-on likewise may get out of sync with
        //  the above Events Manager plugin if it is continually updated
        'stonehenge-em-osm/stonehenge-em-osm.php',
        
        // Advanced Custom Fields Pro requires you to pay to update it - we
        //  have no need for this
        'advanced-custom-fields-pro/acf.php'
    ];
    
    foreach ($no_updates as $plugin) {
        unset( $value->response[ $plugin ] );
    }
    
    return $value;
}

add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

?>