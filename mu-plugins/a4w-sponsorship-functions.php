<?php

/****************************************************************************
 *
 * Various functions, hooks, etc. to be used for reporting on sponsorships.
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2021-04-13
 *
 ****************************************************************************/

require_once( __DIR__ . '/a4w-db-functions.php' );

// To show all A4W sponsors at a given sponsorship level.
//  We have one shortcode:
//   • get-a4w-upcoming
// This will return a list of all sponsors at the specified level(s), in
//  alphabetical order, formatted with <ul> and <li>.
// It takes two parameters:
//   • "levels" contains a comma-separated list of sponsorship levels to
//     include. Levels are:
//      • 'grizzly'     : $5000 or more
//      • 'caribou'     : $2500-4999
//      • 'sage-grouse' : $1000-2499
//      • 'trout'       : $500-999
//      • 'bison'       : $250-499
//      • 'wolf'        : up to $250
//     Default is to include all sponsors at all levels.
//   • "year" determines which year the qualifying sponsorships should be
//     taken from. Defaults to current year.
add_shortcode( 'list-a4w-sponsors', 'sc_list_a4w_sponsors' );

function sc_list_a4w_sponsors( $atts ) {
    $nl = "\n";
    
    $defaults = [
        'levels' => NULL,
        'year'   => current_time( 'Y' )
    ];

    extract( shortcode_atts( $defaults, $atts ) );
    
    $levelspec = [
        'grizzly'     => [500000, 10000000],
        'caribou'     => [250000, 500000],
        'sage-grouse' => [100000, 250000],
        'trout'       => [50000,  100000],
        'bison'       => [25000,  50000],
        'wolf'        => [0,      25000]
    ];

    if (is_null( $levels )) {
        $levels = array_keys( $levelspec );
    } else {
        $levels = explode( ',', $levels );
    }
    
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r[ 1 ];
    }
    
    $dbh = $r[ 1 ];
    
    $ranges = [];
    foreach ($levels as $level) {
        $ranges[] = $levelspec[ $level ];
    }
    
    $r = a4w_db_get_sponsors_in_range( $dbh, $ranges, $year );
    if (! $r[ 0 ]) {
        return $r[ 1 ];
    }
    
    $output = '<ul>' . $nl;
    foreach ($r[ 1 ] as $sponsor) {
        $output .= '<li>' . a4w_propercase( $sponsor[ 0 ] ) . '</li>' . $nl;
    }
    $output .= '</ul>' . $nl;
    
    return $output;
}

function a4w_verify_participant_for_sponsorship( $booking_id, $token, $name ) {
    a4w_admin_log( LOG_DEBUG, "Verifying participant details with [$booking_id], [$token], [$name]." );
    $r = a4w_db_get_dbh();
    if (! $r[ 0 ]) {
        return $r;
    }
    
    $dbh = $r[ 1 ];
    
    $event_id = a4w_db_verify_participant_info( $dbh, $booking_id, $token, $name );
    if (! $event_id) {
        a4w_admin_log( LOG_DEBUG, "Failed to verify." );
        return [
            FALSE,
            'Unknown Adventure participant: We are unable to find any information for that participant.'
        ];
    }
    a4w_admin_log( LOG_DEBUG, "Verified participant in event [$event_id]." );
    
    $fullname = a4w_db_get_participant_name( $dbh, $event_id, $booking_id )[ 1 ];
    $r = a4w_db_get_event_header_info( $dbh, $event_id );
    $header = $r[ 1 ][ 0 ];
    
    $r = a4w_db_get_participant_sponsors( $dbh, $event_id, $booking_id );
    $sponsors = ($r[ 0 ] ? $r[ 1 ] : []);

    $participant_info = [
        'fullname'  => $fullname,
        'sponsors'  => $sponsors,
        'adventure' => [
            'id'     => $event_id,
            'postid' => $header[ 0 ],
            'name'   => $header[ 1 ],
            'slug'   => $header[ 2 ]
        ]
    ];
    
    return [TRUE, $participant_info];
}

?>