<?php

/****************************************************************************
 *
 * This hooks into WooCommerce to export the relevant order details to the
 *  A4W site whenever an order is made that contains an A4W product. The
 *  API endpoint ('v1/transfer_shop_order') on the A4W site will handle
 *  unpacking the order details and processing them as it sees fit. (Note
 *  this happens in _addition_ to any usual WooCommerce processing - per
 *  contract implied by the woocommerce_payment_complete hook.)
 *
 * For API parameters and other documentation, refer to comments on the A4W
 *  site, in file wp-content/mu-plugins/a4w-admin-functions.php
 *
 * Author: Sean Nichols, AWA
 *         snichols@abwild.ca
 *
 * Date: 2020-09-25
 *
 ****************************************************************************/

// A4W_WEBSITE_SALT is defined here. It's in a separate file as a quick
//  prevention against it being saved to Git / other source repositories. 
require_once( __DIR__ . '/a4w-credentials.php' );

add_action( 'woocommerce_payment_complete', 'awa_woocommerce_check_a4w_export' );

/**
 * Top-line function, called on every woocommerce_payment_complete.
 *
 * If the order represents an A4W item, it extracts the relevant information
 *  (needed by the A4W API endpoint as above) and sends it to the A4W server.
 *
 */
function awa_woocommerce_check_a4w_export( $order_id ) {
    $order = wc_get_order( $order_id );
    
    if (awa_woocommerce_order_is_a4w( $order )) {
        $order_data = awa_woocommerce_extract_a4w_order_data( $order );
        
        // The API has no user security on it (since it's on a different
        //  WordPress site that can't share data with this one - hence the
        //  need for this API call in the first place), so it enforces data
        //  integrity (i.e.: to prevent 3rd-party data injections) by
        //  requiring that the data is also hashed with this secret salt, then
        //  sent along in the "token" field. It's quick and dirty, but
        //  probably ok for what we're doing. We could probably do better
        //  than SHA512.
        $ts = current_time( 'timestamp' );
        $o = json_encode( $order_data );
        
        $token = hash( 'sha512', $o . $ts . A4W_WEBSITE_SALT );
        
        awa_woocommerce_export_order( $o, $ts, $token );
    }
}

/**
 * Checks whether an instance of WC_Order is an A4W order, by examining whether
 *  any of its constituent items is an A4W item.
 *
 */
function awa_woocommerce_order_is_a4w( $order ) {
    foreach ($order->get_items() as $item) {
        if (awa_woocommerce_item_is_a4w( $item )) {
            return TRUE;
        }
    }
    
    return FALSE;
}

/**
 * Checks whether an instance of WC_Order_Item is an A4W item.
 *
 * This is the case when either:
 *   - the item name starts with "Sponsor an Adventure for Wilderness"; or
 *   - the item name starts with "Donate" AND there is a Gravity Forms field
 *      (stored in the meta data) keyed by "This gift is:" and with value
 *      "an Adventure 4 Wilderness Donation".
 *
 */
function awa_woocommerce_item_is_a4w( $item ) {
    $name = trim( $item->get_name() );
    if (substr( $name, 0, 35 ) == 'Sponsor an Adventure for Wilderness') {
        return TRUE;
    } elseif (substr( $name, 0, 6 ) == 'Donate') {
        foreach ($item->get_meta_data() as $meta) {
            if (trim( $meta->key ) == 'This gift is:' &&
                trim( $meta->value ) == 'an Adventure 4 Wilderness Donation') {
                    
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

/**
 * Extracts the relevant A4W-related data from a WC_Order object.
 *
 * Grabs the order number, name of the donor, and email address for the order
 *  as a whole, along with the quantity, amount donated, adventure and notes
 *  from each item that is an A4W item.
 *
 * NOTA BENE:
 *
 * The last three fields are (Gravity Forms) fields indexed as 28, 29 and 32.1
 *  in both applicable forms. If the forms are rearranged or rebuilt, etc.
 *  then this will need to change.
 *
 */
function awa_woocommerce_extract_a4w_order_data( $order ) {
    $order_data = [];
    
    $order_data[ 'number' ] = $order->get_order_number();
    $order_data[ 'sponsor' ] = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
    $order_data[ 'email' ] = $order->get_billing_email();
    $order_data[ 'targets' ] = [];
    
    $ADVN_FIELD = '28';
    $NOTE_FIELD = '29';
    $ANON_FIELD = '32.1';
    
    foreach ($order->get_items() as $item) {
        if (awa_woocommerce_item_is_a4w( $item )) {
            $target = [
                'amount' => $item->get_subtotal(),
                'quantity' => $item->get_quantity()
            ];
            foreach ($item->get_meta_data() as $meta) {
                if (trim( $meta->key ) == '_gravity_forms_history') {
                    $form_data = $meta->value;
                    $target[ 'adventure' ] = $form_data[ '_gravity_form_lead' ][ $ADVN_FIELD ];
                    $target[ 'notes' ] = $form_data[ '_gravity_form_lead' ][ $NOTE_FIELD ];
                    $target[ 'anonymous' ] = ($form_data[ '_gravity_form_lead' ][ $ANON_FIELD ] == 'yes');
                }
            }
            
            $order_data[ 'targets' ][] = $target;
        }
    }
    
    return $order_data;
}

/**
 * Exports the order to the A4W site by calling the 'v1/transfer_shop_order'
 *  RESTful API on that site. Needs to make the call via HTTPS POST, for which
 *  the easiset way to do is make a cURL call, and set CURLOPT_RETURNTRANSFER
 *  to TRUE so that it just saves the response (as it happens, to the $response
 *  variable) rather than echoing it to the screen, which would be suboptimal.
 *
 */
function awa_woocommerce_export_order( $order, $timestamp, $token ) {
    $url = 'https://www.adventuresforwilderness.ca/wp-json/a4w-admin/v1/transfer_shop_order';
    $post_data = http_build_query( [
        'order'     => $order,
        'timestamp' => $timestamp,
        'token'     => $token
    ] );
    
    $log_file_name = '/www/wp-content/uploads/wildspaces2020/' . $timestamp . '.txt';
    $request = print_r( $post_data, TRUE );
    
    
    $conn = curl_init();

    curl_setopt( $conn, CURLOPT_URL, $url );
    curl_setopt( $conn, CURLOPT_FOLLOWLOCATION, TRUE );
    curl_setopt( $conn, CURLOPT_CUSTOMREQUEST, 'POST' );
    curl_setopt( $conn, CURLOPT_POSTFIELDS, $post_data );
    curl_setopt( $conn, CURLOPT_POSTREDIR, 3 );

    curl_setopt( $conn, CURLOPT_RETURNTRANSFER, TRUE );

    $response = curl_exec( $conn );
    
    if ($response === FALSE) {
        $response = '[FALSE]';
    }
    
    $log_file_contents = "REQUEST:\n" . $request . "\nRESPONSE:\n" . $response;

    file_put_contents( $log_file_name, $log_file_contents );
}

?>